# Import Python libraries #
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor
from functools import partial
import datetime as dt
import inspect
import json
import numpy as np
import os
import pandas as pd
import pdfplumber
import re
import requests
from requests_futures.sessions import FuturesSession
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from sklearn.feature_extraction.text import TfidfVectorizer
import spacy
import time
import toolz
import urllib


# Launch SGX home page with Selenium's webdriver to get voting rights #  
def arr_convert_df(vote_array, df_isin):
    ''' 
    Convert Numpy array into Pandas Dataframe and perform pre-processing.
    Parameters:
        vote_array    : World Trading Data GET url
        df_isin       : DataFrame containing securities' ISIN and ticker extracted
                        from Bloomberg POST API
    Return:
        df_securities : DataFrame containing security-level details
    '''
    # Convert array into DataFrame #
    df_securities = pd.DataFrame(vote_array, columns=['COMPANY', 'YEAR_END_DATE', 'SECURITY_TYPE', 'VOTING_RIGHT', 'CORPUS'])
    
    # Manipulate column data types and/or outputs #
    ## Convert voting rights information to float ##
    df_securities['VOTING_RIGHT'] = df_securities['VOTING_RIGHT'].astype(float)
    ## Create additional column SECURITY_TYPE_1 to standardise Ordinary and Preference Shares ##
    ### Standardise Share(s) as Stock ###
    df_securities['SECURITY_TYPE_1'] = df_securities['SECURITY_TYPE']
    df_securities.loc[df_securities['SECURITY_TYPE_1'].str.contains(r'Shares?'), 'SECURITY_TYPE_1'] = \
        df_securities.loc[df_securities['SECURITY_TYPE_1'].str.contains(r'Shares?'), 'SECURITY_TYPE_1'].str.replace(r'Shares?', 'Stock')
    ### Standardise Ordinary Unit(s) as REIT ###
    df_securities.loc[df_securities['SECURITY_TYPE_1'].str.contains(r'Ordinary Units?|Stapled Security'), 'SECURITY_TYPE_1'] = 'REIT'
    ### Standardise Ordinary and Preference as Common and Preferred Stock respectively ###
    df_securities.loc[df_securities['SECURITY_TYPE_1'].str.contains('Ordinary'), 'SECURITY_TYPE_1'] = \
        df_securities.loc[df_securities['SECURITY_TYPE_1'].str.contains('Ordinary'), 'SECURITY_TYPE_1'].str.replace('Ordinary', 'Common')
    df_securities.loc[df_securities['SECURITY_TYPE_1'].str.contains('Preference'), 'SECURITY_TYPE_1'] = \
        df_securities.loc[df_securities['SECURITY_TYPE_1'].str.contains('Preference'), 'SECURITY_TYPE_1'].str.replace('Preference', 'Preferred')
    
    # Merge df_securities and df_isin to obtain the ISINs and tickers #
    df_securities = pd.merge(df_securities, 
                             df_isin[['COMPANY', 'ISIN', 'TICKER', 'SECURITY_TYPE_2']], 
                             how='left', 
                             left_on=['COMPANY',  'SECURITY_TYPE_1'], 
                             right_on=['COMPANY', 'SECURITY_TYPE_2'])
    
    # Rearrange the columns in df_securities #
    df_securities = df_securities[['COMPANY', 'YEAR_END_DATE', 'ISIN', 'TICKER', 
                                   'SECURITY_TYPE', 'VOTING_RIGHT', 'CORPUS']]

    return df_securities

    
def chunks(lst, n):
    '''
    Generate successive n-sized chunks from list.
    Parameters:
        lst           : List containing the elements to be broken into chunks
        n             : Size of each chunk
    Return:
        chunked_list  : List containing n-sized chunks
    '''
    chunked_list = [lst[i:i + n] for i in range(0, len(lst), n)]
    
    return chunked_list


def corpus_splitter(corpus, n):
    '''
    Generate successive n-sized corpus.
    Parameters:
        corpus        : Corpus text
        n             : Length of each corpus
    Return:
        split_corpus  : Generator containing the split corpus
    '''
    # Apply word tokenization and remove irrelevant words #
    pieces = corpus.split()
    pieces = [piece for piece in pieces if len(piece) < 100]
    
    # Join the remaining corpus # 
    split_corpus = (' '.join(pieces[i:i+n]) for i in range(0, len(pieces), n))
    
    return split_corpus
    

def download_reports(web_url, download_path, df_reports):
    '''
    Download the list of Annual and Sustainability Reports for each target company.
    Parameters:
        web_url          : Url for relevant company announcement 
        download_path    : String containing the local download filepath
        df_reports       : DataFrame containing Singaporean listed companies and 
                           their associated announcement weblinks     
    '''
    # Initialise parameters #
    ## Row containing the web_url ##
    row = df_reports.loc[df_reports['WEBLINK'] == web_url]
    ## Company Name ##
    company = row['COMPANY'].iloc[0]
    ## Filing Date ##
    filing_date = row['FILING_DATE'].iloc[0].strftime('%Y%m%d')
    ## Report Type ##
    if row['REPORT'].iloc[0] == 'Annual Report':
        report = 'AR'
    elif row['REPORT'].iloc[0] == 'Sustainability Report':
        report = 'SR'
    else:
        report = 'OTHERS'
    
    # Parse GET request to obtain the actual report weblink #
    response = requests.get(web_url)
    soup = BeautifulSoup(response.text)
    try:
        web_url = 'https://links.sgx.com' + soup.find('a')['href']
    except Exception:
        print('Report could not be extracted for this company: %s' % company)
    else:    
        # Download the underlying report into local directory #
        urllib.request.urlretrieve(web_url, 
                                   download_path + '\\%s_%s_%s.pdf' % (company, report, filing_date))
    

def get_full_companies_sginvestors(download_path, max_workers, df_isin, errant_companies, errant_isins):
    '''
    Consolidate the list of Singaporean listed companies and their associated announcement weblinks from SG Investors.
    Parameter:
        download_path     : String containing the local download filepath
        max_workers       : Number of threads
        df_isin           : DataFrame containing Singaporean listed companies and
                            their associated details
        errant_companies  : Dictionary containing the list of errant companies in SGX websites
        errant_isins      : Dictionary containing the list of errant isins in SGInvestors
    Return:
        sgx_companies     : DataFrame containing Singaporean listed companies and 
                            their associated announcement weblinks
    '''  
    # Initialise parameters #
    ## String containing SG Investors' website url ##
    sginvestors_website = 'https://sginvestors.io/sgx/stock-listing/alpha'
    ## Empty arrays ##
    isin_arr = np.empty((0,3), str)
    security_types = np.empty((0,4), str)

    # Send GET request to SG Investors' website and scrape relevant details #
    ## Send GET request ##
    response = requests.get(sginvestors_website)
    ## Apply BeautifulSoup to text response ##
    soup = BeautifulSoup(response.text)
    ## Scrape all the component_websites ##
    component_websites = soup.find_all('li', {'class':'stockitem'})
    component_websites = [[component_website.find('span', {'itemprop':'name'}).text, component_website.a['href']] 
                           for component_website in component_websites]
    ## Repeat above process for SG REITs ##
    sginvestors_website = 'https://sginvestors.io/sgx/reit-listing/alpha'
    response = requests.get(sginvestors_website)
    soup = BeautifulSoup(response.text)
    reit_websites = soup.find_all('li', {'class':'stockitem'})
    reit_websites = [[reit_website.find('span', {'itemprop':'name'}).text, reit_website.a['href']] 
                      for reit_website in reit_websites]
    ## Consolidate the websites and remove all duplicates ##
    component_websites = component_websites + reit_websites
    component_websites = list(map(list,toolz.unique(map(tuple, component_websites))))
    
    # Send GET request to the component websites to obtain more information #
    with FuturesSession(max_workers = max_workers) as session:
        futures = np.array([[company, web_url, session.get(web_url).result()] for company, web_url in component_websites])  
    for seq, response in enumerate(futures[:,-1]):
        soup = BeautifulSoup(response.text)
        if soup.find('div', {'title':re.compile('Last Traded Share Price')}).find_all('span')[1].text.strip() != '':
            ## Append results for active securities ##
            isin_arr = np.vstack((isin_arr,
                                  np.array([[futures[seq,0],
                                             futures[seq,1],
                                             soup.find('span', string='Market / ISIN Code: ').parent.find_all('strong')[-1].text.strip()]]
                                            )))
        else:
            ## Print results for inactive securities ##
            print('Company with inactive securities: %s' % futures[seq,0])
    
    # Convert isin_arr into DataFrame and pre-process it #
    ## Convert into array into DataFrame ##
    active_securities = pd.DataFrame(isin_arr, columns=['COMPANY', 'WEB_URL', 'isin'])
    ## Update all blanks as nan ##
    active_securities.replace('', np.nan, inplace=True)
    ## Merge active_securities with df_isin to fill for missing ISINs ##
    active_securities = pd.merge(active_securities, df_isin, how='left', on='COMPANY')
    active_securities.loc[active_securities['isin'].isnull(), 'isin'] =  active_securities.loc[active_securities['isin'].isnull(), 'ISIN']
    ## Drop irrelevant securities ##
    ### Drop Japanese securities ###
    jp_index = active_securities.loc[active_securities['isin'].str.contains('JP')].index
    active_securities.drop(jp_index, inplace=True)
    ### Drop securities with $ symbol ###
    dollar_index = active_securities.loc[active_securities['COMPANY'].str.contains(r'\$')].index
    active_securities.drop(dollar_index, inplace=True)
    ### Drop duplicate ISINs ###
    active_securities.drop_duplicates(subset='isin', inplace=True)
    ## Replace errant ISINs ##
    pattern = re.compile('|'.join(errant_isins['ERRANT']))
    active_securities.sort_values(by='isin', inplace=True)
    active_securities.loc[active_securities['isin'].str.contains(pattern), 'isin'] = errant_isins['CORRECT']
    ## Replace errant company names and sort them in alphabetical order ##
    pattern = re.compile('|'.join(errant_companies['ERRANT']))
    active_securities.sort_values(by='COMPANY', inplace=True)
    active_securities.loc[active_securities['COMPANY'].str.contains(pattern), 'COMPANY'] = errant_companies['CORRECT']
    ## Manipulate columns ##
    active_securities = active_securities[['COMPANY', 'WEB_URL', 'isin']]
    active_securities.columns = ['COMPANY', 'WEB_URL', 'ISIN']
    
    # Get security type using Bloomberg POST API and merge with active_securities #
    ## Convert ISINs in df_securities into a list ##
    isins = list(active_securities['ISIN'])
    ## Break up the ISINs into batches of 100 (Upper limit set by Bloomberg POST API) ##
    chunked_isins = chunks(isins, 100)
    ## Feed each batch of ISINs into Bloomberg POST API ##
    for seq, chunked_isin in enumerate(chunked_isins):
        types = get_security_type(openfigi_url, openfigi_key, chunked_isin, 'SP')
        security_types = np.vstack((security_types, types))
    ## Transform security_types array into DataFrame ##
    df_sec_types = pd.DataFrame(security_types, columns=['ISIN', 'TICKER', 'SECURITY_TYPE_1', 'SECURITY_TYPE_2'])
    df_sec_types = df_sec_types.replace('nan', np.nan)
    ## Filter cases with nan security types and repeat cycle with SGX-ST as Exchange Code ##
    isins = list(df_sec_types.loc[df_sec_types['SECURITY_TYPE_1'].isnull(), 'ISIN'])
    chunked_isins = chunks(isins, 100)
    for seq, chunked_isin in enumerate(chunked_isins):
        types = get_security_type(openfigi_url, openfigi_key, chunked_isin, 'SGX-ST')
        security_types = np.vstack((security_types, types))
    ## Transform security_types array into DataFrame ##
    df_sec_types = pd.DataFrame(security_types, columns=['ISIN', 'TICKER', 'SECURITY_TYPE_1', 'SECURITY_TYPE_2'])   
    df_sec_types = df_sec_types.replace('nan', np.nan)     
    ## Merge security_types with df_isin ##
    active_securities = pd.merge(active_securities, df_sec_types, on='ISIN')
    ## Drop duplicate ISINs ##
    active_securities.drop_duplicates(subset='ISIN', inplace=True)
    ## Fill nan as Common Stock in df_isin ## 
    active_securities.loc[active_securities['SECURITY_TYPE_2'].isnull(), 'SECURITY_TYPE_2'] = 'Common Stock'
    ## Convert Receipt to Common Stock in df_isin ##
    active_securities.loc[active_securities['SECURITY_TYPE_2'].str.contains('Receipt'), 'SECURITY_TYPE_2'] = 'Common Stock'
    ## Convert Units to Ordinary Units in df_isin ##
    active_securities.loc[active_securities['SECURITY_TYPE_2'].str.contains('Unit'), 'SECURITY_TYPE_2'] = 'REIT'

    # Download processed DataFrame #
    active_securities.to_excel(download_path + '\\SGX_ACTIVE_SECURITIES.xlsx', sheet_name='SECURITIES', index=False)
    
    return active_securities
    

def get_full_companies_sgx(browser, company, sgx_root_website):
    '''
    Consolidate the list of Singaporean listed companies and their associated announcement weblinks from SGX.
    Parameters:
        browser           : Firefox browser used
        company           : Listed company name
        sgx_root_website  : SGX root website address
    Return:
        sgx_announcements : DataFrame containing Singaporean listed companies and 
                            their associated announcement weblinks
    '''
    # Initialise parameters # 
    ## Empty array ##
    sgx_announcements = np.empty((0,4), str)  
    ## Time delay ##
    delay = 5
    
    # Send GET request to SGX homepage #
    ## Send GET request ##
    browser.get(sgx_root_website)
    # Cater waiting time #
    WebDriverWait(browser, 30).until(EC.presence_of_element_located((By.CLASS_NAME, 'sgx-input-select-filter'))) 
    time.sleep(delay)
    ## Accept cookies ##
    try:
        browser.find_element_by_class_name('sgx-consent-banner-acceptance-button').click()
    except Exception:
        pass
    
    # Feed company name to the Search Bar #
    ## Locate search bar ##
    search_bar = browser.find_element_by_class_name('sgx-input-control')
    ## Clear Search Bar ##
    search_bar.clear()
    ## Input company name ##
    if company == 'GRP LTD':
        search_bar.send_keys('GRP LIMITED')    
        time.sleep(delay)
        browser.find_element_by_xpath("//sgx-select-picker-option[@title='GRP LIMITED']").click()
        time.sleep(delay)
    else:
        search_bar.send_keys(company)
        time.sleep(delay)
        search_bar.send_keys(Keys.ENTER)
        time.sleep(delay)
    
    # Scrape all the results #
    ## Apply BeautifulSoup to browser content ##
    soup = BeautifulSoup(browser.page_source, 'html.parser')
    ## Perform validation check to ensure the target company queried is correct ##
    if soup.find('span', {'class':'sgx-select-filter-icon'})['title'] == 'Clear':    
        ### Search table and get relevant details ###
        rows = soup.find_all('sgx-table-row')
        ### Append company details in sgx_companies ###       
        if company == 'GRP LTD':
            data_rows = [row 
                         for row in rows 
                         if row.findChild('sgx-table-cell-text', {'data-column-id':'security'}).text == company and \
                         not re.search('display: none;', row['style'])]
        else:
            data_rows = [row for row in rows if not re.search('display: none;', row['style'])]
        if data_rows:
            sgx_announcements = np.array([[company,
                                           dt.datetime.strptime(data_row.find('sgx-table-cell-date').text.strip(), '%d %b %Y'),
                                           data_row.find('sgx-table-cell-link').text.strip(),
                                           data_row.find('sgx-table-cell-link').find('a')['href']]
                                            for data_row in data_rows])
        else:
            ### Print status message ###
            print('No announcements for this company: %s' % company)
    else:
        ### Print status message ###
        print('No announcements for this company: %s' % company)
  
    # Clear Search Bar #
    search_bar.clear() 
            
    return sgx_announcements
        

def get_isin(browsers, openfigi_url, openfigi_key):
    ''' 
    Download the list of Singaporean listed securities and their associated ISINs 
    to merge with other files.
    Parameters:
        browsers         : List of Selenium Firefox browsers 
        openfigi_url     : String containing Openfigi API GET url
        openfigi_key     : String containing Openfigi API key
    Return:
        df_isin          : DataFrame containing Singaporean listed securities and their ISINs 
    '''  
    # Initiate parameters #
    ## Base URL ##
    base_url = 'https://www2.sgx.com/research-education/securities#ISIN%20Code'
    ## Firefox Browser used ##
    browser = browsers[0]
    ## Boolean counter ##
    continue_ = True
    
    # Retrieve weblink for SG companies' ISIN #
    ## Send GET request to base_url ##
    browser.get(base_url)
    time.sleep(3)
    ## Get weblink for SG companies' ISIN ##
    while continue_:
        try:
            ### Apply BeautifulSoup to browser content ###
            soup = BeautifulSoup(browser.page_source, 'html.parser')  
            isin_weblink = soup.find('a', {'class' : 'widget-download-list-item-link text-body website-link'})['href']
        except Exception:
            ### Refresh browser if it fails to load ###
            browser.refresh()
            time.sleep(3)
        else:
            ### Exit while loop ###
            continue_ = False
        
    # Convert ISIN data to readable format # 
    ## Send GET request to isin_weblink ##
    browser.get(isin_weblink)
    ## Get URL of current page and feed it to urllib ##
    data = urllib.request.urlopen(browser.current_url)
    ## Extract the ISIN data ##
    lines = data.readlines()[1:]
    lines = [re.split(r'[\s]{2,}', line.decode('utf-8').strip()) 
             for line in lines
             if len(re.split(r'[\s]{2,}', line.decode('utf-8').strip())) == 4]
    ## Convert lines into DataFrame ##
    df_isin = pd.DataFrame(lines, columns=['COMPANY', 'ISIN', 'TICKER', 'TRADING_COUNTER_NAME'])
    
    # Apply filters to drop irrelevant securities #
    pattern = re.compile('%|\$|\d+M|FM\sCAS')
    df_isin = df_isin.loc[~df_isin['COMPANY'].str.contains(pattern)]
    df_isin = df_isin.loc[~df_isin['ISIN'].str.startswith('LU')]
    df_isin = df_isin.loc[~df_isin['ISIN'].str.startswith('XS')]
#    df_isin = df_isin.loc[~df_isin['ISIN'].str.startswith('SGX')]
    df_isin = df_isin.loc[~df_isin['TRADING_COUNTER_NAME'].str.contains(pattern)]
    df_isin.drop_duplicates(subset=['ISIN'], inplace=True)

    # Drop irrelevant records #
    df_isin = df_isin[['COMPANY', 'ISIN']]
    df_isin = df_isin.replace('nan', np.nan)
    df_isin.dropna(inplace=True)

    return df_isin


def get_security_type(openfigi_url, openfigi_key, isins, exchange_code):
    ''' 
    Return security types for each ISIN fed to the openfigi API.
    Parameters:
        openfigi_url     : String containing Openfigi API GET url
        openfigi_key     : String containing Openfigi API key
        isins            : List containing target ISINs
        exchange_code    : Exchange code which the security is traded
    Return:
        security_types   : Numpy array containing the security types of the target ISINs
    ''' 
    # Initialise parameters #
    headers = {'Content-Type':'text/json', 'X-OPENFIGI-APIKEY':openfigi_key}
    payload = json.dumps([{"idType":"ID_ISIN", "idValue": "%s" % isin, "exchCode": "%s" % exchange_code} 
                            for isin in isins])
    
    # Send POST request for each isin and exchange_code, and retrieve relevant details #
    response = requests.post(openfigi_url, data=payload, headers=headers)
    try: 
        response.json()
    except Exception as e:
        print(e, isins)
    else:
        keys = [key for res in response.json() for key in res.keys()]
        error_keys = [seq for seq, key in enumerate(keys) if 'error' in key]
        if error_keys:
            ticker = [res['data'][0]['ticker'] 
                      if not i in error_keys 
                      else np.nan 
                      for i, res in enumerate(response.json())]
            security_type = [res['data'][0]['securityType'] 
                             if not i in error_keys 
                             else np.nan 
                             for i, res in enumerate(response.json())]
            security_type2 = [res['data'][0]['securityType2'] 
                              if not i in error_keys 
                              else np.nan 
                              for i, res in enumerate(response.json())]
        else:
            ticker = [res['data'][0]['ticker'] for res in response.json()]
            security_type = [res['data'][0]['securityType'] for res in response.json()]
            security_type2 = [res['data'][0]['securityType2'] for res in response.json()]
            
        security_types = np.hstack((np.array([isins]).T,
                                    np.array([ticker]).T,
                                    np.array([security_type]).T,
                                    np.array([security_type2]).T))
        
    return security_types     
        

def get_threaded_res(func, max_workers, *args):
    ''' 
    Return threaded responses for each function and input parameters.
    Parameters:
        func        : Function that will be executed in the threads
        max_workers : Number of threads
        *args       : Other input arguments
    ''' 
    # Initiate parameters #
    ## List containing get_full_companies_sgx parameters ##
    get_full_companies_sgx_list = ['browser', 'company', 'sgx_root_website']
    ## List containing download_reports parameters ##
    get_reports = ['web_url', 'download_path', 'df_reports']
    ## List containing get_voting_rights parameters ##
    get_votes = ['report_name', 'download_path', 'nlp', 'nlp_lang', 'bigram_vec', 'stock_patterns']
    ## List containing the target func's parameters ##
    func_params = inspect.getfullargspec(func)[0]
    
    # Validate function #            
    if len(set(get_full_companies_sgx_list).intersection(func_params)) == len(get_full_companies_sgx_list):
        ## Initialise parameters ##
        ### New arguments ###
        sgx_announcements = np.empty((0,4), str) 
        ### Original arguments ###
        browsers = args[0]
        companies = args[1]
        sgx_root_website = args[-1]
        
        ## Separate companies into batches of 10 ##
        batched_companies = chunks(companies, 10)
        
        ## Declare partial function ##
        func_get_urls = partial(func, sgx_root_website = sgx_root_website)
        
        ## Loop through each batch of company names and get associated report weblinks ## 
        with ThreadPoolExecutor(max_workers = max_workers) as executor:
            for batched_company in batched_companies:
                results = executor.map(func_get_urls, browsers, batched_company)
                for result in results:
                    if result.size:
                        sgx_announcements = np.vstack((sgx_announcements, result))
        
        return sgx_announcements 
    
    elif len(set(get_reports).intersection(func_params)) == len(get_reports):
        ## Initialise parameters ##
        web_urls = args[0]
        download_path = args[1]
        df_reports = args[-1]
        
        ## Declare partial function ##
        func_get_reports = partial(func, download_path = download_path, df_reports = df_reports)
        
        ## Loop through each web_url and download the relevant reports ##
        with ThreadPoolExecutor(max_workers = max_workers) as executor:
            executor.map(func_get_reports, web_urls)
    
    elif len(set(get_votes).intersection(func_params)) == len(get_votes):
        ## Initialise parameters ##
        ### New arguments ###
        vote_array = np.empty((0,5), str)
        ### Original arguments ###
        reports = args[0]
        download_path = args[1]
        nlp = args[2]
        nlp_lang = args[3]
        bigram_vec = args[-2]
        stock_patterns = args[-1]
        
        ## Declare partial function ##
        func_get_votes = partial(func, 
                                 download_path = download_path, 
                                 nlp = nlp,
                                 nlp_lang = nlp_lang,
                                 bigram_vec = bigram_vec,
                                 stock_patterns = stock_patterns)
        
        ## Loop through each web_url and download the relevant reports ##
        with ThreadPoolExecutor(max_workers = max_workers) as executor:
            results = executor.map(func_get_votes, reports)
            ### Write to files ###
            results = [result for result in results if result is not None]
            if results:
#                arrs = [arr for arrs, outcome in results for arr in arrs if len(arr)==5]
#                np.savetxt(download_path + '\\' + 'SUCCESS.txt', tuple(arrs), fmt='%s', delimiter='|')
                errata = [np.array([outcome]) for arrs, outcome in results for arr in arrs if len(arr)!=5]
                np.savetxt(download_path + '\\' + 'ERRATA.txt', tuple(errata), fmt='%s')
                logs = [np.array([outcome]) for arrs, outcome in results]
                np.savetxt(download_path + '\\' + 'STATUS.txt', tuple(logs), fmt='%s')
                ### Append to existing vote_array ###
                for result, outcome in results:
                    if result.shape[-1] == 5:
                        vote_array = np.vstack((vote_array, result))
        
        return vote_array
        
    else:
        raise ValueError('Unknown function and/or arguments declared. Please verify.')
        

def get_voting_order(stock_indicator, vote_indicator):
    ''' 
    Pair potential stocks and voting information in chronological sequence. 
    Parameters:
        stock_indicator  : List containing potential stocks
        vote_indicator   : List containing voting information
    Return:
        vote_array       : Numpy array containing class stocks and voting rights information
    '''  
    # Initialise parameter #
    vote_array = np.empty((0,3), str)
    
    # Check if stock_indicator == vote_indicator, i.e. one-to-one relationship # 
    ## One-to-one relationship ## 
    if len(stock_indicator) == len(vote_indicator):
        ### Apply First-In-First-Out (FIFO) for stock-vote assignment ###
        vote_array = np.array([[stock_indicator[i][0], vote_indicator[i][0], stock_indicator[i][-1]]
                                 for i in range(len(stock_indicator))
                                 if stock_indicator[i][-1] == vote_indicator[i][-1]])
    ## Other relationships ##
    else:
        ### Vectorise stock_indicator and vote_indicator ###
        stocks = np.array(stock_indicator)
        votes = np.array(vote_indicator)
        ### Match stock-vote for many-to-one relationships ### 
        if len(stock_indicator) > len(vote_indicator):
            for i in range(len(stock_indicator)):
                vote_array = np.vstack((vote_array,
                                        np.hstack((np.array([[stocks[i,0]]]), 
                                                   votes[np.where(stocks[i,-1] == votes[:,-1])]))))      
        ### Match stock-vote for one-to-many relationship ###
        else:
            for i in range(len(vote_indicator)):
                vote_array = np.vstack((vote_array,
                                        np.hstack((stocks[np.where(votes[i,-1] == stocks[:,-1]), 0].T, 
                                                   np.tile(votes[np.where(votes[i,-1] == stocks[:,-1])],
                                                           (len(stocks[np.where(votes[i,-1] == stocks[:,-1]), 0]), 1)))))) 
        ### Remove duplicate rows in vote_array ###
        vote_array = np.unique(vote_array, axis=0)
    
    return vote_array


def get_voting_paragraphs(text, stock_patterns):
    '''
    Preprocess the text into paragraphs and shortlist those that contain voting information.
    Parameter:
        text               : Text extracted from the PDF report
        stock_patterns     : List containing the common stock naming conventions 
    Return:
        voting_paragraphs  : List containing paragraphs that contain voting information
    '''
    # Initialise parameter #
    ## List containing the paragraphs that contain voting information ##
    voting_paragraphs = []
    
    # Tokenize the extracted text into paragraphs #
    ## Split the text into paragraphs ##
    paragraphs = re.split(r'(\.\n[A-Z])', text)
    if len(paragraphs) > 1:
        paragraphs = [re.split(r'\n', paragraph)[-1] + paragraphs[seq+1] + '.' 
                      for seq, paragraph in enumerate(paragraphs) 
                      if re.search(r'\.\n[A-Z]', paragraph)]
    ## Replace all tabs and new lines as spaces in paragraphs ##
    paragraphs = [re.sub(r'\t|\n', ' ', paragraph) for paragraph in paragraphs]
    ### Remove redundant spaces in paragraphs ###
    paragraphs = [re.sub(r'\s{2,}', ' ', paragraph).strip() for paragraph in paragraphs]
    
    # Preprocess each paragraph #
    ## Remove irrelevant words and change paragraphs to lower case ##
    paragraphs = [re.sub(r'\(cid:\d+\)', '', paragraph.lower()) for paragraph in paragraphs]
    ## Remove security exclusions ##
    paragraphs = [re.sub(r'\(exclud\w+(\s\w+)+\)', '', paragraph) for paragraph in paragraphs]
    paragraphs = [re.sub(r'exclud\w+(\s\w+)+ shares?', '', paragraph) for paragraph in paragraphs]     
    ## Reduce target words, e.g. voting, to its root form, i.e. vote ## 
    paragraphs = [re.sub(r'\bvotes\b', 'vote', paragraph) for paragraph in paragraphs]
    ## Standardise the case for the phrase 'non-voting' ##
    paragraphs = [re.sub(r'[Nn]on-[Vv]ot[a-z]+', 'non-voting', paragraph) for paragraph in paragraphs]
    paragraphs = [re.sub(r'[Nn]on[Vv]ot[a-z]+', 'non-voting', paragraph) for paragraph in paragraphs]
    ## Identify potential non-voting stocks and translate their voting power to 0 votes ##
    paragraphs = [re.sub(r'no voting (rights?|power)', 'non-voting', paragraph) for paragraph in paragraphs]
    paragraphs = [re.sub(r'do not(?:\s\w+) any \bvot[a-z]+\b(?:\srights?)?', 'are non-voting', paragraph)
                  for paragraph in paragraphs]
    paragraphs = [re.sub(r'\w+ not (\w+\s)?entitled? to vote', 'non-voting', paragraph) for paragraph in paragraphs]
    paragraphs = [re.sub(r'\w+ not? (\b\w+\b\s)+right to (\b\w+\b\s)+vote', 'non-voting', paragraph) for paragraph in paragraphs]
    paragraphs = [re.sub(r'\w+ not? (\b\w+\b\s)+right to (\b\w+\b\s)?vote', 'non-voting', paragraph) for paragraph in paragraphs]
    ## Remove redundant spaces ##
    paragraphs = [re.sub(r'\s{2,}', ' ', paragraph).strip() for paragraph in paragraphs]
#    ## Standardise the phrases: (1) voting rights and (2) voting power as votes collectively ##
#    paragraphs = [re.sub(r'voting rights?', 'vote', paragraph) for paragraph in paragraphs]
#    paragraphs = [re.sub(r'voting power', 'vote', paragraph) for paragraph in paragraphs]
        
    # Identify paragraphs that contain potential stock and its voting power #
    ## Shortlist paragraphs that contain the word 'vote' and 'non-voting' ##
    vote_paragraphs = [paragraph for paragraph in paragraphs if re.search(r'vote|non-voting', paragraph)]
    ## Capitalise and capture the potential class of stocks in vote_paragraph ##
    for vote_paragraph in vote_paragraphs:
        for match in re.finditer('|'.join(stock_patterns), vote_paragraph):
            pattern = match.group(0)
            if pattern is not None:
                vote_paragraph = re.sub(pattern, pattern.title(), vote_paragraph)
        voting_paragraphs.append(vote_paragraph)
    
    return voting_paragraphs
        

def get_voting_rights(report_name, download_path, nlp, nlp_lang, bigram_vec, stock_patterns):
    '''
    Extract voting rights from PDF reports downloaded from SGX.
    Parameters:
        report_name     : Name of PDF report
        download_path   : String containing the local download filepath
        nlp             : Natural Language Processing Model - Voting Rights
        nlp_lang        : Natural Language Processing Model - English Dictionary
        bigram_vec      : Bigram Vectoriser
        stock_patterns  : List containing the common stock naming conventions 
    Return:
        vote_array      : Numpy array containing the securities and associated voting information
    '''
    # Initialise parameters #
    ## Text String ##
    text = ''
    ## String containing the company name ##
    company = report_name.split('_')[0]
    
    # Parse PDF reports and extract voting rights information #
    ## Extract the text for all pages ##
    try:
        with pdfplumber.open(download_path + '\\' + report_name) as pdf:
            for page_num in range(1, pdf.pages[-1].page_number):
                try:
                    text += pdf.pages[page_num].extract_text()
                except Exception:
                    pass
    except Exception as e:
        outcome = '%s: %s' % (str(e), company)
        print(outcome)
        return np.empty((0,3), str), outcome
    else:
        ## Tokenize the text into paragraphs and identify those with voting information ##
        voting_paragraphs = get_voting_paragraphs(text, stock_patterns)
        voting_paragraphs = [paragraph
                             for voting_paragraph in voting_paragraphs
                             for paragraph in corpus_splitter(voting_paragraph, 100000)]
        
        ## Apply NLP to paragraph to identify potential stocks and voting rights ##
        if voting_paragraphs:
            stock_indicator, vote_indicator = nlp_extraction(text, voting_paragraphs, nlp, nlp_lang, bigram_vec)
        else:
            stock_indicator = vote_indicator = []       
        ## Remove all irrelevant/duplicated securities and voting rights ##
        if stock_indicator:
            stock_indicator = [[re.sub(r'(\bShare\b|\bUnit\b)',
                                       r'\1' + 's', 
                                       security), corpus] 
                                for security, corpus in stock_indicator]
            stock_indicator = list(map(list,toolz.unique(map(tuple, stock_indicator))))
            vote_indicator = list(map(list,toolz.unique(map(tuple, vote_indicator))))
            stock_corpus = [corpus for security, corpus in stock_indicator]
            vote_indicator = [[num_vote, corpus] for num_vote, corpus in vote_indicator if corpus in stock_corpus]    
            #### Pair potential stocks and voting information in chronological sequence ####
            try:
                vote_array = get_voting_order(stock_indicator, vote_indicator)
            except Exception as e:
                if len(stock_indicator) == 0 and len(vote_indicator) == 0:
                    outcome = 'No stock and voting rights: %s' % company
                elif len(stock_indicator) == 0:
                    outcome = 'No stock identified: %s' % company
                elif len(vote_indicator) == 0:
                    outcome = 'No voting rights: %s' % company
                else:
                    outcome = '%s: %s' % (str(e), company)
                print(outcome)
                return np.empty((0,3), str), outcome
            else:
                if vote_array.shape[0] > 0:
                    #### Perform final cleansing ####
                    vote_array = nlp_cleansing(vote_array, company, nlp, nlp_lang, stock_patterns)
                    #### Append company and filing details to vote_array #### 
                    other_details = np.tile(np.array([[company, 
                                                       dt.datetime.strptime(re.sub('.pdf', '', report_name.split('_')[-1]), '%Y%m%d')]]),
                                            (len(vote_array), 1))
                    vote_array = np.hstack((other_details, vote_array)) 
                    outcome = 'Parsing completed: %s' % company
                else:
                    outcome = 'No voting rights: %s' % company
                #### Print status message ####
                print(outcome)

                return vote_array, outcome
        else:
            outcome = 'No voting rights: %s' % company
            print(outcome)
            return np.empty((0,3), str), outcome


def isposfloat(value):
    ''' 
    Test whether a value is a positive float.
    Parameter:
        value        : Input value
    Return:
        True/False   : Boolean indicating whether the input value is a positive float
    ''' 
    try:
        if float(value) >= 0:
            return True
        else:
            return False 
    except ValueError:
        return False        
        
    
def nlp_cleansing(vote_array, company, nlp, nlp_lang, stock_patterns):
    '''
    Performs final round of cleansing of securities and voting rights.
    Parameters:
        vote_array     : Numpy array containing raw class stocks and voting rights information
        company        : String containing company name
        nlp            : Natural Language Processing Model - Voting Rights
        nlp_lang       : Natural Language Processing Model - English Dictionary
        stock_patterns : List containing the common stock naming conventions 
    Return:
        final_array    : Numpy array containing finalised class stocks and voting rights information
    '''
    # Initiate parameters #
    ## Empty array ##
    final_array = np.empty((0,3), str)
    
    ## Standardise security names and remove duplicates ##
    ### Update Common/Equity as Ordinary ###
    vote_array[:,0] = [re.sub(r'\bCommon\b|\bEquity\b', 'Ordinary', element) 
                        for element in list(vote_array[:,0])]
    ### Update Stock as Shares ###
    vote_array[:,0] = [re.sub(r'\bStock\b', 'Shares', element) 
                        for element in list(vote_array[:,0])]
    ### Update Shares as Units for Trust Funds ###
    if re.search(r'\bTRUSTS?\b|REIT', company):
        vote_array[:,0] = [re.sub(r'\bShares?\b', 'Units', element) 
                            for element in list(vote_array[:,0])]
    ### Remove redundant/duplicate securities ###
    securities_list = list(vote_array[:,0])
    if 'Stapled Security' in securities_list and 'Ordinary Units' in securities_list:
        vote_array = vote_array[vote_array[:,0] != 'Ordinary Units']
    elif 'Stapled Security' in securities_list and 'Ordinary Shares' in securities_list:
        vote_array = vote_array[vote_array[:,0] != 'Ordinary Shares']    
    elif 'Ordinary Units' in securities_list and 'Ordinary Shares' in securities_list:
        vote_array = vote_array[vote_array[:,0] != 'Ordinary Shares']    
    
    # Loop through each row #
    for row in vote_array:
        ## Perform validation checks on Ordinary Shares with zero-voting rights ##
        if (row[0] == 'Ordinary Shares' or row[0] == 'Ordinary Units') and row[1] == '0.0':
            ## Perform sentence tokenisation and query the correct security ##
            doc = nlp_lang(''.join(list(row[-1])))
            for sentence in doc.sents:
                pattern = [match.group(0) for match in re.finditer('|'.join(stock_patterns), sentence.text)]
                if pattern and re.search(r'[Nn]on-?[Vv]oting', sentence.text):
                    ### Append results in final_array ###
                    final_array = np.vstack((final_array, 
                                             np.array([[re.sub(r'(\bShare\b|\bUnit\b)',
                                                               r'\1' + 's', 
                                                               pattern[-1]), row[1], row[-1]]])))
        ## Perform validation checks on Preference Shares with one voting right ##
        elif re.search(r'[Ss]eries|[Pp]refer[a-z]*', row[0]) and row[1] == '1.0':
            ## Perform sentence tokenisation ##
            doc = nlp_lang(''.join(list(row[-1])))
            sentences = [sentence.text for sentence in doc.sents]
            ## Apply NLP model ##
            corpora = [nlp(sentence) for sentence in sentences]
            ## Filter those that meet conditions ##
            pattern = [[word2num(ent.text.lower(), 'votes'), corpus.text, row[-1]] 
                        for corpus in corpora 
                        for ent in corpus.ents 
                        if ent.label_ == 'VOTE_POWER' and \
                        re.search('vote|non-voting', ent.text.lower()) and \
                        not re.search(re.sub('\*', r'\*', ent.text.lower()) + r'\s?[:\?]', corpus.text) and \
                        word2num(ent.text.lower(), 'votes') is not None]
            pattern = [[num_vote, corpus] 
                        for num_vote, sentence, corpus in pattern 
                        if re.search(r'[Ss]eries|[Pp]refer[a-z]*', sentence)]          
            if pattern:
                ### Append results in final_array ###
                final_array = np.vstack((final_array, np.array([row])))
        else:
            final_array = np.vstack((final_array, np.array([row])))  
        
    return final_array
    

def nlp_extraction(text, voting_paragraphs, nlp, nlp_lang, bigram_vec, encoding='utf-8'):
    '''
    Extracts potential securities and voting rights from corpus
    Parameters:
        text              : Extracted text from PDF report
        voting_paragraphs : List containing all paragraphs that contain voting rights information
        nlp               : Natural Language Processing Model - Voting Rights
        nlp_lang          : Natural Language Processing Model - English Dictionary
        bigram_vec        : Bigram vectoriser
        encoding          : Encoding information. Default value is utf-8
    Return:
        potential_stock   : Temporary list containing all potential securities
        vote_indicator    : Finalised list containing voting rights informtion
    '''
    # Initiate parameters #
    ## Temporary list containing all potential voting rights ##
    potential_vote = []
    ## Finalised list containing all potential voting rights ##
    vote_indicator = []
    
    # Apply NLP to paragraph to identify potential stocks and voting rights #
    ## Apply NLP model ##
    corpora = [nlp(voting_paragraph) for voting_paragraph in voting_paragraphs]
    ## Capture voting rights via NLP extraction ##
    vote_indicator = [[word2num(ent.text.lower(), 'votes'), 
                       corpus.text.encode(encoding,'replace').decode(encoding),
                       ent.text.lower()] 
                        for corpus in corpora 
                        for ent in corpus.ents 
                        if ent.label_ == 'VOTE_POWER' and \
                        re.search('vote|non-voting', ent.text.lower()) and \
                        not re.search(re.sub('\*|\(|\)', '', ent.text.lower()) + r'\s?[:\?]', corpus.text) and \
                        word2num(ent.text.lower(), 'votes') is not None]
    ## Enrich vote_indicator with the voting rights information extracted from Regular Expression ##
    more_votes = [[word2num(match, 'votes'), 
                   voting_paragraph.encode(encoding,'replace').decode(encoding),
                   match] 
                    for voting_paragraph in voting_paragraphs 
                    for match in re.findall(r'\(?\d+\)? vote', voting_paragraph)
                    if re.search(r'\(?\d+\)? vote', voting_paragraph) and \
                    not re.search(r'\(?\d+\)? vote\s?[:\?]', voting_paragraph) and \
                    not int(re.sub(' vote', '', match)) in range(dt.datetime.now().year-2, dt.datetime.now().year+1)]
    vote_indicator += more_votes
    
    # Apply Bigram Vectoriser to identify voting rights if NLP and Regular Expression fail #
    if not vote_indicator:
        for voting_paragraph in voting_paragraphs:
            ## Fit and apply bigram vectoriser to each voting_paragraph ##
            bigram_vec.fit_transform([voting_paragraph])    
            ## Return the list of bigrams ##
            bigrams = bigram_vec.get_feature_names()
            ## Narrow down possible voting sentences using parts-of-speech tags ##
            pos_tags = [[(token.text, token.pos_) for token in nlp_lang(bigram)] 
                        for bigram in bigrams]
            for pos_tag in pos_tags:
                if pos_tag[-1][0].lower() == 'vote' and \
                   pos_tag[-1][-1] == 'NOUN' and \
                   pos_tag[0][-1] == 'NUM':
                       vote_indicator.append([word2num(pos_tag[0][0] + ' ' + pos_tag[-1][0], 'votes'), 
                                              voting_paragraph.encode(encoding,'replace').decode(encoding),
                                              pos_tag[0][0] + ' ' + pos_tag[-1][0]])
                elif pos_tag[-1][0] == 'voting' and \
                     pos_tag[-1][-1] == 'NOUN' and \
                     pos_tag[0][0] == 'non':
                         vote_indicator.append([word2num(pos_tag[0][0] + '-' + pos_tag[-1][0], 'votes'), 
                                              voting_paragraph.encode(encoding,'replace').decode(encoding),
                                              pos_tag[0][0] + '-' + pos_tag[-1][0]])
    
    # Remove erroneous voting rights information #
    ## Invalid voting rights, e.g. nan voting rights ##
    vote_indicator = [[num_vote, corpus, phrase] 
                       for num_vote, corpus, phrase in vote_indicator 
                       if isposfloat(num_vote)]
    ## Voting rights for treasury shares ##
    for num_vote, corpus, phrase in vote_indicator:
        ### Validate whether there are any outstanding securities with zero voting rights ###
        if num_vote == 0 and not re.search(r'no share|no outstanding|not? issued', corpus):
            #### Perform sentence tokenisation ####
            doc = nlp_lang(corpus)
            sentences = [sent.text for sent in doc.sents]
            #### Validate whether treasury shares are present in the corpus ####
            treasury_share_indicator = [sentence for sentence in sentences
                                        if re.search('treasury share', sentence) and \
                                           re.search(r'zero vote|no vote|non-voting', sentence)]
            #### Append num_vote, corpus and phrase to potential_vote 
            ### if treasury shares are not present and all securities are outstanding ####
            if not treasury_share_indicator:
                potential_vote.append([num_vote, corpus, phrase])
        ### Append num_vote, corpus and phrase to potential_vote if there are no zero voting rights ###
        elif num_vote > 0:
            potential_vote.append([num_vote, corpus, phrase])
    ## Assign potential_vote back to vote_indicator ##
    vote_indicator = potential_vote
    
    # Apply NLP and Regular Expression to identify potential stocks in vote corpus ##  
    stock_indicator = nlp_securities(nlp_lang, vote_indicator, text)

    # Omit phrase in vote_indicator #
    if vote_indicator:
        vote_indicator = [[num_vote, corpus] for num_vote, corpus, phrase in vote_indicator]
        
    return stock_indicator, vote_indicator


def nlp_securities(nlp_lang, vote_indicator, text):
    '''
    Apply NLP to perform sentence tokenisation and security recognition.
    Parameters:
        nlp_lang        : Natural Language Processing Model - English Dictionary
        vote_indicator  : Finalised list containing voting rights informtion
    Return:
        stock_indicator : List containing the confirmed securities
    '''
    # Initialise parameters #
    stock_pattern = []
    stock_indicator = []
    
    # Loop through each corpus in vote_indicator #
    for num_vote, corpus, phrase in vote_indicator:
        ## Apply sentence tokenisation ##
        doc = nlp_lang(corpus)
        sentences = [sent.text for sent in doc.sents]
        ## Identify potential stocks ##
        for sentence in sentences:
            pattern = [match.group(0) for match in re.finditer('|'.join(stock_patterns), sentence)]
            if pattern:
                stock_pattern.append([pattern[-1], sentence])
        ## Shortlist the potential stock based on (limited) information present in vote_sentences ##
        vote_sentences = [sentence for sentence in sentences if re.search(phrase, sentence)]
        if stock_pattern:
            for security, sentence in stock_pattern:
                for vote_sentence in vote_sentences:
                    if re.search(security.lower(), vote_sentence.lower()):
                        stock_indicator.append([security, corpus])
                    elif re.search(r'(\b[A-Za-z]+[Cc][Pp][Ss]\b)', vote_sentence):
                        stock_indicator.append(['Preference Shares', corpus])
                    elif not re.search(r'[Nn]on-?[Vv]oting', vote_sentence) and \
                        re.search(r'\b[Ss]tapled [Ss]ecurit\w+\b', vote_sentence):        
                        stock_indicator.append(['Stapled Security', corpus])    
                    elif not re.search(r'[Nn]on-?[Vv]oting', vote_sentence) and \
                        re.search(r'\bunits?\b|unitholder', vote_sentence):
                        stock_indicator.append(['Ordinary Units', corpus])
                    else:
                        security_segments = security.lower().split()
                        for security_segment in security_segments:
                            if security_segment in vote_sentence:
                                stock_indicator.append([security, corpus])
        else:
            for vote_sentence in vote_sentences:
            ### Check whether it is Unit Trust ###
                if re.search(r'\b[Ss]tapled [Ss]ecurit\w+\b', vote_sentence):
                    stock_indicator.append(['Stapled Security', corpus])
                elif re.search(r'\bunits?\b|unitholder', vote_sentence):
                    stock_indicator.append(['Ordinary Units', corpus])
                elif re.search(r'(\b[A-Za-z]+[Cc][Pp][Ss]\b)', vote_sentence):
                    stock_indicator.append(['Preference Shares', corpus])
                else:
                    ### Search the rest of the text for potential stock ###
                    pattern = [match.group(0) for match in re.finditer('|'.join(stock_patterns), text)]
                    if pattern:
                        stock_indicator.append([pattern[-1].title(), corpus])
    
    # Remove duplicate securities #
    if stock_indicator:
        stock_indicator = [[re.sub(r'(\b[A-Za-z]+[Cc][Pp][Ss]\b)', 'Preferred Shares', security), corpus]
                            for security, corpus in stock_indicator]
        
    return stock_indicator


def review_reports(reports, download_path, df_reports):
    '''
    Scans the PDF report for parsing errors and re-downloads the relevant report.
    Parameters:
        reports          : List containing the names of all Annual & Sustainability Reports 
        download_path    : String containing the local download filepath
        df_reports       : DataFrame containing all the companies and their related announcements 
    '''       
    # Loop through each file in download_path #
    for report in reports:
        # Initialise counter #
        repeat_counter = True
        while repeat_counter:
            try:
                ## Parse PDF Report ##
                pdf = pdfplumber.open(download_path + '\\' + report)
            except Exception as e:
                ## Re-download the report if there are parsing errors ##
                ### Initialise parameters ###
                company = re.split(r'_[AS]R_', report)[0]
                report_type = re.sub('_', '', re.search(r'_([AS]R)_', report).group(0))
                filing_date = re.sub('.pdf', '', re.split(r'_[AS]R_', report)[-1])
                if report_type == 'AR':
                    url = df_reports.loc[(df_reports['COMPANY']==company) & 
                                         (df_reports['REPORT']=='Annual Report'), 
                                         'WEBLINK'].iloc[0]
                else:
                    url = df_reports.loc[(df_reports['COMPANY']==company) & 
                                         (df_reports['REPORT']=='Sustainability Report'), 
                                         'WEBLINK'].iloc[0]
                ### Print name of errorneous report ###
                print(company, report_type, e)
                ### Send GET request ###
                response = requests.get(url)
                ### Parse GET request to obtain the actual report weblink ###
                soup = BeautifulSoup(response.text)
                web_url = 'https://links.sgx.com' + soup.find('a')['href']
                ### Re-download report ###
                urllib.request.urlretrieve(web_url, 
                                           download_path + '\\%s_%s_%s.pdf' % (company, report_type, filing_date))
            else:
                ## Terminate all processes ##
                ### Close the pdf ###
                pdf.close()
                ### Print status message ###
                print('%s is ready to parse.' % report)
                ### Exit the while loop ###
                repeat_counter = False
    
    
def start_homepage(log_path, download_path=None, mime=None):
    ''' 
    Initiate a Firebox browser session.
    Parameters:
        log_path      : String containing the filepath of the geckodriver log file
        download_path : String containing the local download filepath.
                        Default value is None
        mime          : Mime file type(s) that would be downloaded.
                        Default value is None
    Return:
        browser       : Instantiated Firefox browser
    '''
    # Initiate a Firefox browser session #
    profile = webdriver.FirefoxProfile()
    profile.set_preference('accessibility.blockautorefresh', True)
    profile.set_preference('network.http.response.timeout', 10000)
    profile.native_events_enabled = True
    if download_path != None:
        profile.set_preference('browser.download.folderList', 2)
        profile.set_preference('browser.download.dir', download_path)
        profile.set_preference('pdfjs.disabled', True)
        if mime != None:
            profile.set_preference('browser.helperApps.neverAsk.saveToDisk', mime)
        else:
            raise ValueError('Please provide MIME type in the argument!')
                       
    options = Options()
    options.add_argument('--headless')
    
    browser = webdriver.Firefox(firefox_profile=profile,
#                                options=options,
                                service_log_path=log_path)
    
    return browser


def word2num(textnum, conversion, numwords = {}):
    ''' 
    Convert words to numbers.
    Parameters:
        textnum              : Numbers that are spelt out in text
        conversion           : Share data that we are converting, e.g. shares, votes etc.
        numwords             : Dictionary containing the scales and increments
    Return:
        result+current       : Numbers in numerical format after translation
    ''' 
    if not numwords:
        # Initialise whole numbers #
        units = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', \
                 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
        tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']
        scales = ['hundred', 'thousand', 'million', 'billion', 'trillion']
        
        # Initialise other common vocabulary #
        if conversion.lower() == 'shares':
            numwords['no'] = (1, 0)
        
        elif conversion.lower() == 'votes':
            numwords['no vote'] = numwords['non-voting'] = \
                                  numwords['Non-Voting'] = \
                                  numwords['Nonvoting'] = \
                                  numwords['Non voting'] = \
                                  numwords['nonvoting'] = \
                                  numwords['non voting'] = \
                                  (1, 0)
        
        # Create scale and increment and store in dictionary: numwords #
        for idx, word in enumerate(units):  
            numwords[word] = (1, idx)
        for idx, word in enumerate(tens):       
            numwords[word] = (1, idx * 10)
        for idx, word in enumerate(scales): 
            numwords[word] = (10 ** (idx * 3 or 2), 0)
    
    # Initialise fraction dictionary: ordinal_words #
    ordinal_words = {'half': float(1/2), 'third': float(1/3), 'fourth': float(1/4), 'quarter': float(1/4), 'fifth': float(1/5), 
                     'sixth': float(1/6), 'seventh': float(1/7), 'eighth': float(1/8), 'ninth': float(1/9), 
                     'tenth': float(1/10), 'eleventh': float(1/11), 'twelfth': float(1/12), 'thirteenth': float(1/13), 
                     'fourteenth': float(1/14), 'fifteenth': float(1/15), 'sixteenth': float(1/16), 'seventeenth': float(1/17), 
                     'eighteenth': float(1/18), 'nineteenth': float(1/19), 'twentieth': float(1/20), 'hundredth': float(1/100),
                     'thousandth': float(1/1000)}    
    
    # Perform preprocessing #
    ## Replace hyphen in text with space and change the text to lower case ##
    textnum = textnum.replace('-', ' ').lower().strip()
    ## Replace comma in text with space ##
    textnum = textnum.replace(',', '')
    ## Replace with actual vote number in brackets ##
    if re.search(r'\(?\d+\)?', textnum) and re.search('vote', textnum):
        num_vote = re.search(r'\(?\d+\)?', textnum).group(0)
        textnum = re.search(r'\d+', num_vote).group(0) + ' vote'
    
    # Compute the results #
    current = 0 
    result = 0
    counter = 0
    if len(textnum.split()) == 0:
        scale, increment = (0, -np.inf)
    elif len(textnum.split()) == 1 and re.search('vote', textnum):
        scale, increment = (0, -np.inf)
    else:
        counter += 1
        if textnum in numwords:
            scale, increment = numwords[textnum]
        else:
            for seq, word in enumerate(textnum.split()):
                if not re.search(r'\bof\b', word) and not re.search(r'\bvote\b', word):
                    if re.sub(r'(\w+)s\b', r'\1' , word) in ordinal_words:
                        ## Assign scale and increment for fractions ##
                        scale, increment = (ordinal_words[re.sub(r'(\w+)s\b', r'\1' , word)], 0)
                    elif word in numwords:
                        ## Assign scale and increment for decimals and whole numbers ## 
                        if re.search(r'\bof\b', textnum) and seq == len(textnum.split()) - 1:
                            scale, increment = (numwords[word][1], 0)
                        else:
                            scale, increment = numwords[word]
                    ## Assign scale and increment for numerical real numbers ## 
                    elif isposfloat(word):
                        scale, increment = (1, float(word))
                    elif re.search(r'\d+[/]\d+\w*', word):
                        temp = re.search('\d+[/]\d+', word)[0]
                        scale, increment = (0, float(temp.split('/')[0]) / float(temp.split('/')[1]))
                    else:
                        ## Assign scale and increment for invalid word ##
                        scale, increment = (0, -np.inf)
                        #raise Exception('Illegal word: ' + word)
            
                    current = current * scale + increment
                    if scale > 100:
                        result += current
                        current = 0             
                
    if re.search(r'\bof\b', textnum) and float(result + current) == 0:      
        return float(-np.inf)
    elif counter == 0 and float(result + current) == 0:
        return float(-np.inf)
    else:
        return float(result + current)


# Initialise parameters #
## Number of threads ##
max_workers = 10
## Empty array ##
sgx_companies = np.empty((0,4), str)
## String containing the local path to master file ##
master_path = os.environ['USERPROFILE'] + '\\Dropbox\\ESG Research\\Voting Rights\\Master.xlsx'
## String containing the root website address ##
master_file = pd.read_excel(master_path, sheet_name='SECURITIES')
sgx_root_website = master_file.loc[master_file['COUNTRY']=='Singapore', 'WEBLINK'].iloc[0]
## String containing the local download filepath ##
download_path = os.environ['USERPROFILE'] + master_file.loc[master_file['COUNTRY']=='Singapore', 'LOCAL_DOWNLOAD_PATH'].iloc[0]
## Openfigi API GET url and key ##
api_keys = pd.read_excel(master_path, sheet_name='API_KEYS')
openfigi_url = api_keys.loc[api_keys['APPLICATION'] == 'openfigi', 'API'].iloc[0]
openfigi_key = api_keys.loc[api_keys['APPLICATION'] == 'openfigi', 'KEY'].iloc[0]
## Natural Language Processing Model ##
nlp_path = os.environ['USERPROFILE'] + '\\Dropbox\\ESG Research\\Voting Rights'
nlp = spacy.load(nlp_path)
nlp_lang = spacy.load('en_core_web_sm')
## Bigram vectoriser ##
bigram_vec = TfidfVectorizer(ngram_range=(2,2))
## List containing the common stock naming conventions ## 
stock_patterns = [r'([Ss]eries|[Cc]lass) ([A-Z0-9])(?:\s[Cc]onvertible)?' + \
                  ' ([Pp]refer[a-z]*|[Cc]ommon)?[\s]?([Ss]hare[s]?|[Ss]tock|[Uu]nit[s]?)?', \
                  r'([Cc]ommon|[Pp]refer[a-z]*) ([Ss]eries)* ([A-Z0-9])(?:\s[Ss]eries)? ([Ss]hare[s]?|[Ss]tock|[Uu]nit[s]?)', \
                  r'([Oo]rdinary|[Cc]ommon|[Ee]quity|[Pp]refer[a-z]*|[Mm]anagement) ([Ss]hare[s]?|[Ss]tock|[Uu]nit[s]?)', \
                  r'(\w+ [Ss]tock [Uu]nit[s]?)', \
                  r'(\b[Ss]tapled [Ss]ecurit\w{1,3}\b)',
                  r'(\b[A-Za-z]+[Cc][Pp][Ss]\b)']
## Dictionary containing the list of errant companies and isins (ascending order) in SGX and SGInvestors respectively ##
errant_companies = {'ERRANT': ['ALITA RESOURCES LIMITED', 'ARION ENTERTAINMENT SPORE LTD', 
                               'CHINA SKY CHEM FIBRE CO., LTD.', 
                               'COURAGE INVESTMENT GRP LIMITED', 'FRASER AND NEAVE, LIMITED', 'INCREDIBLE HOLDINGS LTD.', 
                               'KEPPEL PACIFIC OAK US REIT', 'PAN OCEAN CO., LTD.', 'REENOVA INVESTMENT HOLDING LIMITED', 
                               'REVEZ CORPORATION LTD.', 'SINGTEL', 'UNITED ENGINEERS LTD ORD', 
                               'VIKING OFFSHORE AND MARINE LTD', 'WINAS LIMITED', 
                               'ZHENENG JINJIANG ENV HLDG CO'], 
                    'CORRECT':['ALLIANCE MINERAL ASSETS LIMITED', 'ARION ENTERTAINMENT SINGAPORE LIMITED', 
                               'CHINA SKY CHEMICAL FIBRE CO. LTD.', 
                               'COURAGE MARINE GROUP LIMITED', 'FRASER AND NEAVE LIMITED.', 'VASHION GROUP LTD.', 
                               'KEPPEL-KBS US REIT MANAGEMENT PTE. LTD.', 'PAN OCEAN CO. LTD.', 'ISR CAPITAL LIMITED', 
                               'JASON HOLDINGS LIMITED', 'SINGAPORE TELECOMMUNICATIONS LIMITED', 'UNITED ENGINEERS LIMITED', 
                               'VIKING OFFSHORE AND MARINE LIMITED', 'SINWA LIMITED', 
                               'CHINA JINJIANG ENVIRONMENT HOLDING COMPANY LIMITED']}
errant_isins = {'ERRANT': ['BMG2113G2068', 'BMG6361R1117', 'BMG9884X1020', 'KYG2119H1092', 'KYG3932J1572', 'SGXT31096785'], 
                'CORRECT':['BMG2113G1151', 'BMG6361R2024', 'VGG9889R1001', 'KYG9898S1075', 'KYG4211E1098', 'BMG7354C1038']}

# Launch threaded browsers to SGX home page #
browsers = [start_homepage(log_path=download_path + '\\geckodriver.log',
                           download_path=download_path, 
                           mime='text/html,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') 
            for i in range(max_workers)]

# Get SGX companies' ISINs #
## Download data from SGX and Bloomberg POST API ##
df_isin = get_isin(browsers, openfigi_url, openfigi_key)
## Validate completeness of df_isin against those downloaded from sginvestors ##
df_isin = get_full_companies_sginvestors(download_path, max_workers, df_isin, errant_companies, errant_isins)
                    
# Get SGX companies' report weblinks using company names #
## Get companies who have issued common shares and convert their names into a list ##
companies = list(df_isin['COMPANY'])
## Feed company names into SGX and retrieve announcement weblinks ##
sgx_companies = get_threaded_res(get_full_companies_sgx, max_workers, browsers, companies, sgx_root_website)
## Close browsers ##
[browser.close() for browser in browsers]
## Get the list of intermediate urls containing the latest announcements, i.e. annual & sustainability reports, for each company ##
df_reports = pd.DataFrame(sgx_companies, columns=['COMPANY', 'FILING_DATE', 'REPORT', 'WEBLINK'])
df_reports = df_reports.loc[df_reports['REPORT'].isin(['Annual Report', 'Sustainability Report'])]
df_reports.sort_values(by=['COMPANY','FILING_DATE'], ascending=[True, False], inplace=True)
df_reports.drop_duplicates(subset=['COMPANY', 'REPORT'], inplace=True)
web_urls = list(df_reports['WEBLINK'])

# Download reports into local directory #
## Download the Annual and Sustainability Reports from all target companies ##
get_threaded_res(download_reports, max_workers, web_urls, download_path, df_reports)
### Parse all reports to ensure all of them are error-free ### 
reports = [file for file in os.listdir(download_path) if re.search('.pdf', file)]
review_reports(reports, download_path, df_reports)

# Extract the voting rights information from the reports #
## Start timer ##
start_time = time.time()
## Get the names of all Annual Reports ##
reports = [file for file in os.listdir(download_path) if re.search(r'_AR_|_PROSPECTUS_', file)]
vote_array = get_threaded_res(get_voting_rights, max_workers, reports, download_path, 
                              nlp, nlp_lang, bigram_vec, stock_patterns)
## End timer and print status message ##
end_time = time.time()
print('The extraction process took %s minutes' % str((end_time-start_time)/60))
## Download processed DataFrame ## 
df_securities = arr_convert_df(vote_array, df_isin)
df_securities.to_excel(download_path + '\\SGX_SECURITIES.xlsx', sheet_name='SECURITIES', index=False)