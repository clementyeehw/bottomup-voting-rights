# Import Python libraries #
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor
from functools import partial
import inspect
import numpy as np
import os
import pandas as pd
import re
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import urllib


# Launch BCBA home page with Selenium's webdriver to scrape details #
def arr_convert_df(wtd_url, wtd_key, max_workers, *args):
    ''' 
    Convert Numpy array into Pandas Dataframe.
    Parameters:
        wtd_url       : World Trading Data GET url
        wtd_key       : API key for World Trading Data (WTD)
        max_workers   : Number of threads
        args          : Other input parameters
    Return:
        df_securities : DataFrame containing security-level details
    ''' 
    # Initiate parameters #
    securities = args[0] 
    corporates = args[1]
    subsidiaries = args[2]
    directors = args[-2] 
    shareholders = args[-1]
    
    # Transform numpy arrays into DataFrame #
    df_securities = pd.DataFrame(securities, columns=['TICKER', 'STATUS', 'SECURITY_LUMP', 'TYPE', 'ISIN', 'COMPANY'])
    df_corporates = pd.DataFrame(corporates, columns=['COMPANY', 'CORPORATE_WEBSITE'])
    df_corporates = df_corporates.replace('nan',np.nan)
    df_subsidiaries = pd.DataFrame(subsidiaries, columns=['COMPANY', 'RELATED_COMPANY', 'PERCENTAGE_CONTROL'])
    df_subsidiaries = df_subsidiaries.replace('nan',np.nan)
    df_subsidiaries['PERCENTAGE_CONTROL'] = df_subsidiaries['PERCENTAGE_CONTROL'].astype(float)
    df_directors = pd.DataFrame(directors, columns=['COMPANY', 'POSITION', 'DIRECTOR'])
    df_directors = df_directors.replace('nan',np.nan)
    df_shareholders = pd.DataFrame(shareholders, columns=['COMPANY', 'SECURITY_LUMP'])
    
    # Extract security and voting right details from df_shareholders #
    ## Create additional columns for df_shareholders and extract the relevant details ##
    df_shareholders['SECURITY_TYPE'] = np.nan
    df_shareholders['SECURITY_CLASS'] = np.nan
    df_shareholders['VOTING_RIGHT'] = np.nan
    ### Security Type ###
    df_shareholders.loc[df_shareholders['SECURITY_LUMP'].str.contains('voto'), 'SECURITY_TYPE'] = 'Common Stock'
    ### Security Class ###
    df_labels = df_shareholders['SECURITY_LUMP'].str.extract(r'"\s?(\w+)\s?"|[Cc]lase (\w+)')
    df_labels.loc[df_labels[0].isnull(), 0] = df_labels.loc[df_labels[0].isnull(), 1]
    df_shareholders['SECURITY_CLASS'] = df_labels.loc[:, 0]
    df_shareholders.loc[df_shareholders['SECURITY_LUMP'].str.contains('voto'), 'SECURITY_CLASS'] = \
        'Class ' + df_shareholders.loc[df_shareholders['SECURITY_LUMP'].str.contains('voto'), 'SECURITY_CLASS']
    ### Voting Right ###
    df_labels = df_shareholders['SECURITY_LUMP'].str.extract(r'(\d+)\s?[Vv]oto|([Pp]referidas)')
    ### Assign voting right of preferred stock as 0 ###
    df_labels.loc[(df_labels[0].isnull()) & (df_labels[1] == 'Preferidas'), 0] = 0
    df_shareholders['VOTING_RIGHT'] = df_labels[0]  
    
    ## Query all the securities with voting rights in df_shareholders and reformat into new DataFrame ##
    df_add = df_shareholders.loc[(df_shareholders['SECURITY_CLASS'].notnull()) & \
                                 (df_shareholders['VOTING_RIGHT'].notnull())]
    
    # Extract security and voting right details from df_securities #
    ## Create additional columns for df_securities and extract the relevant details ##
    ### Security Type ###
    df_securities['SECURITY_TYPE'] = np.nan
    df_securities.loc[df_securities['SECURITY_LUMP'].str.contains(r'Ordinarias|Ordinaris', case=False), 'SECURITY_TYPE'] = 'Common Stock'
    df_securities.loc[df_securities['SECURITY_LUMP'].str.contains(r'Preferidas', case=False), 'SECURITY_TYPE'] = 'Preferred Stock'
    ### Security Class ###
    df_labels = df_securities['SECURITY_LUMP'].str.extract(r'"\s?(\w+)\s?"|Clase (\w+)')
    df_labels.loc[df_labels[0].isnull(), 0] = df_labels.loc[df_labels[0].isnull(), 1]
    df_securities['SECURITY_CLASS'] = df_labels.loc[:, 0]
    df_securities.loc[df_securities['SECURITY_CLASS'].notnull(), 'SECURITY_CLASS'] = \
            'Class ' + df_securities.loc[df_securities['SECURITY_CLASS'].notnull(), 'SECURITY_CLASS']
    ### Voting Right ###
    df_labels = df_securities['SECURITY_LUMP'].str.extract(r'(\d+)\s?[Vv]oto|(Preferidas)')
    ### Assign voting right of preferred stock as 0 ###
    df_labels.loc[(df_labels[0].isnull()) & (df_labels[1] == 'Preferidas'), 0] = 0
    df_securities['VOTING_RIGHT'] = df_labels[0]
    
    ## Append the securities and associated voting rights found in df_shareholders to df_securities ##
    df_securities = pd.concat([df_securities, df_add], ignore_index=True, sort=False)
    
    ## Reformat data types in columns ##
    df_securities = df_securities.replace('nan', np.nan)
    df_securities = df_securities.replace('', np.nan)
    df_securities['VOTING_RIGHT'] = df_securities['VOTING_RIGHT'].astype(float)
    df_securities['COMPANY'] = df_securities['COMPANY'].str.replace(r'[\s]{2,}', ' ')
    
    # Extract outstanding shares from WTD and transform into DataFrame #
    ## Transform ticker column into list #
    tickers = list(df_securities['TICKER'].dropna() + '.BA')
    ## Separate the tickers into batches of 5 (Note: Upper request limit set by WTD) ##
    chunked_tickers = chunks(tickers, 5)
    ticker_strings = [','.join(chunked_ticker) for chunked_ticker in chunked_tickers]
    ## Create payloads ##
    payloads = [{"symbol": ticker_string, "api_token" : wtd_key} 
                for ticker_string in ticker_strings]
    ## Feed tickers into WTD API ##
    outstanding_shares = get_threaded_res(get_outstanding_shares, max_workers, payloads, wtd_url)
    ## Transform array into DataFrame and reformat each data column in df_outstanding ##
    df_outstanding = pd.DataFrame(outstanding_shares, columns = ['TICKER', 'COMPANY_NAME', 'NUM_OF_SHARES'])
    errata_index = df_outstanding.loc[df_outstanding['NUM_OF_SHARES'] == 'N/A'].index
    df_outstanding.drop(errata_index, inplace=True)
    df_outstanding['TICKER'] = df_outstanding['TICKER'].str.replace('.BA', '')
    df_outstanding['COMPANY_NAME'] = df_outstanding['COMPANY_NAME'].str.upper()
    df_outstanding['NUM_OF_SHARES'] = df_outstanding['NUM_OF_SHARES'].astype(float)
    
    # Merge df_outstanding with df_securities and output necessary columns #
    ## Merge df_outstanding with df_securities ##
    df_securities = pd.merge(df_securities,
                             df_outstanding,
                             how = 'left',
                             on = 'TICKER')
    ## Output necessary columns ##
    df_securities = df_securities[['COMPANY', 'TICKER', 'ISIN', 
                                   'SECURITY_TYPE', 'SECURITY_CLASS', 'NUM_OF_SHARES',
                                   'VOTING_RIGHT']]
    df_securities.sort_values(by=['COMPANY', 'SECURITY_TYPE', 'SECURITY_CLASS'], inplace=True)
    df_securities.drop_duplicates(inplace=True)
    
    return df_securities, df_corporates, df_subsidiaries, df_directors
    

def chunks(lst, n):
    '''
    Generate successive n-sized chunks from list.
    Parameters:
        lst           : List containing the elements to be broken into chunks
        n             : Size of each chunk
    Return:
        chunked_list  : List containing n-sized chunks
    '''
    chunked_list = [lst[i:i + n] for i in range(0, len(lst), n)]
    
    return chunked_list


def get_company_details(browsers, max_workers, root_url, download_path):
    ''' 
    Navigate to relevant BCBA webpages and scrape relevant company details.
    Parameters:
        browsers      : List of Selenium Firefox browsers
        max_workers   : Number of threads
        root_url      : String containing BCBA root website address
        download_path : String containing the local download filepath 
    Return:
        securities    : Numpy array containing security-level details
        corporates    : Numpy array containing company-level details
        subsidiaries  : Numpy array containing subsidiary-level details
        directors     : Numpy array containing director-level details
        shareholders  : Numpy array containing shareholder-level details
    '''          
    # Initiate parameters #
    ## Url containing the overview of all security details ##
    bcba_sec_overview_url = root_url + '/Investigaciones/Especies.aspx'
    ## Url containing the overview of all company details ##
    bcba_company_overview_url = root_url + '/Sociedades/BusquedaFichaTecnica.aspx'
    ## Integer containing the lead time used to complete webpage loading ##
    delay = 30
    ## Firefox browser ##
    browser = browsers[0] 
    
    # Load first set of filters in BCBA webpage #
    ## Send GET request for bcba_sec_url ##
    browser.get(bcba_sec_overview_url) 
    ## Set waiting time ##
    time.sleep(1)
    ## Select 100 results per page ##
    browser.find_element_by_xpath("//select[@class='textoMenuTop']").send_keys(100)
    ## Set waiting time ##   
    WebDriverWait(browser, delay).until(EC.invisibility_of_element_located((By.LINK_TEXT, '...')))
    
    # Retrieve relevant security details for first set of filters #
    ## Apply BeautifulSoup to browser content ##
    soup = BeautifulSoup(browser.page_source, 'html.parser')
    ## Get total number of pages and their related javascript weblinks ##
    num_of_pages = int(soup.find_all('a', {'href':re.compile('javascript')}, string=re.compile(r'\d+'))[-1].text.strip())
    ## Scrape the first page ##
    rows = soup.find_all('tr', {'class':re.compile('filaFiltroPanelPrincipal|filaVerde')})
    data_rows = [row.find_all('td') for row in rows]
    security_details = [[item.text.strip() 
                         if item != ''
                         else np.nan
                         for item in data_row] 
                         for data_row in data_rows]
    ## Loop through subsequent pages ##
    for i in range(2, num_of_pages + 1):
        ### Navigate to next page and grab relevant content ###
        browser.find_element_by_link_text('%s' % str(i)).click()
        ### Set waiting time ###
        time.sleep(5)           
        ### Search table and get company details ###
        security_details = scrape_page_details(browser, security_details)  
    
    # Load next set of filters in BCBA webpage #
    ## Select SME stocks ##
    browser.find_element_by_xpath("//select[@name ='ctl00$ctl00$ContentPlaceHolder1$tablaContenidoFiltro$cboTipoEspecie']").\
                                 send_keys('Acciones Pymes')
    ## Set waiting time ##
    time.sleep(1)        
    ## Click submit button ##
    browser.find_element_by_id('ctl00_ctl00_ContentPlaceHolder1_tablaContenidoFiltro_btnBuscar').click()
    ## Set waiting time ##
    time.sleep(5)
    ## Search table and get company details ##
    security_details = scrape_page_details(browser, security_details)  
    
    # Get associated weblink for each company #
    ## Convert company column to list ##
    securities = np.array(security_details)
    companies_list = list(np.unique(securities[:,-1]))

    # Get threaded urls for the target companies ##
    bcba_company_urls = get_threaded_res(get_full_companies, max_workers, companies_list, browsers, bcba_company_overview_url)
    
    # Get other corporate details, e.g. directors, subsidiaries etc. from associated weblinks #
    ## Get threaded responses for the target companies ##
    corporates, subsidiaries, directors, shareholders = get_threaded_res(get_other_details, 
                                                                         max_workers, 
                                                                         bcba_company_urls, 
                                                                         download_path, 
                                                                         browsers)    
    
    return securities, corporates, subsidiaries, directors, shareholders


def get_full_companies(company, browser, bcba_company_overview_url):
    '''
    Consolidate the list of Argentinian listed companies and their associated weblinks.
    Parameters:
        company                   : String containing the target Argentinian listed company
        browser                   : Firefox browser used
        bcba_company_overview_url : BCBA website url containing an overview of all companies 
    Return:
        bcba_companies            : Numpy array containing Argentinian listed companies and their associated weblinks
    '''
    # Query each company's associated BCBA weblink using its name #
    ## Send GET request for url ##
    browser.get(bcba_company_overview_url)
    ## Set waiting time ##
    time.sleep(1)
    ## Clear search bar ##
    browser.find_element_by_name('ctl00$ctl00$ContentPlaceHolder1$tablaContenidoFiltro$txbDenominacion').clear()
    ## Set waiting time ##
    time.sleep(1)
    ## Input the target Argentian listed company in the Search bar ##
    browser.find_element_by_name('ctl00$ctl00$ContentPlaceHolder1$tablaContenidoFiltro$txbDenominacion').send_keys(company)
    ## Set waiting time ##
    time.sleep(1)
    ## Click submit button ##
    browser.find_element_by_id('ctl00_ctl00_ContentPlaceHolder1_tablaContenidoFiltro_btnBuscar').click()
    ## Set waiting time ##
    time.sleep(5)
    
    # Scrape the company and its associated weblink that shows up in the display page #
    ## Apply BeautifulSoup to browser content ##
    soup = BeautifulSoup(browser.page_source, 'html.parser')
    ## Scrape the company associated weblink, if any ##
    try:
        row = soup.find_all('td', string=company)[0]
    except Exception:
        print('%s cannot be found!' % company)
    else:
        bcba_companies = np.array([[company, bcba_company_overview_url.split('BusquedaFichaTecnica.aspx')[0] + row.a['href']]])
    ## Clear search bar ##
    browser.find_element_by_name('ctl00$ctl00$ContentPlaceHolder1$tablaContenidoFiltro$txbDenominacion').clear()
    ## Set waiting time ##
    time.sleep(1)
    
    return bcba_companies


def get_other_details(bcba_company_url, browser, bcba_company_urls, download_path):
    '''
    Consolidate remaining corporate details, e.g. directors, subsidiaries etc, of Argentinian listed companies.
    Parameters:
        bcba_company_url  : BCBA website url for each target company
        browser           : Firefox browser used
        bcba_company_urls : Numpy array containing Argentinian listed companies and their associated weblinks
        download_path     : String containing the local download filepath
    Return:
        corporate_details : Numpy array containing company-level details
        subsidiary_details: Numpy array containing subsidiary-level details
        director_details  : Numpy array containing director-level details
        other_details     : Numpy array containing shareholder-level details
    '''
    # Initiate parameters #
    ## List containing company names ##
    company_name = bcba_company_urls[np.where(bcba_company_urls[:,1] == bcba_company_url),0].flatten()[0]
    ## String containing directorship titles ##
    directorships = re.compile('Auditoria|Auditoría|Autoria|Contable|De Vigilancia|' + \
                               'En Argentina|Fiscalizadora|Presidente|Relaciones|Secretario|' +\
                               'Suplente|Titular|Vicepresidente',
                               flags = re.IGNORECASE)  
    
    # Get company websites #
    ## Send GET request for bcba_company_url ##
    browser.get(bcba_company_url)
    ## Set waiting time ##
    time.sleep(1)
    ## Apply BeautifulSoup to browser content ##
    soup = BeautifulSoup(browser.page_source, 'html.parser')
    ## Scrape the company website under the General Information tab, if any ##    
    try:
        row = soup.find_all('a', {'id': 'ctl00_ContentPlaceHolder1_hypWeb'})[0]
    except Exception:
        corporate_details = np.array([[company_name, np.nan]])
    else:
        corporate_details = np.array([[company_name, row['href']]])
    
    # Get subsidiary details (if any) #
    ## Scrape the subsidiary details under the Subsidiary tab, if any ##    
    try:
        rows = soup.find_all('div', {'id': 'ctl00_ContentPlaceHolder1_divSocVinculadas'})[0].find_all('tr')[1:]
    except Exception:
        subsidiary_details = np.array([[company_name, np.nan, np.nan]])
    else:
        ## Store the values into array ##
        data_rows = [row.find_all('td') for row in rows]
        subsidiary_details = np.array([[company_name, 
                                        data_row[0].text.strip(), 
                                        float(re.sub(',', '.', data_row[-1].text.strip()))] 
                                        for data_row in data_rows])
    
    # Get director details (if any) #
    ## Scrape the director details under the Board of Directors tab, if any ##    
    try:
        rows = soup.find_all('tr', {'id': 'ctl00_ContentPlaceHolder1_trContCargos'})[0].find_all('td')[1:]
    except Exception:
        director_details = np.array([[company_name, np.nan, np.nan]])
    else:
        ## Store the values into array ##
        data_rows = [re.split(r'[\s]{3,}', row.text.strip()) for row in rows]
        try:
            director_details = np.array([[company_name, data_row[-1].strip().upper(), data_row[0].strip()] 
                                          if len(data_row) == 2 
                                          else [company_name,
                                                ' '.join(data_row[1:]).upper(),
                                                data_row[0].strip()]
                                          if len(data_row) > 2
                                          else [company_name,
                                                re.split(re.findall(directorships, data_row[0])[-1], data_row[0])[-1].strip().upper(),
                                                re.split(re.findall(directorships, data_row[0])[-1], data_row[0])[0] + \
                                                re.findall(directorships, data_row[0])[-1]]
                                          for data_row in data_rows])
        except Exception as e:
            print(e, data_rows)
    
    # Get latest controlling shareholders' details (if any) #
    ## Scrape the latest controlling shareholders' details under the Controlling Shareholders' tab, if any ##    
    try:
        data = soup.find_all('table', {'id': 'tblPrimerosAccControlantes'})[0].\
                    find_all('tr', {'class':'filaFiltroPanelPrincipal'})[0].\
                    find_all('td')[0].text.strip()
    except Exception:
        other_details = np.array([[company_name, np.nan]])
    else:
        other_details = np.array([[company_name, data]])
    
    # Download the latest Quarterly Financial Statement from the same webpage #
    quarterly_fs = soup.find_all('a', {'id': 'ctl00_ContentPlaceHolder1_hypEstadoContable'})[0]['href']
    if quarterly_fs:
        urllib.request.urlretrieve('https://www.bolsar.com/' + re.split('../', quarterly_fs)[-1],
                                   download_path + '\\QUARTERLY_FS_(%s).pdf' % company_name)
        
    return corporate_details, subsidiary_details, director_details, other_details
    
    
def get_outstanding_shares(payload, wtd_url):
    ''' 
    Return outstanding shares for each ticker fed to the openfigi API.
    Parameters:
        payload            : Dictionary/JSON containing the input keys and values
        wtd_url            : World Trading Data GET url
    Return:
        outstanding_shares : Numpy array containing the outstanding shares for the target tickers
    ''' 
    # Send GET request for payload and retrieve relevant details #
    response = requests.get(wtd_url, params=payload)
    try:
        response.json()['data']
    except Exception:
        outstanding_shares = np.array([[ticker, np.nan, np.nan] 
                                        for ticker in re.split(',', payload['symbol'])])
    else:
        outstanding_shares = np.array([[row['symbol'], row['name'], row['shares']] 
                                        for row in response.json()['data']])
    
    return outstanding_shares


def get_threaded_res(func, max_workers, *args):
    ''' 
    Return threaded responses for each function and input parameters.
    Parameters:
        func        : Function that will be executed in the threads
        max_workers : Number of threads
        *args       : Other input arguments
    ''' 
    # Initiate parameters #
    ## List containing get_full_companies parameters ##
    get_full_companies_list = ['company', 'browser', 'bcba_company_overview_url']
    ## List containing get_other_details parameters ##
    get_other_details_list = ['browser', 'bcba_company_url', 'bcba_company_urls', 'download_path']
    ## List containing get_outstanding_shares parameters ##
    get_outstanding_shares_list = ['payload', 'wtd_url']
    ## List containing the target func's parameters ##
    func_params = inspect.getfullargspec(func)[0]
    
    # Validate function #            
    if len(set(get_full_companies_list).intersection(func_params)) == len(get_full_companies_list):
        ## Initialise parameters ##
        ### New arguments ###
        bcba_company_urls = np.empty((0,2), str)
        ### Original arguments ###
        companies_list = args[0]
        browsers = args[1]
        bcba_company_overview_url = args[-1]
        
        ## Separate companies into batches of 10 ##
        batched_companies = chunks(companies_list, 10)
        
        ## Declare partial function ##
        func_get_urls = partial(func, bcba_company_overview_url = bcba_company_overview_url)
        
        ## Loop through each batch of companies and get associated weblinks ## 
        with ThreadPoolExecutor(max_workers = max_workers) as executor:
            for batched_company in batched_companies:
                results = executor.map(func_get_urls, batched_company, browsers)
                for result in results:
                    bcba_company_urls = np.vstack((bcba_company_urls, result))
        
        return bcba_company_urls
        
    elif len(set(get_other_details_list).intersection(func_params)) == len(get_other_details_list):
        ## Initialise parameters ##
        ### New ###
        corporates = np.empty((0,2), str)
        subsidiaries = np.empty((0,3), str)
        directors = np.empty((0,3), str)
        shareholders = np.empty((0,2), str)
        ### Original arguments ###
        bcba_company_urls = args[0]
        bcba_company_urls_list = list(np.unique(bcba_company_urls[:, -1]))
        download_path = args[1]
        browsers = args[-1]
        
        ## Separate bcba_company_urls_list into batches of 10 ##
        batched_urls = chunks(bcba_company_urls_list, 10)
        
        ## Declare partial function ##
        func_get_details = partial(func, 
                                   bcba_company_urls = bcba_company_urls,
                                   download_path = download_path)
        
        ## Loop through each url and get relevant company details ##
        with ThreadPoolExecutor(max_workers = max_workers) as executor:
            for batched_url in batched_urls:
                results = executor.map(func_get_details, batched_url, browsers)
                for result in results:
                    corporates = np.vstack((corporates, result[0]))
                    subsidiaries = np.vstack((subsidiaries, result[1]))
                    directors = np.vstack((directors, result[2]))
                    shareholders = np.vstack((shareholders, result[-1]))
        
        return corporates, subsidiaries, directors, shareholders
    
    elif len(set(get_outstanding_shares_list).intersection(func_params)) == len(get_outstanding_shares_list):
        ## Initiate parameters ##
        ### New ###
        outstanding_shares = np.empty((0,3), str)
        ### Original arguments ###
        payloads = args[0]
        wtd_url = args[-1]
        
        ## Separate payloads into batches of 10 ##
        batched_payloads = chunks(payloads, max_workers)
        
        ## Declare partial function ##
        func_get_outstanding = partial(func, wtd_url = wtd_url)
        
        ## Loop through each url and get outstanding shares details ##
        with ThreadPoolExecutor(max_workers = max_workers) as executor:
            for batched_payload in batched_payloads:
                results = executor.map(func_get_outstanding, batched_payload)
                for result in results:
                    outstanding_shares = np.vstack((outstanding_shares, result))
        
        return outstanding_shares
                    
    else:
        raise ValueError('Unknown function and/or arguments declared. Please verify.')
       
    
def scrape_page_details(browser, security_details):
    ''' 
    Scrape relevant security details from the relevant webpage.
    Parameter:
        browser          : Firefox browser used
    Return:
        security_details : List of security details scrapped from the page
    '''    
    # Apply BeautifulSoup to browser content #
    soup = BeautifulSoup(browser.page_source, 'html.parser')
        
    # Search table and get company details #
    rows = soup.find_all('tr', {'class':re.compile('filaFiltroPanelPrincipal|filaVerde')})
    data_rows = [row.find_all('td') for row in rows]
    data_lists = [[item.text.strip() 
                   if item != ''
                   else np.nan
                   for item in data_row] 
                   for data_row in data_rows]
        
    ## Append each list in data_items ##
    for data_list in data_lists:
        security_details.append(data_list)  
    
    return security_details

    
def start_homepage(log_path, download_path=None, mime=None):
    ''' 
    Initiate a Firebox browser session.
    Parameters:
        log_path      : String containing the filepath of the geckodriver log file
        download_path : String containing the local download filepath.
                        Default value is None
        mime          : Mime file type(s) that would be downloaded.
                        Default value is None
    Return:
        browser       : Instantiated Firefox browser
    '''
    # Initiate a Firefox browser session #
    profile = webdriver.FirefoxProfile()
    profile.set_preference('accessibility.blockautorefresh', True)
    profile.set_preference('network.http.response.timeout', 10000)
    if download_path != None:
        profile.set_preference('browser.download.folderList', 2)
        profile.set_preference('browser.download.dir', download_path)
        profile.set_preference('pdfjs.disabled', True)
        if mime != None:
            profile.set_preference('browser.helperApps.neverAsk.saveToDisk', mime)
        else:
            raise ValueError('Please provide MIME type in the argument!')  
            
    options = Options()
    options.add_argument('--headless')
    
    browser = webdriver.Firefox(firefox_profile=profile,
                                options=options,
                                service_log_path=log_path)
    
    return browser  


def main():
    '''
    Main function
    '''
    # Initialise parameters #
    ## Number of threads ##
    max_workers = 10
    ## String containing the local path to master file ##
    master_path = os.environ['USERPROFILE'] + '\\Dropbox\\ESG Research\\Voting Rights\\Master.xlsx'
    ## String containing the root website address ##
    master_file = pd.read_excel(master_path, sheet_name='SECURITIES')
    bcba_root_website = master_file.loc[master_file['COUNTRY']=='Argentina', 'WEBLINK'].iloc[0]
    ## String containing the local download filepath ##
    download_path = os.environ['USERPROFILE'] + master_file.loc[master_file['COUNTRY']=='Argentina', 'LOCAL_DOWNLOAD_PATH'].iloc[0]
    ## Openfigi API GET url and key ##
    api_keys = pd.read_excel(master_path, sheet_name='API_KEYS')
    wtd_url = api_keys.loc[api_keys['APPLICATION'] == 'WorldTradingData', 'API'].iloc[0]
    wtd_key = api_keys.loc[api_keys['APPLICATION'] == 'WorldTradingData', 'KEY'].iloc[0]
    
    # Start timer #
    start_time = time.time()
    
    # Launch threaded browsers and get security details from webpage #
    browsers = [start_homepage(log_path = download_path + '\\geckodriver.log') 
                for i in range(max_workers)]
    securities, corporates, subsidiaries, directors, shareholders = get_company_details(browsers, max_workers, 
                                                                                        bcba_root_website, download_path)   
    
    # Close threaded browsers #
    [browser.close() for browser in browsers]
    
    # End timer and print status message #
    end_time = time.time()
    print('The webscrapping process took %s minutes' % str((end_time-start_time)/60))
    
    # Convert data into DataFrame #
    df_securities, df_corporates, df_subsidiaries, df_directors = arr_convert_df(wtd_url, wtd_key, max_workers, 
                                                                                 securities, corporates, subsidiaries, 
                                                                                 directors, shareholders)
    
    # Download processed DataFrames #
    with pd.ExcelWriter(download_path + '\\BCBA_SECURITIES.xlsx') as writer:
        df_corporates.to_excel(writer, sheet_name='COMPANIES', index=False) 
        df_subsidiaries.to_excel(writer, sheet_name='SUBSIDIARIES', index=False) 
        df_directors.to_excel(writer, sheet_name='DIRECTORS', index=False)
        df_securities.to_excel(writer, sheet_name='SECURITIES', index=False)
    
if __name__ == '__main__':
    main()