# Import Python libraries #
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor
from functools import partial
import inspect
import numpy as np
import os
import pandas as pd
import re
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import time
import string


# Launch SET home page with Selenium's webdriver to scrape details #
def arr_convert_df(*args):
    ''' 
    Convert numpy arrays in pandas dataframes.
    Parameter:
        *args         : Input arguments
    Return:
        df_corporates : DataFrame containing company-level details
        df_directors  : DataFrame containing director-level details
        df_securities : DataFrame containing security-level details
    ''' 
    # Initialise parameters #
    corporates = args[0]
    directors = args[1]
    securities = args[2]
    df_isin = args[-1]
    
    # Convert arrays into DataFrames #
    df_corporates = pd.DataFrame(corporates, columns=['ISIN', 'TICKER', 'COMPANY', 'CORPORATE_WEBSITE'])
    df_corporates = df_corporates.replace('nan',np.nan)
    df_directors = pd.DataFrame(directors, columns=['COMPANY', 'DIRECTOR', 'POSITION'])
    df_directors = df_directors.replace('nan',np.nan)
    df_securities = pd.DataFrame(securities, columns=['COMPANY', 'SECURITY_TYPE', 'NUM_OF_SHARES', 'PAID_UP_CAPITAL', 'VOTING_RIGHT'])
    df_securities = df_securities.replace('nan',np.nan)
    
    # Append df_securities with voting rights of U City's preferred shares #
    preferred_sec = df_securities.loc[(df_securities['COMPANY']=='U CITY PUBLIC COMPANY LIMITED') & 
                                      (df_securities['TICKER']=='U-P')]
    preferred_sec['VOTING_RIGHT'] = '0.1'
    df_securities = df_securities.append(preferred_sec, ignore_index=True)
    df_securities.sort_values(by=['COMPANY','TICKER'], inplace=True)
    
    # Map df_corporates and df_isin with df_securities #
    df_securities_new = pd.merge(df_securities.dropna(), 
                                 df_corporates[['COMPANY', 'TICKER', 'ISIN']],
                                 how = 'left',
                                 left_on = ['COMPANY'],
                                 right_on = ['COMPANY'])
    df_securities_new = pd.merge(df_securities_new, 
                                 df_isin,
                                 how = 'left',
                                 left_on = ['COMPANY', 'SECURITY_TYPE'],
                                 right_on = ['Company Name', 'Securities Type'])
    
    # Overwrite row data for all preferred stocks # 
    df_securities_new.loc[(df_securities_new['SECURITY_TYPE'] == 'Preferred Stock') &
                          ((df_securities_new['Securities Type'] == 'Preferred Stock') | 
                           (df_securities_new['Securities Type'].isnull())), 
                           'TICKER'] = \
                          df_securities_new.loc[(df_securities_new['SECURITY_TYPE'] == 'Preferred Stock') &
                                                ((df_securities_new['Securities Type'] == 'Preferred Stock') | 
                                                 (df_securities_new['Securities Type'].isnull())), 
                                                'Securities Symbol']

    df_securities_new.loc[(df_securities_new['SECURITY_TYPE'] == 'Preferred Stock') &
                          ((df_securities_new['Securities Type'] == 'Preferred Stock') | 
                           (df_securities_new['Securities Type'].isnull())),  
                            'ISIN'] = \
                          df_securities_new.loc[(df_securities_new['SECURITY_TYPE'] == 'Preferred Stock') &
                                                ((df_securities_new['Securities Type'] == 'Preferred Stock') | 
                                                (df_securities_new['Securities Type'].isnull())), 
                                                'ISIN Code']
    
    # Remove duplicates and output necessary columns #
    df_securities_new = df_securities_new[['COMPANY', 'TICKER', 'ISIN',
                                           'SECURITY_TYPE', 'NUM_OF_SHARES', 'PAID_UP_CAPITAL', 
                                           'VOTING_RIGHT']]
    df_securities_new.drop_duplicates(inplace=True)
    
    # Convert data types for NUM_OF_SHARES and VOTING_RIGHT columns #
    df_securities['NUM_OF_SHARES'] = df_securities['NUM_OF_SHARES'].str.replace(' Shares', '').str.replace(',', '').astype(float)
    df_securities['VOTING_RIGHT'] = df_securities['VOTING_RIGHT'].str.split(' : ').str[-1].str.replace(',', '').astype(float) / \
                                    df_securities['VOTING_RIGHT'].str.split(' : ').str[0].str.replace(',', '').astype(float)
    
    return df_corporates, df_directors, df_securities_new


def chunks(lst, n):
    '''
    Generate successive n-sized chunks from list.
    Parameters:
        lst           : List containing the elements to be broken into chunks
        n             : Size of each chunk
    Return:
        chunked_list  : List containing n-sized chunks
    '''
    chunked_list = [lst[i:i + n] for i in range(0, len(lst), n)]
    
    return chunked_list


def get_company_details(url, browser, dataset):
    ''' 
    Consolidate the list of Thai listed companies and their associated details.
    Parameters:
        url               : SET website url for each company
        browser           : Firefox browser used
        dataset           : Raw dataset containing the SET webpages
    Return:
        corporate_details : Numpy array containing company-level details
        director_details  : Numpy array containing director-level details
        security_details  : Numpy array containing security-level details
    '''      
    # Initiate parameter #
    voting_errata = ['https://www.set.or.th/set/companyprofile.do?symbol=PK&ssoPageId=4&language=en&country=US', 'Preferred Stock', '100 : 1',
                     'https://www.set.or.th/set/companyprofile.do?symbol=SCC&ssoPageId=4&language=en&country=US', 'Common Stock', '1 : 1',
                     'https://www.set.or.th/set/companyprofile.do?symbol=NC&ssoPageId=4&language=en&country=US', 'Common Stock', '1 : 1',
                     'https://www.set.or.th/set/companyprofile.do?symbol=CTW&ssoPageId=4&language=en&country=US', 'Common Stock', '1 : 1',]
    
    # Send GET request for url and retrieve relevant details # 
    soup = navigate_page(url, browser)

    # Search table and get company details #
    rows = soup.find_all('tr')
    if rows:
        ## Corporate information ##
        company_websites = [row.find('div', string='Website').find_next_sibling().text.strip()
                            for row in rows
                            if row.find('div', string='Website')]    
        company_website = np.array([[website] 
                                     if website != '-'
                                     else [np.nan]
                                     for website in company_websites])

        try:
            stack = [row.find('div', string='Position').find_all_next('div', {'class':'col-xs-6'})
                     for row in rows
                     if row.find('div', string='Position')][0]
        except Exception:
            directors = np.array([[np.nan, np.nan]])
        else:
            directors = []
            for i in range(0, len(stack), 2):
                if stack[i].text.strip() != '':
                    directors.append([stack[i].text.strip(), stack[i+1].text.strip()])
                else:
                    temp = directors[-1][0]
                    directors.append([temp, stack[i+1].text.strip()])    
            directors = np.array(directors)
        
        ## Securities ##
        boldings_cap_detail = [row.find_all('strong') 
                               for row in rows
                               if row.find('div', string='Capital Detail')][0]
        boldings_share_detail = [row.find_all('strong') 
                                 for row in rows
                                 if row.find('div', string='Shares Detail')][0]
        boldings_share_detail = [bolding 
                                 for bolding in boldings_share_detail
                                 if 'Stock' in bolding.text.strip()]
        ### Names/Classes ###
        securities_cap_detail = np.array([[bolding.text.strip()] 
                                           for bolding in boldings_cap_detail
                                           if 'Stock' in bolding.text.strip()])
        ### ISIN ###
        securities_isin = np.array([[bolding.find_next('div').text.strip().split()[-1]] 
                                    for bolding in boldings_cap_detail 
                                    if 'ISIN' in bolding.text.strip()])
        ### Paid-up Capital and Shares ###
        paid_up_capital = [bolding.find_next('div', text=re.compile('Paid-up Capital')).find_next_sibling().text.strip()
                           for bolding in boldings_cap_detail 
                           if 'Stock' in bolding.text.strip()]
        paid_up_capital = np.array([[capital]
                                   if capital != '-' 
                                   else [np.nan]
                                   for capital in paid_up_capital])
        
        stack = np.hstack((securities_cap_detail, paid_up_capital))
        zero_outstanding_secs = list(stack[np.in1d(stack[:,1], np.array(['nan']))][:,0])
        paid_up_shares = np.array([[bolding.find_next('div', text=re.compile('Paid-up Stock')).find_next_sibling().text.strip()] 
                                  if not bolding.text.strip() in zero_outstanding_secs
                                  else [np.nan]
                                  for bolding in boldings_share_detail])
        ### Voting Rights ###
        if not url in voting_errata:
            securities_voting_rights = np.array([[bolding.find_next('div', text=re.compile('Voting Right Ratio')).\
                                                  find_next_sibling().text.strip()] 
                                                  if not bolding.text.strip() in zero_outstanding_secs
                                                  else [np.nan]
                                                  for bolding in boldings_share_detail])

        else:
            securities_voting_rights = np.array([[voting_errata[voting_errata.index(url) + 2]]
                                                  if (not bolding.text.strip() in zero_outstanding_secs) and \
                                                     (bolding.text.strip() == voting_errata[voting_errata.index(url) + 1])
                                                  else [bolding.find_next('div', text=re.compile('Voting Right Ratio')).\
                                                  find_next_sibling().text.strip()]
                                                  if (not bolding.text.strip() in zero_outstanding_secs) and \
                                                     (bolding.text.strip() != voting_errata[voting_errata.index(url) + 1])
                                                  else [np.nan]
                                                  for bolding in boldings_share_detail])                  
    
        ## Consolidation ##
        corporate_details = np.hstack((securities_isin,
                                       dataset[np.where(dataset == url)[0], :2],
                                       company_website))
        director_details = np.hstack((np.tile(dataset[np.where(dataset == url)[0], 1:2], directors.shape[0]).T,
                                      directors))
        security_details = np.hstack((np.tile(dataset[np.where(dataset == url)[0], 1:2], securities_cap_detail.shape[0]).T,
                                      securities_cap_detail,
                                      paid_up_shares,
                                      paid_up_capital,
                                      securities_voting_rights))
        
    return corporate_details, director_details, security_details

            
def get_full_companies(url, browser, root_url):
    ''' 
    Consolidate the list of Thai listed companies and their associated SET weblinks.
    Parameters:
        url               : SET website url for each prefix 
        browser           : Firefox browser used
        root_url          : String containing the root SET website address
    Return:
        set_companies     : Numpy array containing Thai listed companies 
    '''                     
    # Send GET request for url and retrieve relevant details #  
    soup = navigate_page(url, browser)
    
    # Search table and get company details #
    rows = soup.find_all('tr')[1:]
    if rows:
        set_weblinks = np.array([[root_url + str(row.a['href'])] for row in rows])
        set_symbols = np.array([[str(row.a.text)] for row in rows])
        set_company_names = np.array([[row.a.find_next('td').contents[0]] for row in rows])
        set_companies = np.hstack((set_symbols, set_company_names, set_weblinks))

    return set_companies


def get_isin(browser, root_url, download_path):
    ''' 
    Download the list of Thai listed companies and their associated ISINs 
    to merge with other files.
    Parameters:
        browser       : Firefox browser used
        root_url      : String containing the root SET website address 
        download_path : String containing the local download path 
    Return:
        df_active     : DataFrame containing active Thai listed securities and their ISINs 
    '''  
    # Initialise parameters #
    url = root_url + '/tsd/download/isin/index_en.html'
    repeat_counter = True
    
    # Remove any current day ISIN files #
    filepath = [download_path + '\\' + file for file in os.listdir(download_path) if 'ISINCode' in file]
    if filepath:
        os.remove(filepath[0])
    
    # Send GET request for url and retrieve relevant details #  
    browser.get(url)
    
    # Download ISIN file from SET into local directory #
    download_button = browser.find_element_by_tag_name('img')
    download_button.click()
    
    # Wait for download to complete #
    while repeat_counter:
        time.sleep(3)
        files = os.listdir(download_path)
        if '.part' not in files:
            time.sleep(2)
            filepath = [download_path + '\\' + file for file in os.listdir(download_path) if 'ISINCode' in file][0]
            repeat_counter = False
        else:
            time.sleep(2)

    # Parse the ISIN file and extract the list of active securities #
    df = pd.read_excel(filepath, skiprows=9)
    df_isin = df.loc[(df['Status'] == 'Active') & (df['Type of ISIN Code'] == 'Local'), \
                       ['Company Name', 'Securities Symbol', 'ISIN Code', 'Securities Type']]
    df_isin['Securities Type'] = df_isin['Securities Type'].str.replace(r'\bS\b', 'Common Stock')
    df_isin['Securities Type'] = df_isin['Securities Type'].str.replace(r'\bP\b', 'Preferred Stock')
    
    return df_isin
    

def get_threaded_res(func, max_workers, *args):
    ''' 
    Return threaded responses for each function and input parameters.
    Parameters:
        func        : Function that will be executed in the threads
        max_workers : Number of threads
        *args       : Other input arguments
    '''             
    # Initialise parameters #
    ## List containing get_company_details parameters ##
    get_company_details_list = ['url', 'browser', 'dataset']
    ## List containing get_full_companies parameters ##
    get_full_companies_list = ['url', 'browser', 'root_url']
    ## List containing the target func's parameters ##
    func_params = inspect.getfullargspec(func)[0]
    
    # Validate function #            
    if len(set(get_company_details_list).intersection(func_params)) == len(get_company_details_list):
        ## Initiate parameters ##
        ### New arguments ###
        corporates = np.empty((0,4), str)
        directors = np.empty((0,3), str)
        securities = np.empty((0,5), str)
        ### Original arguments ###
        urls = args[0]
        browsers = args[1]
        dataset = args[-1]
        
        ## Separate urls into batches of 10 ##
        chunked_urls = chunks(urls, max_workers)      
        
        ## Declare partial function ##
        func = partial(func, dataset=dataset)
        
        ## Loop through each batch of urls and get associated details ## 
        start_time = time.time()
        with ThreadPoolExecutor(max_workers = max_workers) as executor:
            for chunked_url in chunked_urls:
                results = executor.map(func, chunked_url, browsers)
                for result in results:
                    corporates = np.vstack((corporates, result[0]))
                    directors = np.vstack((directors, result[1]))
                    securities = np.vstack((securities, result[-1]))
        
        ## End timer and print status message ##
        end_time = time.time()
        print('The webscrapping process took %s minutes' % str((end_time-start_time)/60))
        return corporates, directors, securities
        
    elif len(set(get_full_companies_list).intersection(func_params)) == len(get_full_companies_list):
        ## Initiate parameters ##
        ### New arguments ###
        set_webpages = np.empty((0,3), str)
        ### Original arguments ###
        urls = args[0]
        root_url = args[1]
        browsers = args[-1]
        
        ## Separate urls into batches of 10 ##
        chunked_urls = chunks(urls, max_workers)      
        
        ## Declare partial function ##
        func = partial(func, root_url=root_url)
        
        ## Loop through each batch of urls and get associated SET webpages ##
        start_time = time.time()
        with ThreadPoolExecutor(max_workers = max_workers) as executor:
            for chunked_url in chunked_urls:
                results = executor.map(func, chunked_url, browsers)
                for result in results:
                    set_webpages = np.vstack((set_webpages, result))
    
        ## End timer and print status message ##
        end_time = time.time()
        print('The retrieval of webpages took %s minutes' % str((end_time-start_time)/60))
        return set_webpages
    
    else:
        raise ValueError('Unknown function and/or arguments declared. Please verify.')
         
    
def navigate_page(url, browser):
    ''' 
    Consolidate the list of Thai listed companies and their associated SET weblinks.
    Parameters:
        url      : String containing SET webpage url
        browser  : Firefox browser used
    Return:
        soup     : Browser content 
    '''                     
    # Send GET request for each url and retrieve relevant details #      
    try:
        ## Navigate to url with Firefox browser ##
        browser.get(url)
    except Exception as e:
        print(e)
    else:
        ## Set waiting time ##
        time.sleep(1)
    
        ## Apply BeautifulSoup to browser content ##
        soup = BeautifulSoup(browser.page_source, 'html.parser')
    
    return soup
    

def start_homepage(log_path, download_path=None, mime=None):
    ''' 
    Initiate a Firebox browser session.
    Parameters:
        log_path      : String containing the filepath of the geckodriver log file 
        download_path : String containing the local download filepath.
                        Default value is None
        mime          : Mime file type(s) that would be downloaded.
                        Default value is None
    Return:
        browser       : Instantiated Firefox browser
    '''
    # Initiate a Firefox browser session #
    profile = webdriver.FirefoxProfile()
    profile.set_preference('accessibility.blockautorefresh', True)
    profile.set_preference('network.http.response.timeout', 10000)
    if download_path != None:
        profile.set_preference('browser.download.folderList', 2)
        profile.set_preference('browser.download.dir', download_path)
        profile.set_preference('pdfjs.disabled', True)
        if mime != None:
            profile.set_preference('browser.helperApps.neverAsk.saveToDisk', mime)
        else:
            raise ValueError('Please provide MIME type in the argument!')
            
    options = Options()
    options.add_argument('--headless')
    
    browser = webdriver.Firefox(firefox_profile=profile,
                                options=options,
                                service_log_path=log_path)
    
    return browser


def main():
    # Initialise parameters #
    ## Number of threads ##
    max_workers = 10
    ## List contaning all SET prefixes ##
    set_prefixes = ['NUMBER'] + list(string.ascii_uppercase)
    ## String containing the local path to master file ##
    master_path = os.environ['USERPROFILE'] + '\\Dropbox\\ESG Research\\Voting Rights\\Master.xlsx'
    ## String containing the root website address ##
    master_file = pd.read_excel(master_path, sheet_name='SECURITIES')
    set_root_website = master_file.loc[master_file['COUNTRY']=='Thailand', 'WEBLINK'].iloc[0]
    ## String containing the local download filepath ##
    download_path = os.environ['USERPROFILE'] + master_file.loc[master_file['COUNTRY']=='Thailand', 'LOCAL_DOWNLOAD_PATH'].iloc[0]
    
    # Launch threaded browsers #
    browsers = [start_homepage(log_path=download_path + '\\geckodriver.log',
                               download_path=download_path, 
                               mime='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') 
                for i in range(max_workers)]
    
    # Create lists of target company urls based on number of threads #
    urls = [set_root_website + '/set/commonslookup.do?language=en&country=US&prefix=' + str(set_prefix)
            for set_prefix in set_prefixes]
    # Get threaded responses for the target parameters #
    set_webpages = get_threaded_res(get_full_companies, max_workers, urls, set_root_website, browsers)

    # Create lists of target company detail urls based on number of threads #
    urls = list(set_webpages[:, -1])
    # Get threaded responses for the target parameters #
    corporates, directors, securities = get_threaded_res(get_company_details, max_workers, urls, browsers, set_webpages)
    
    # Download ISINs from SET #
    df_isin = get_isin(browser = browsers[0], 
                       root_url = set_root_website, 
                       download_path = download_path)

    # Close browser #
    [browser.close() for browser in browsers]

    # Convert into Pandas DataFrames #
    df_corporates, df_directors, df_securities = arr_convert_df(corporates, directors, securities, df_isin)
    
    # Download processed DataFrames #
    with pd.ExcelWriter(download_path + '\\SET_SECURITIES.xlsx') as writer:
        df_corporates[['COMPANY', 'CORPORATE_WEBSITE']].to_excel(writer, sheet_name='COMPANIES', index=False) 
        df_directors.to_excel(writer, sheet_name='DIRECTORS', index=False)
        df_securities.to_excel(writer, sheet_name='SECURITIES', index=False)
    
if __name__ == '__main__':
    main()