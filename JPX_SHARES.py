# Import Python libraries #
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor
from functools import partial
import json
import math
import numpy as np
import os
import pandas as pd
import re
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import urllib


# Launch JPX home page with Selenium's webdriver to scrape details #  
def arr_convert_df(openfigi_url, openfigi_key, securities):
    ''' 
    Convert numpy array in pandas dataframe.
    Parameters:       
        openfigi_url  : String containing Openfigi API GET url
        openfigi_key  : String containing Openfigi API key
        securities    : Numpy array containing security-level details
    Return:
        df_securities : DataFrame containing security-level details
    ''' 
    # Initialise parameter #
    security_types = np.empty((0,3), str)
    
    # Transform numpy array into DataFrame and drop duplicates #
    df_securities = pd.DataFrame(securities, columns=['COMPANY', 'TICKER', 'ISIN', 'NUM_OF_SHARES', 'VOTING_RIGHT'])
    df_securities.drop_duplicates(inplace=True)

    # Extract security types from Bloomberg and transform into DataFrame #
    ## Convert ISINs in df_securities into a list ##
    isins = list(df_securities['ISIN'])
    ## Break up the ISINs into batches of 100 (Upper limit set by Bloomberg POST API) ##
    chunked_isins = chunks(isins, 100)
    ## Feed each batch of ISINs into Bloomberg POST API ##
    for seq, chunked_isin in enumerate(chunked_isins):
        types = get_security_type(openfigi_url, openfigi_key, chunked_isin, 'JP')
        security_types = np.vstack((security_types, types))
    ## Print status message ##
    print('ISIN batch %d is complete!' % (seq + 1))
    ## Re-submit request for security_type = nan with alternative exchange code ##
    nan_security = list(np.where(security_types[:,1] == 'nan')[0])
    nan_security = [security_types[i,0] for i in nan_security]
    types = get_security_type(openfigi_url, openfigi_key, nan_security, 'JT')
    ## Consolidate latest results with earlier ones ##
    security_types = np.vstack((security_types, types))
    
    # Transform security_types into DataFrame and drop duplicate ISINs #
    df_isin = pd.DataFrame(security_types, columns=['ISIN', 'SECURITY_TYPE_1', 'SECURITY_TYPE_2'])
    df_isin = df_isin.replace('nan', np.nan)
    df_isin.dropna(inplace=True)
    df_isin.drop_duplicates(subset=['ISIN'], inplace=True)

    # Merge df_isin and df_securities #
    df_securities = pd.merge(df_securities, df_isin, how='left', on='ISIN')
    df_securities = df_securities[['COMPANY', 'TICKER', 'ISIN', 
                                   'SECURITY_TYPE_1', 'SECURITY_TYPE_2', 'NUM_OF_SHARES', 
                                   'VOTING_RIGHT']]
    
    # Convert data types for NUM_OF_SHARES and VOTING_RIGHT columns #
    df_securities['NUM_OF_SHARES'] = df_securities['NUM_OF_SHARES'].str.replace(',', '').astype(float)
    df_securities['VOTING_RIGHT'] = df_securities['VOTING_RIGHT'].astype(float)

    return df_securities


def chunks(lst, n):
    '''
    Generate successive n-sized chunks from list.
    Parameters:
        lst           : List containing the elements to be broken into chunks
        n             : Size of each chunk
    Return:
        chunked_list  : List containing n-sized chunks
    '''
    chunked_list = [lst[i:i + n] for i in range(0, len(lst), n)]
    
    return chunked_list


def get_company_details(basic_info_button, browser, download_path, browsers):
    ''' 
    Navigate to relevant JPX webpage and scrape relevant security details.
    Parameters:
        browser           : Firefox browser used
        basic_info_button : Button to be selected by Selenium
        download_path     : String containing the local download filepath
        browsers          : List of Selenium Firefox browsers
    Return:
        company_details   : Numpy array containing company details 
    '''                     
    # Select Basic Information Button #
    browser.execute_script("arguments[0].click();", basic_info_button)
    
    # Create buffer time #
    time.sleep(3)
    
    # Validate whether it is dead browser #
    browser = validate_request(browser, 'basic_info_button', browsers)
        
    # Cater waiting time #
    WebDriverWait(browser, 30).until(EC.presence_of_element_located((By.CLASS_NAME, 'icon_subnavi1')))    
    
    # Apply BeautifulSoup to browser content #
    soup = BeautifulSoup(browser.page_source, 'html.parser')
    
    # Search table and get company details #
    rows = soup.find_all('tr')
    data_rows = [row.find_next_sibling() 
                 for row in rows 
                 if row.find('th', string='Code')][0].find_all('td')
    if len(data_rows) == 6 or len(data_rows) == 7:
        ## Company name ##
        company_name = soup.find('h3').text.strip()
        ## Ticker ##
        security_ticker = data_rows[0].text.strip()[:-1]
        ## ISIN ##
        security_isin = data_rows[1].text.strip()
        ## Number of shares outstanding ##
        share_details = [row.find_next_sibling() 
                         for row in rows 
                         if row.find('th', text=re.compile('Information related to Warning'))][0].find_all('td')
        if len(share_details) == 4:
            num_of_shares = share_details[-2].text.strip()
        else:
            raise ValueError('%s has incomplete share details. Please check!' 
                              % str(company_name))
        ## Voting Rights ##
        try:
            float(data_rows[-1].text.strip())
        except Exception:
            voting_rights = np.nan
        else:
            voting_rights = 1 / float(data_rows[-1].text.strip())
            
    else:
        raise ValueError('%s has incomplete company details. Please check!' 
                         % str(soup.find('h3').text.strip()))
        
    # Consolidate all entries into an array and print array after batch completion #
    company_details = np.array([[company_name, security_ticker, security_isin,
                                 num_of_shares, voting_rights]])
    print(company_details)     
    
    # Locate and download the latest Articles of Incorporation #
    articles = soup.find_all('strong', string=re.compile('Articles of Incorporation|Rules'))[0].find_next('table').find_all('a')
    if articles:
        urllib.request.urlretrieve('https://www2.tse.or.jp' + articles[0]['href'],
                                   download_path + '\\AA_(%s).pdf' % company_name)
    
    return company_details


def get_security_type(openfigi_url, openfigi_key, isins, exchange_code):
    ''' 
    Return security types for each ISIN fed to the openfigi API.
    Parameters:
        openfigi_url     : String containing Openfigi API GET url
        openfigi_key     : String containing Openfigi API key
        isins            : List containing target ISINs
        exchange_code    : Exchange code which the security is traded
    Return:
        security_types   : Numpy array containing the security types of the target ISINs
    ''' 
    # Initialise parameters #
    headers = {'Content-Type':'text/json', 'X-OPENFIGI-APIKEY':openfigi_key}
    payload = json.dumps([{"idType":"ID_ISIN", "idValue": "%s" % isin, "exchCode": "%s" % exchange_code} 
                            for isin in isins])
    
    # Send POST request for each isin and exchange_code, and retrieve relevant details #
    response = requests.post(openfigi_url, data=payload, headers=headers)
    try: 
        response.json()
    except Exception as e:
        print(e, isins)
    else:
        keys = [key for res in response.json() for key in res.keys()]
        error_keys = [seq for seq, key in enumerate(keys) if 'error' in key]
        if error_keys:
            security_type = [res['data'][0]['securityType'] 
                             if not i in error_keys 
                             else np.nan 
                             for i, res in enumerate(response.json())]
            security_type2 = [res['data'][0]['securityType2'] 
                              if not i in error_keys 
                              else np.nan 
                              for i, res in enumerate(response.json())]
        else:
            security_type = [res['data'][0]['securityType'] 
                             for res in response.json()]
            security_type2 = [res['data'][0]['securityType2'] for res in response.json()]
            
        security_types = np.hstack((np.array([isins]).T, 
                                    np.array([security_type]).T,
                                    np.array([security_type2]).T))
        
    return security_types


def navigate_page(browser, page_counter):
    ''' 
    Navigate to main result page.
    Parameters:
        browser      : Firefox browser used
        page_counter : Integer representing the index of next page
    Return:
        soup     : Browser content 
    '''  
    # Initialise parameter #
    delay = 3
    
    # Go back to previous search page #
    browser.execute_script("window.history.go(-1)")
    
    # Select Next button to navigate to next page #
    WebDriverWait(browser, 30).until(EC.presence_of_element_located((By.XPATH, "//img[@alt='Next']")))
    next_page = browser.find_element_by_xpath("//img[@alt='Next']")
    browser.execute_script('arguments[0].click();', next_page)
    
    # Create buffer time #
    time.sleep(delay)
    
    # Validate whether it is dead browser #
    browser = validate_request(browser, 'navigate_page')
    WebDriverWait(browser, 30).until(EC.presence_of_element_located((By.CLASS_NAME, 'current')))
    
    # Create buffer time #
    time.sleep(delay)

    return browser
    

def select_page_filters(browser, url):
    ''' 
    Select relevant filters in JPX webpage.
    Parameters:
        browser      : Firefox browser used
        url          : String containing JPX webpage url
    Return:
        browser      : Browser with page filters loaded 
    '''   
    # Initialise parameter #
    market_filters = ['001', '002', '004', '006', '008', 'RET']
    
    # Send GET request for url and retrieve relevant details # 
    browser.get(url)   
    
    # Set waiting time #
    time.sleep(1)
    
    # Select Market Divisions: First Section, Second Section, Mothers, JASDAQ and Tokyo Pro Market #
    [browser.find_element_by_xpath("//input[@value='%s']" % str(market_filter)).click()
    for market_filter in market_filters]
        
    # Select Search button to confirm filters #
    browser.find_element_by_name('searchButton').click()
    
    # Wait for page to finish loading #
    try:
        WebDriverWait(browser, 30).until(EC.presence_of_element_located((By.CLASS_NAME, 'pagingmenu')))
    except Exception:
        ## Validate whether it is dead browser ##
        browser = validate_request(browser, 'select_filters')

    return browser


def start_homepage(log_path, download_path=None, mime=None):
    ''' 
    Initiate a Firebox browser session.
    Parameters:
        log_path      : String containing the filepath of the geckodriver log file
        download_path : String containing the local download filepath.
                        Default value is None
        mime          : Mime file type(s) that would be downloaded.
                        Default value is None
    Return:
        browser       : Instantiated Firefox browser
    '''
    # Initiate a Firefox browser session #
    profile = webdriver.FirefoxProfile()
    profile.set_preference('accessibility.blockautorefresh', True)
    profile.set_preference('network.http.response.timeout', 10000)
    if download_path != None:
        profile.set_preference('browser.download.folderList', 2)
        profile.set_preference('browser.download.dir', download_path)
        profile.set_preference('pdfjs.disabled', True)
        if mime != None:
            profile.set_preference('browser.helperApps.neverAsk.saveToDisk', mime)
        else:
            raise ValueError('Please provide MIME type in the argument!')
                       
    options = Options()
    options.add_argument('--headless')
    
    browser = webdriver.Firefox(firefox_profile=profile,
#                                options=options,
                                service_log_path=log_path)
    
    return browser


def validate_request(browser, process, browsers=None):
    '''
    Validate whether the request leads to a dead browser.
    Parameters:
        browser     : Subject browser in question
        process     : String containing the process name. Currently it takes these values:
                        (A) select_filters 
                        (B) basic_info_button
                        (C) navigate_page
        browsers    : List of Selenium Firefox browsers
    Return:
        browser     : Return subject browser
    '''
    # Initiate parameter #
    repeat_counter = True
    
    # Parse the webpage contents #
    time.sleep(2)
    soup = BeautifulSoup(browser.page_source, 'html.parser')
    
    # Validate webpage contents #
    while repeat_counter:
        ## Dead browser ##
        if 'Service Unavailable' in soup.title.text.strip():
            ## Go back to previous page ##
            browser.execute_script('window.history.go(-1)')
            time.sleep(2)
            ## Identify the process name where the dead browser is sprung ##
            if process == 'select_filters':
                ### Select search button ###
                WebDriverWait(browser, 30).until(EC.presence_of_element_located((By.NAME, 'searchButton')))
                browser.find_element_by_name('searchButton').click()
            elif process == 'basic_info_button':
                ### Identify the browser from the list and extract the new Basic Information button ###
                WebDriverWait(browser, 30).until(EC.presence_of_element_located((By.NAME, 'detail_button')))
                sequence = [seq for seq, original_browser in enumerate(browsers) if original_browser == browser][0]
                basic_info_button = browser.find_elements_by_class_name('activeButton')[sequence]
                ### Select Basic Information Button ### 
                browser.execute_script("arguments[0].click();", basic_info_button)
            elif process == 'navigate_page':
                ### Select next page button ###
                WebDriverWait(browser, 30).until(EC.presence_of_element_located((By.XPATH, "//img[@alt='Next']")))
                next_page = browser.find_element_by_xpath("//img[@alt='Next']")
                browser.execute_script('arguments[0].click();', next_page)
            else:
                raise ValueError('Invalid process name selected.')
            ## Parse the webpage contents ##
            time.sleep(2)
            soup = BeautifulSoup(browser.page_source, 'html.parser')
        else:
            repeat_counter = False 
    
    return browser
    

def main():
    '''
    
    '''
    
# Initialise parameters #
## Number of threads ##
max_workers = 10
## Empty array ##
securities = np.empty((0,5), str)
## String containing the local path to master file ##
master_path = os.environ['USERPROFILE'] + '\\Dropbox\\ESG Research\\Voting Rights\\Master.xlsx'
## String containing the root website address ##
master_file = pd.read_excel(master_path, sheet_name='SECURITIES')
jpx_root_website = master_file.loc[master_file['COUNTRY']=='Japan', 'WEBLINK'].iloc[0]
## String containing the local download filepath ##
download_path = os.environ['USERPROFILE'] + master_file.loc[master_file['COUNTRY']=='Japan', 'LOCAL_DOWNLOAD_PATH'].iloc[0]
## Openfigi API GET url and key ##
api_keys = pd.read_excel(master_path, sheet_name='API_KEYS')
openfigi_url = api_keys.loc[api_keys['APPLICATION'] == 'openfigi', 'API'].iloc[0]
openfigi_key = api_keys.loc[api_keys['APPLICATION'] == 'openfigi', 'KEY'].iloc[0]

# Launch threaded browsers to JPX home page #
browsers = [start_homepage(log_path = download_path + '\\geckodriver.log') 
            for i in range(max_workers)]
func_get_details = partial(get_company_details, download_path=download_path, browsers=browsers)
func_set_filters = partial(select_page_filters, url=jpx_root_website)
with ThreadPoolExecutor(max_workers = max_workers) as executor:
    results = executor.map(func_set_filters, browsers)
browsers = [result for result in results]

# Get number of pages from paging index #
soup = BeautifulSoup(browsers[0].page_source, 'html.parser')
num_of_pages = math.ceil(int(soup.find_all('div', {'class':'pagingmenu'})[0].find('span').text.strip().split('/')[-1])/10)

# Loop through each page and get relevant company details # 
for i in range(1, num_of_pages + 1):
    ## Print status message ##
    print('Start: %d of %d' % (i, num_of_pages))
    ## Get locations of Basic Information buttons ##
    basic_info_buttons = [browsers[i].find_elements_by_class_name('activeButton')[i] 
                          for i in range(len(browsers[0].find_elements_by_class_name('activeButton')))]
    ## Multi-threading ##
    with ThreadPoolExecutor(max_workers = max_workers) as executor:
        results = executor.map(func_get_details, basic_info_buttons, browsers)
        for result in results:
            securities = np.vstack((securities, result))
    ## Re-initialise browsers and basic_info_buttons ##
    if i < num_of_pages:
        func_navigate = partial(navigate_page, page_counter=i+1)
        with ThreadPoolExecutor(max_workers = max_workers) as executor:
            results = executor.map(func_navigate, browsers)
        browsers = [result for result in results]

# Close browser #
[browser.close() for browser in browsers]

# Convert into DataFrame #
df_securities = arr_convert_df(openfigi_url, openfigi_key, securities)

# Download processed DataFrame #
df_securities.to_excel(download_path + '\\JPX_SECURITIES.xlsx', sheet_name='SECURITIES', index=False)