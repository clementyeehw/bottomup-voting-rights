# Overview

The python scripts facilitate the extraction of companies' voting rights of all listed and / or unlisted securities.

---

## Description of Python Scripts

1. BCBA_SHARES.py : Extraction of voting rights of all securities listed on Argentinian Stock Exchange.
2. EDGAR_SHARES.py : Extraction of voting rights of all listed and unlisted US securities using EDGAR corporate filings.
3. EDGAR_NLP_SHARES.py : Extraction of voting rights of all listed and unlisted US securities using EDGAR corporate filings and ML/NLP.
4. JPX_SHARES.py : Extraction of voting rights of all securities listed on Tokyo Stock Exchange.
5. SET_SHARES.py : Extraction of voting rights of all listed and unlisted Thai securities.
6. SGX_SHARES.py : Extraction of voting rights of all listed and unlisted Singapore securities using annual reports.

---