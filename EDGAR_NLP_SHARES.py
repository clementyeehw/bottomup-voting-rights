## Import Python libraries ##
from bs4 import BeautifulSoup
import datetime
import Levenshtein as lev
import nltk
from nltk.corpus import stopwords
import numpy as np
import pandas as pd
import re
import requests
import spacy
from spacy.lang.en import English, LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES
from spacy.lang.en.stop_words import STOP_WORDS
from spacy.lemmatizer import Lemmatizer
from spacy.tokenizer import Tokenizer
import time
import urllib
import warnings
warnings.filterwarnings('ignore')


## Initialise variables/instances ##
nlp = spacy.load('en_core_web_sm')
#nlp = spacy.load('en')
language = English()
lemmatizer = Lemmatizer(LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES)
tokenizer = Tokenizer(language.vocab)
target_pairs = ['vote-stock', 'vote-share']
stopWords = set(stopwords.words('english'))

## Download the list of company filings from EDGAR ##
def edgar_download(start_year, end_year, start_quarter = 1, end_quarter = 4):
    ''' 
    Connect to EDGAR website and download company filings for the relevant financial periods
    Parameters:
        start_year    : The first year (e.g. 2020) which we wish to obtain the company filings from
        end_year      : The last year (e.g. 2020) which we wish to obtain the company filings from
        start_quarter : The first quarter which we wish to obtain the company filings from. 
                        Takes values 1 to 4. Default value is 1
        end_quarter   : The last quarter which we wish to obtain the company filings from. 
                        Takes values 1 to 4. Default value is 4
    Return:
        edgar_filings : Pandas DataFrame containing all company filings and associated weblinks
        edgar_10K     : Pandas DataFrame containing only 10-K company filings and associated weblinks
        edgar_14A     : Pandas DataFrame containing only 14-A company filings and associated weblinks
    '''      
    # Initialise an empty array with shape (0,5): edgar_arr #
    edgar_arr = np.empty((0,5), str)

    # Connect to EDGAR website #
    start_time = time.time()
    if start_year > end_year:
        print('Please revise start_year as it is greater than end_year!')
    
    elif start_year == end_year:
        if start_quarter > end_quarter:
            print('Please revise start_quarter as it is greater than end_quarter!')
    
        elif start_quarter == end_quarter:
        # Connect to EDGAR archives and get company.idx #
            data = urllib.request.urlopen('https://www.sec.gov/Archives/edgar/full-index/%s/QTR%s/company.idx' 
                                          %(start_year, start_quarter))
            # Preprocess the data in company.idx #
            lines = edgar_preprocess(data)
            # Vectorize column data into array #
            edgar_arr = np.vstack([edgar_arr, np.array([np.array(line) for line in lines])])
        
        else:
            for quarter in range(start_quarter, end_quarter + 1):
                # Connect to EDGAR archives and get company.idx #
                data = urllib.request.urlopen('https://www.sec.gov/Archives/edgar/full-index/%s/QTR%s/company.idx' 
                                              %(start_year, quarter))
                # Preprocess the data in company.idx #
                lines = edgar_preprocess(data)
                # Vectorize column data into array #
                edgar_arr = np.vstack([edgar_arr, np.array([np.array(line) for line in lines])])
                
    else:
        for year in range(start_year, end_year + 1):
            for quarter in range(start_quarter, end_quarter + 1):
                # Connect to EDGAR archives and get company.idx #
                data = urllib.request.urlopen('https://www.sec.gov/Archives/edgar/full-index/%s/QTR%s/company.idx' 
                                              %(year, quarter))
                # Preprocess the data in company.idx #
                lines = edgar_preprocess(data)
                # Vectorize column data into array #
                edgar_arr = np.vstack([edgar_arr, np.array([np.array(line) for line in lines])])
    
    # Import edgar_arr as Pandas DataFrame #
    columns = ['Company', 'Filing_Type', 'CIK', 'Filing_Date', 'Web_Path']
    if edgar_arr.size:
        edgar_filings = pd.DataFrame(edgar_arr, columns=columns)    
        # Add root path to Web_Path #
        edgar_filings['Web_Path'] = 'https://www.sec.gov/Archives/' + edgar_filings['Web_Path'] 
        # Merge datasets to get ticker data #
        edgar_tickers = edgar_cik_ticker()
        edgar_filings = pd.merge(edgar_filings, edgar_tickers, how='left', on = 'CIK')
        # Re-arrange the column data #
        edgar_filings = edgar_filings[['Company', 'Ticker', 'CIK', 'Filing_Type', 'Filing_Date', 'Web_Path']]
        # Consider only original 10-K and revised 10-K filings: edgar_10K #
        edgar_10K = edgar_filings.loc[edgar_filings['Filing_Type'].isin(['10-K','10-K/A'])]
        # Consider only Definitive Proxy Statements: edgar_14A #
        edgar_14A = edgar_filings.loc[edgar_filings['Filing_Type'].isin(['DEF 14A'])]   
        # Sort them by Company Name and Filing Date, and drop the earlier duplicates #
        edgar_10K = edgar_10K.sort_values(['Company','Filing_Date'])
        edgar_10K.drop_duplicates(subset=['Company'], keep='last', inplace=True)
        edgar_14A = edgar_14A.sort_values(['Company','Filing_Date'])
        edgar_14A.drop_duplicates(subset=['Company'], keep='last', inplace=True)
        # Print end status message #
        print('The filings are downloaded successfully! It took %s seconds.' % str(time.time() - start_time))
    
    else:
        print('The dataset is empty. Please check your parameters!')
    
    return edgar_filings, edgar_10K, edgar_14A
        
def edgar_preprocess(data):
    ''' 
    Pre-process the data in company.idx
    Parameter:
        data     : Raw data in company.idx
    Return:
        lines    : List containing all the pre-processed data
    '''    
    # Parse the data - From line 10 onwards and skipping the last line #
    lines = data.readlines()[10:-1]
    # Convert bytes into string and split data into columns #
    lines = [re.split(r'[\s]{2,}', line.decode('utf-8').strip()) for line in lines]
    # Concatenate additional segments as part of company string #
    lines = [[' '.join(line[:-4])] + line[-4:] for line in lines if len(line) >= 5]

    return lines

def edgar_cik_ticker():
    ''' 
    Maps a company's Central Index Key (CIK) to its corresponding ticker
    Parameter:
        cik              : Company's CIK
    Return:
        edgar_ticker     : Company's ticker
    ''' 
    # Initialise an empty array with shape (0,1): cik_ticker_arr #
    cik_ticker_arr = np.empty((0,2), str)
    # Connect to EDGAR archives and get ticker.txt #
    data = urllib.request.urlopen('https://www.sec.gov/include/ticker.txt')
    # Preprocess the data in ticker.txt #
    lines = data.readlines()
    lines = [re.split(r'[\t]', line.decode('utf-8').strip()) for line in lines]
    lines = [line for line in lines if len(line) == 2]
    # Vectorize column data into array #
    cik_ticker_arr = np.vstack([cik_ticker_arr, np.array([line for line in lines])])
    # Convert into Pandas DataFrame # 
    edgar_tickers = pd.DataFrame(cik_ticker_arr, columns=['Ticker', 'CIK'])
    # Re-arrange the column data #
    edgar_tickers = edgar_tickers[['CIK', 'Ticker']]
    
    return edgar_tickers


## Obtain voting rights data from EDGAR filing ##
def edgar_extract_votes(edgar_filing):
    ''' 
    Connect to EDGAR web links and extract relevant voting rights_data
    Parameter:
        edgar_data     : Edgar filings data (i.e. edgar_10K, edgar_14A)
    Return:
        edgar_votes    : Pandas DataFrame containing the relevant voting rights data
    '''   
    # Initialise list to store class shares and associated voting rights #
    share_rights = []
    # Make a list of iterables containing web links: url_list #
    url_list = list(edgar_filing['Web_Path'])
    # Iterate the url_list to connect to each company's web filings #
    for seq, url in enumerate(url_list):
        # GET request #
        response = requests.get(url)
        # Parse the html details using BeautifulSoup #
        soup = BeautifulSoup(response.text, 'xml')
        # Get all html tags that might contain useful information and compile into a list: bodies #
        bodies = soup.find_all('body')
        # Perform pre-processing on all the body text and tokenize them for NLP #
        for body in bodies:
            body_text = body.text.strip()
            for word_pair in target_pairs:
                # Split the word-pair into its target and surrounding word constitutents: target, surround #
                target = re.split('-', word_pair)[0]
                surround = re.split('-', word_pair)[1]
                if all(elem in body_text.lower() for elem in [target, surround]):
                    npmi_arr, full_arr, filtered_sents = edgar_sent_selector(body_text, 
                                                                        target_word = target, 
                                                                        surround_word = surround)
                    # Initialise the list containing all the corpora text: sentences #
                    sentences = []
                    if len(filtered_sents) > 0:
                        # Pre-process the class shares data #
                        for sentence in filtered_sents:
                            if (re.finditer(r'\b[Cc]lass\w\b', sentence) and len(sentence) < 100000):
                                for string in re.finditer(r'\b[Cc]lass\w\b', sentence): 
                                    sentence = re.sub(string[0], 'Class ' + re.split(r'[Cc]lass', string[0])[1], sentence)    
                            elif (re.finditer(r'\b[Ss]eries\w\b', sentence) and len(sentence) < 100000):
                                for string in re.finditer(r'\b[Ss]eries\w\b', sentence): 
                                    sentence = re.sub(string[0], 'Series ' + re.split(r'[Ss]eries', string[0])[1], sentence)
                            sentence = re.sub(r'[Cc]ompany\W\w ', '', sentence)
                            sentences.append(sentence)
                        # Tokenize sentences in text: corpora #
                        corpora = [nlp(sentence) for sentence in sentences if len(sentence) < 100000]
                        # Loop through each corpus #
                        for corpus in corpora:
                            # Break down each corpus into chunks: chunks #
                            chunks = edgar_chunk_retokeniser(corpus)
                            # Narrow down the choice of chunks and extract class shares and their associated voting power: share_rights #
                            filtered_chunks, vote_chunks = edgar_chunk_selector(chunks)
                            if len(vote_chunks) != 0:
                                for chunk in vote_chunks:
                                    share_rights.append((edgar_filing.loc[edgar_filing['Web_Path'] == url]['Company'].iloc[0], 
                                                         edgar_filing.loc[edgar_filing['Web_Path'] == url]['Ticker'].iloc[0],
                                                         edgar_filing.loc[edgar_filing['Web_Path'] == url]['CIK'].iloc[0],
                                                         chunk[0].title(), chunk[1]))
        # Remove duplicates in share_rights and sort in alphabetical order #      
        share_rights = list(set(share_rights))
        share_rights = sorted(share_rights, key=lambda tup: (tup[0], tup[3]))
        print('%s of %s companies completed!' % (seq + 1, len(url_list)))
        print(share_rights)
        
    # Print voting rights data into Pandas DataFrame # 
    edgar_votes = pd.DataFrame(share_rights, 
                               columns=['Company', 'Ticker', 'CIK', 'Class_Shares', 'Votes_Per_Share'])
    
    return edgar_votes

def edgar_sent_selector(body_text, target_word, surround_word, window_size = 5):
    ''' 
    Narrow down sentences that meet the Normalized Point-wise Mutual Information (NPMI) measure
    Parameters:
        body_text      : Body text extracted from EDGAR 10-K filings
        target_word    : Target word in target_pairs
        surround_word  : Surround word in target_pairs
        window_size    : Size of context window. Default value is 5
    Return:
        npmi_arr       : Array containing the word counts, PMI and NPMI computations
        full_arr       : Array containing the sentences and their associated word counts
        filtered_sents : List containing the sentences whose NPMI values are greater than 0.5
    '''  
    # Pre-process the text data extracted from the html tag: text #
    text = re.sub(r'[\n\xa0]+', ' ', body_text)
    text = ' '.join(re.split(r'[\s]{2,}', text)) 
    # Apply word tokenizer to the text #
    word_tokens = tokenizer(text)
    # Apply lemmatizer to reduce target words to its root form #
    text = ' '.join([lemmatizer(word_token.text,'VERB')[0] \
                     if re.search(r'\bvot.+\b', word_token.text) \
                     else word_token.text
                     for word_token in word_tokens])
    # Tokenize sentences in text #
    sentences = nltk.sent_tokenize(text)
    # Initialise an empty array with shape (0,4): full_arr #
    full_arr = np.empty((0,4), str)
    # Initialise an empty list: co_occur_count_list #
    co_occur_count_list = []
    # Compute word counts and Normalized PMI (NPMI) # 
    for seq, sentence in enumerate(sentences):
        # Initialise the co-occurrence count of target and surrounding words: co_occur_count #    
        co_occur_count = 0
        if all(elem in sentence.lower() for elem in [target_word, surround_word]):
            # Apply word tokenization to each sentence that has both target and surround words #
            word_tokens = nltk.word_tokenize(sentence.lower())
            for seq, word_token in enumerate(word_tokens):
                # Compute the co-occurrence counts of target and surrounding words #
                if target_word in word_token and \
                surround_word in word_tokens[max(0, seq - window_size):min(seq + window_size + 1, len(word_tokens))]:
                    # Increment the counter #
                    co_occur_count += 1
            # Append count to list #
            co_occur_count_list.append(co_occur_count)
        
    # Update npmi_arr which stores the sentences, word counts of both target and surrounding words #
    arr = np.array([(sentence,
                     sentence.count(target_word),
                     sentence.count(surround_word),
                     len(sentence.split())) \
                     for sentence in sentences \
                     if all(elem in sentence.lower() for elem in [target_word, surround_word])])
    
    if arr.size:                        
        full_arr = np.vstack([full_arr, arr]) 
        # Concatenate the co_occur_count with npmi_arr #
        full_arr = np.concatenate((full_arr, np.array([co_occur_count_list]).T), axis=1)        
        # Create a new vector containing only the numerical word counts: npmi_arr # 
        npmi_arr = full_arr[:,1:].astype(float)
        # Create 2 new columns to store PMI and NPMI values #
        npmi_arr = np.append(npmi_arr, np.zeros((npmi_arr.shape[0],2), dtype=float), axis=1)
        # Compute PMI values #
        npmi_arr[:,-2] = np.log((npmi_arr[:,3]*npmi_arr[:,2])/(npmi_arr[:,0]*npmi_arr[:,1]))
        # Compute NPMI values #
        npmi_arr[:,-1] = npmi_arr[:,-2]/(-1*np.log(npmi_arr[:,3]/npmi_arr[:,2]))
        
        # Create a list that contains sentences whose NPMI vallues are greater than 0.5 #
        filtered_sents = full_arr[np.where(npmi_arr[:,-1]>0.4),0].tolist()[0]
    
    else:
        npmi_arr = full_arr = np.array([])
        filtered_sents = []
        
    return npmi_arr, full_arr, filtered_sents

def edgar_chunk_retokeniser(corpus):
    ''' 
    Identify missing stock(s) and add them into noun chunks
    Parameter:
        corpus   : Processed corpus by spaCy
    Return:
        chunks   : List containing all adjusted noun chunks that contain class shares and voting rights information
    '''       
    # Initialise lists containing original chunks, sequences and corpus indexes #
    stock_indicator = []
    vote_indicator = []
    
    # Extract all the pos tags and noun chunks #
    pos_tags = [(token.text, token.pos_) for token in corpus]
    chunks = [(chunk.text, chunk.root.text, chunk.root.dep_, chunk.root.head.text) for chunk in corpus.noun_chunks]
    # Remove all duplicate noun chunks #
    chunks = list(dict.fromkeys(chunks))  
    
    # Fill stock_indicator and vote_indicator with class shares and votes respectively #
    for seq, chunk in enumerate(chunks):
        if (lemmatizer(chunk[1].lower(), 'VERB')[0] == 'stock' and len(chunk[0].split()) > 1) or \
            (lemmatizer(chunk[1].lower(), 'VERB')[0] == 'share' and len(chunk[0].split()) > 1 and \
             not re.search(r'\beach\b', chunk[0].lower()) and not re.search(r'\ba\b', chunk[0].lower())):
            if 'and' in chunk[0] and 'stock' in chunk[0].lower():
                for elem in re.split(r'\band\b', chunk[0].lower().replace('stock', '')):
                    stock_indicator.append(((elem.strip().title(), 'stock', 'pobj', 'synthetic'), seq))
            elif 'and' in chunk[0] and 'share' in chunk[0].lower():     
                for elem in re.split(r'\band\b', chunk[0].lower().replace('share', '')):
                    stock_indicator.append(((elem.strip().title(), 'stock', 'pobj', 'synthetic'), seq))
            else:
                stock_indicator.append((chunk, seq))
                            
        elif chunk[1].lower() == 'vote' and len(chunk[0].split()) == 2 and isposfloat(word2num(chunk[0].split()[0])):
            vote_indicator.append((chunk, seq))  
        
        elif lemmatizer(chunk[1].lower(), 'VERB')[0] == 'right' and len(chunk[0].split()) == 3 and \
             re.search(r'\bvote\b', chunk[0].split()[1]) and isposfloat(word2num(chunk[0].split()[0])):
            vote_indicator.append((chunk, seq))            
     
    # Identify the missing stock(s) or votes from corpus and retokenise them as valid chunks #
    if (len(stock_indicator) < len(vote_indicator) and (len(stock_indicator) !=0) and (len(vote_indicator) != 0)):
        seqs = [seq for seq, (word, pos_tag) in enumerate(pos_tags) if \
                any(elem in word.lower() for elem in ['series', 'class', 'preferred', 'common'])]
        spans = [corpus[seq: min(seq+1, len(pos_tags)) + 1] \
                for seq in seqs if re.search(r'\b\w\b', pos_tags[min(seq+1, len(pos_tags))][0])]
        indexes = [(corpus.text.index(chunk[0]), chunk) for chunk in chunks]
        for span in spans:
            indexes.append((corpus.text.index(span.text), (span.text.title(), 'stock', 'pobj', 'synthetic')))
            # Sort indexes according to the sequence in which the chunk appears in the corpus #
            indexes = sorted(indexes, key=lambda tup: (tup[0]))
            # Extract the sorted chunks from the indexes #
            chunks = [(index[1]) for index in indexes]
            # Remove all duplicate noun chunks #
            chunks = list(dict.fromkeys(chunks))

    elif (len(stock_indicator) > len(vote_indicator) and (len(stock_indicator) == 2) and (len(vote_indicator) == 1)):
        chunks.append((vote_indicator[0][0][0], vote_indicator[0][0][1], vote_indicator[0][0][2], 'synthetic'))                
                
    return chunks

def edgar_chunk_selector(chunks, window_size = 1):
    ''' 
    Narrow down chunks that have class shares and voting rights information
    Parameters:
        chunks          : List containing all noun chunks extracted from corpus
        window_size     : Size of context window. Default value is 1
    Return:
        filtered_chunks : List containing detailed class shares and voting rights information
        vote_chunks     : List containing summarized class shares and voting rights information
    '''    
    # Initialise lists containing original chunks and sequence #
    stock_indicator = []
    vote_indicator = []
    # Initialise lists containing reduced chunks: filtered_chunks, vote_below_class and vote_above_class #
    filtered_chunks = []
    vote_chunks = []
        
    # Fill stock_indicator and vote_indicator with class shares and votes respectively #
    for seq, chunk in enumerate(chunks):
        if (lemmatizer(chunk[1].lower(), 'VERB')[0] == 'stock' and len(chunk[0].split()) > 1) or \
            (lemmatizer(chunk[1].lower(), 'VERB')[0] == 'share' and len(chunk[0].split()) > 1 and \
             not re.search(r'\beach\b', chunk[0].lower()) and not re.search(r'\ba\b', chunk[0].lower())):
            if 'and' in chunk[0] and 'stock' in chunk[0].lower():
                for elem in re.split(r'\band\b', chunk[0].lower().replace('stock', '')):
                    stock_indicator.append(((elem.strip().title(), 'stock', 'pobj', 'synthetic'), seq))
            elif 'and' in chunk[0] and 'share' in chunk[0].lower():     
                for elem in re.split(r'\band\b', chunk[0].lower().replace('share', '')):
                    stock_indicator.append(((elem.strip().title(), 'stock', 'pobj', 'synthetic'), seq))
            else:
                stock_indicator.append((chunk, seq))
        
        elif chunk[1].lower() == 'vote' and len(chunk[0].split()) == 2 and isposfloat(word2num(chunk[0].split()[0])):
             # Determine if the voting right chunk has omitted other information, e.g. fractions #
            if chunk[-1].lower() == 'of' and isposfloat(word2num(chunks[max(0, seq - window_size)][0])):
                votes_per_share = word2num(chunks[max(0, seq - window_size)][0]) * word2num(chunk[0].split()[0])
                vote_indicator.append((votes_per_share, chunks[max(0, seq - window_size)], chunk[0:]))
            else:
                votes_per_share = word2num(chunk[0].split()[0])
                vote_indicator.append((votes_per_share, chunk[0:]))
        
        elif lemmatizer(chunk[1].lower(), 'VERB')[0] == 'right' and len(chunk[0].split()) == 3 and \
             re.search(r'\bvote\b', chunk[0].split()[1]) and isposfloat(word2num(chunk[0].split()[0])):
            votes_per_share = word2num(chunk[0].split()[0])    
            vote_indicator.append((votes_per_share, chunk[0:]))            
     
    # Fill filtered_chunks with stock_indicator and vote_indicator details (sorted) #
    if (len(stock_indicator) == len(vote_indicator) and (len(stock_indicator) !=0) and (len(vote_indicator) != 0)):
        filtered_chunks = [(stock_indicator[i][0][0], vote_indicator[i][0]) for i in range(len(stock_indicator))]    
        # Fill vote_chunks with summarized information #
        vote_chunks = [(chunk[0], chunk[-1]) for chunk in filtered_chunks]
    
    # Remove duplicates in vote_chunks and sort in alphabetical order # 
    if len(vote_chunks) != 0:
        vote_chunks = list(set(vote_chunks))
        vote_chunks = sorted(vote_chunks, key=lambda tup: tup[0])
    
    return filtered_chunks, vote_chunks  
                               
def word2num(textnum, numwords = {}):
    ''' 
    Convert words to numbers
    Parameters:
        textnum              : Numbers that are spelt out in text
        numwords             : Dictionary containing the scales and increments
    Return:
        result+current       : Numbers in numerical format after translation
    ''' 
    if not numwords:
        # Initialise whole numbers #
        units = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', \
                 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
        tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']
        scales = ['hundred', 'thousand', 'million', 'billion', 'trillion']
        
        # Initialise other common vocabulary #
        numwords['and'] = (1, 0)
        numwords['non-vote'] = (1, 0)
        numwords['no'] = (1, 0)
        
        # Create scale and increment and store in dictionary: numwords #
        for idx, word in enumerate(units):  
            numwords[word] = (1, idx)
        for idx, word in enumerate(tens):       
            numwords[word] = (1, idx * 10)
        for idx, word in enumerate(scales): 
            numwords[word] = (10 ** (idx * 3 or 2), 0)
    
    # Initialise fraction dictionary: ordinal_words #
    ordinal_words = {'half': float(1/2), 'third': float(1/3), 'fourth': float(1/4), 'fifth': float(1/5), 
                     'sixth': float(1/6), 'seventh': float(1/7), 'eighth': float(1/8), 'ninth': float(1/9), 
                     'tenth': float(1/10), 'eleventh': float(1/11), 'twelfth': float(1/12), 'thirteenth': float(1/13), 
                     'fourteenth': float(1/14), 'fifteenth': float(1/15), 'sixteenth': float(1/16), 'seventeenth': float(1/17), 
                     'eighteenth': float(1/18), 'nineteenth': float(1/19), 'twentieth': float(1/20)}    
    
    # Replace hyphen in text with space and change the text to lower case #
    textnum = textnum.replace('-', ' ').lower()
    textnum = textnum.replace(',', '').lower()
    
    # Compute the results #
    current = result = 0
    for word in textnum.split():
        if word in ordinal_words:
            # Assign scale and increment for fractions #
            scale, increment = (ordinal_words[word], 0)      
        elif word in numwords:
            # Assign scale and increment for whole numbers # 
            scale, increment = numwords[word]
        elif isposfloat(word):
            scale, increment = (1, float(word))
        elif re.search(r'\d+[/]\d+\w*', word):
            temp = re.search('\d+[/]\d+', word)[0]
            scale, increment = (0, float(temp.split('/')[0]) / float(temp.split('/')[1]))
        else:
            # Assign scale and increment for invalid word #
            scale, increment = (0, -1)
            #raise Exception('Illegal word: ' + word)
            
        current = current * scale + increment
        if scale > 100:
            result += current
            current = 0
    
    return float(result + current)

def isposfloat(value):
    ''' 
    Test whether a value is a positive float
    Parameter:
        value        : Input value
    Return:
        True/False   : Boolean indicating whether the input value is a positive float
    ''' 
    try:
        if float(value) >= 0:
            return True
        else:
            return False 
    except ValueError:
         return False


def main():    
    try:
        edgar_filings, edgar_10K, edgar_14A = edgar_download(start_year=2018, end_year=2018)    
    except Exception as e:
            print(e)
            input('Press Enter to continue!')
    share_rights = edgar_extract_votes(edgar_14A)
    print(share_rights)

#if __name__ == '__main__':
#    main()

try:
    edgar_filings, edgar_10K, edgar_14A = edgar_download(start_year=2018, end_year=2018)    
except Exception as e:
    print(e)
    input('Press Enter to continue!')
#edgar_votes = edgar_extract_votes(edgar_14A)
#print(edgar_votes)