# Import Python libraries #
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor
import datetime as dt
import xml.etree.ElementTree as ET
from functools import partial
import nltk
from nltk.corpus import stopwords
import numpy as np
import pandas as pd
import re
import requests
import spacy
from spacy.lang.en import English, LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES
from spacy.lemmatizer import Lemmatizer
from spacy.tokenizer import Tokenizer
import time
import urllib
import warnings
warnings.filterwarnings('ignore')


# Initialise NLP variables/instances #
#nlp = spacy.load('en')
nlp = spacy.load('en_core_web_sm')
language = English()
lemmatizer = Lemmatizer(LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES)
tokenizer = Tokenizer(language.vocab)
target_pairs = ['vote-stock', 'vote-share']


# Modify stopwords imported from NLTK #
stopWords = [stopWord for stopWord in list(set(stopwords.words('english')))]
stopWords.remove('a')
stopWords.remove('d')
stopWords.remove('m')
stopWords.remove('i')
stopWords = stopWords + ['companys', 'each']


# Create placeholder to store common company abbreviations: company_abbs #
company_abbs = ['INC', 'CORP', 'LTD', 'CO', 'PLC',
               'LP', 'LLP', 'SA', 'NV', 'AG',
               'LLC', 'CV', 'A/S', 'CO LTD']
# Create placeholder to store CIKs that require rectification after EDGAR extraction: clean_ciks #
clean_ciks = [('DIS', '1001039'), ('ICLR', '1060955'), ('ISBC', '1594012'), ('XRX', '108772')]
# Create placeholder to store potential class and common stocks: stock_name #
stock_name = [r'([Ss]eries|[Cc]lass) ([A-Z0-9])(?:\s[Cc]onvertible)? ([Pp]refer[a-z]*|[Cc]ommon|[Cc]apital)?[\s]?([Ss]hare[s]?|[Ss]tock|[Uu]nit[s]?)?', \
              r'([Cc]ommon|[Pp]refer[a-z]*) ([Ss]eries)* ([A-Z0-9])(?:\s[Ss]eries)? ([Ss]hare[s]?|[Ss]tock|[Uu]nit[s]?)', \
              r'([Oo]rdinary|[Cc]ommon|[Pp]refer[a-z]*|[Cc]apital) ([Ss]hare[s]?|[Ss]tock|[Uu]nit[s]?)', \
              r'(\w+ [Ss]tock [Uu]nit[s]?)', \
              r'([A-Z][Ss][Uu][s]?)']

    
#########################################################################################################################################

# Download web-links of company filings from EDGAR online database #
def edgar_download(start_year, end_year, start_quarter=1, end_quarter=4, rolling=True):
    ''' 
    Connect to EDGAR website and download company filings for the relevant financial periods
    Parameters:
        start_year    : The first year (e.g. 2020) which we wish to obtain the company filings from
        end_year      : The last year (e.g. 2020) which we wish to obtain the company filings from
        start_quarter : The first quarter (tied to start_year) which we wish to obtain the company filings from
                        Takes values 1 to 4. Default value is 1
        end_quarter   : The last quarter (tied to end_year) which we wish to obtain the company filings from
                        Takes values 1 to 4. Default value is 4
        rolling       : Flag to determine whether we want rolling quarters for cross-years filings
                        For example if we indicate start_quarter=2 and end_quarter=3, 
                        rolling=True means that we want Q2 to Q4 for start_year and Q1 to Q3 for end_year.
                        Conversely, rolling=False means we want Q2 to Q3 for both start_year and end_year
    Return:
        edgar_filings : Pandas DataFrame containing all company filings and associated weblinks
        edgar_10K     : Pandas DataFrame containing only 10-K company filings and associated weblinks
        edgar_14A     : Pandas DataFrame containing only 14-A company filings and associated weblinks
    '''      
    # Initialise an empty array with shape (0,5): edgar_arr #
    edgar_arr = np.empty((0,5), str)
     
    # Impose conditional checks on year selected #
    if start_year > end_year:
        raise Exception('Please revise start_year as it is greater than end_year!')
    
    # Impose conditional checks on quarter selected #
    elif not start_quarter in np.arange(1,5) or not end_quarter in np.arange(1,5):
        raise Exception('Invalid start or end quarter selected!')
    
    else:
        # Start timer #
        start_time = time.time()
   
        # Connect to EDGAR website #
        if start_year == end_year:
            if start_quarter > end_quarter:
                raise Exception('Please revise start_quarter as it is greater than end_quarter!')
    
            elif start_quarter == end_quarter:
                # Connect to EDGAR archives and get company.idx #
                data = urllib.request.urlopen('https://www.sec.gov/Archives/edgar/full-index/%s/QTR%s/company.idx' 
                                              %(start_year, start_quarter))
                # Preprocess the data in company.idx #
                lines = edgar_preprocess_quarterly_index(data)
                # Vectorize column data into array #
                edgar_arr = np.vstack([edgar_arr, np.array([np.array(line) for line in lines])])
        
            else:
                for quarter in range(start_quarter, end_quarter + 1):
                    # Connect to EDGAR archives and get company.idx #
                    data = urllib.request.urlopen('https://www.sec.gov/Archives/edgar/full-index/%s/QTR%s/company.idx' 
                                                  %(start_year, quarter))
                    # Preprocess the data in company.idx #
                    lines = edgar_preprocess_quarterly_index(data)
                    # Vectorize column data into array #
                    edgar_arr = np.vstack([edgar_arr, np.array([np.array(line) for line in lines])])
                
        else:
            # Initialise an empty list to store the periods: periods #
            periods = []
        
            if rolling:
                # Record the stub and full years #
                for quarter in range(start_quarter, 5):
                    periods.append((start_year, quarter))
                if end_year - start_year + 1 > 0:
                    for year in range(start_year + 1, end_year):
                        for quarter in range(1, 5):        
                            periods.append((year, quarter))           
                for quarter in range(1, end_quarter + 1):
                    periods.append((end_year, quarter))

            else:
                for year in range(start_year, end_year + 1):
                    for quarter in range(start_quarter, end_quarter + 1):
                        periods.append((year, quarter))
        
            # Connect to EDGAR archives and get company.idx #
            for year, quarter in periods:
                try:
                    data = urllib.request.urlopen('https://www.sec.gov/Archives/edgar/full-index/%s/QTR%s/company.idx' 
                                                  %(year, quarter))
                except Exception:
                    print('The database is not ready for the relevant period: (%s, Q%s)' % (year, quarter))
                    continue
            
                else:
                    # Preprocess the data in company.idx #
                    lines = edgar_preprocess_quarterly_index(data)
                    # Vectorize column data into array #
                    edgar_arr = np.vstack([edgar_arr, np.array([np.array(line) for line in lines])])
    
        # Import edgar_arr as Pandas DataFrame #
        columns = ['Company', 'Filing_Type', 'CIK', 'Filing_Date', 'Web_Filing']
        if edgar_arr.size:
            edgar_filings = pd.DataFrame(edgar_arr, columns=columns)    
            # Add root path to Web_Filing #
            edgar_filings['Web_Filing'] = 'https://www.sec.gov/Archives/' + edgar_filings['Web_Filing']        
            # Re-arrange the column data #
            edgar_filings = edgar_filings[['CIK', 'Company', 'Filing_Type', 'Filing_Date', 'Web_Filing']]
            
            # Consider only original 10-K and 20-F filings: edgar_10K #
            # Note: Revised 10-K filings, i.e. 10-K/A, contains less informative tables #
            edgar_10K = edgar_filings.loc[edgar_filings['Filing_Type'].isin(['10-K', '20-F'])]
            # Create main depository web-link #
            edgar_10K['Web_Depository'] = edgar_10K['Web_Filing'].str.replace('-', '').str.replace(r'\.txt', '').str.strip()
            # Consider only Definitive Proxy Statements, i.e. 14A and 14C: edgar_14A #
            edgar_14A = edgar_filings.loc[edgar_filings['Filing_Type'].isin(['DEF 14A', 'DEF 14C'])]
            # Create main depository web-link #
            edgar_14A['Web_Depository'] = edgar_14A['Web_Filing'].str.replace('-', '').str.replace(r'\.txt', '').str.strip()
            # Sort the filings by Company Name and Filing Date in ascending order, and drop the earlier filings #
            edgar_10K.sort_values(by=['Company','Filing_Date'], ascending=[True,True], inplace=True)
            edgar_10K.drop_duplicates(subset=['CIK'], keep='last', inplace=True)
            # Sort the filings by Company Name, Filing Date and Filing Type in ascending order, and drop the earlier filings #
            edgar_14A.sort_values(['Company','Filing_Date','Filing_Type'], ascending=[True,True,True], inplace=True)
            edgar_14A.drop_duplicates(subset=['CIK','Filing_Type'], keep='last', inplace=True)
            # Drop DEF 14C records from edgar_14A if both DEF 14A and DEF 14C are filed for the target company #
            # Note: DEF 14C proxy statements contain less information #
            idx = edgar_14A[edgar_14A.duplicated(subset=['CIK'], keep=False)][edgar_14A['Filing_Type']=='DEF 14C'].index
            edgar_14A.drop(list(idx), inplace=True)
            # Consider other sources, e.g. S-1, S-3 and 424B5 #
            edgar_others = edgar_filings.loc[edgar_filings['Filing_Type'].isin(['S-1', 'S-3', 'S-3ASR', '424B5'])]
            # Create main depository web-link #
            edgar_others['Web_Depository'] = edgar_others['Web_Filing'].str.replace('-', '').str.replace(r'\.txt', '').str.strip()
            # Sort the filings by Company Name and Filing Date in ascending order, and drop the earlier filings #
            edgar_others.sort_values(by=['Company','Filing_Date'], ascending=[True,True], inplace=True)
            edgar_others.drop_duplicates(subset=['CIK'], keep='last', inplace=True)
            # Reindex DataFrames, starting from 0 #
            edgar_10K.index = np.arange(len(edgar_10K))
            edgar_14A.index = np.arange(len(edgar_14A))
            edgar_others.index = np.arange(len(edgar_others))
            # Print end status message #
            print('The filings are downloaded successfully! It took %s seconds.' % str(time.time() - start_time))
    
        else:
            print('The dataset is empty. Please check your parameters!')
    
        return edgar_filings, edgar_10K, edgar_14A, edgar_others
        
    
def edgar_preprocess_quarterly_index(data):
    ''' 
    Pre-process the data in company.idx
    Parameter:
        data     : Raw data in company.idx
    Return:
        lines    : List containing all the pre-processed data
    '''    
    # Parse the data - From line 10 onwards and skipping the last line #
    lines = data.readlines()[10:-1]
    # Convert bytes into string and split data into columns #
    lines = [re.split(r'[\s]{2,}', line.decode('utf-8').strip()) for line in lines]
    # Concatenate additional segments as part of company string #
    lines = [[' '.join(line[:-4])] + line[-4:] for line in lines if len(line) >= 5]

    return lines


#########################################################################################################################################

# Extract company's security-related details, excluding voting rights data #
def edgar_extract_tickers_shares(edgar_10K_depository, edgar_10K_filing):
    ''' 
    Extracts company's primary ticker and share-related details from its 10-K filings
    Parameters:
        edgar_10K_depository : Web-link containing company's 10-K depository
        edgar_10K_filing     : DataFrame containing 10-K filings 
    Return:
        edgar_cik_ticker     : List containing company's CIK and primary ticker
        edgar_cik_shares     : List containing company's CIK and number of outstanding shares
    ''' 
    # Create general url placeholder #
    # Note: general url contains the CIK, company name and financial year-end # 
    url = edgar_10K_depository + '/R1.htm'
    
    # Send GET request for each general url #
    try:
        table = pd.read_html(url, skiprows=1, header=0, index_col=0)[0]
    # Print error message if GET request is not successful #
    except Exception as e:
        company_name = edgar_10K_filing.loc[edgar_10K_filing['Web_Depository']==edgar_10K_depository, 'Company'].iloc[0]
        print(e, company_name, url)
    else:    
        # Capitalize each word in the index #
        table.index = table.index.str.title()
        
        # Obtain the CIK and company name from edgar_10K_depository #
        company_cik = edgar_10K_filing.loc[edgar_10K_filing['Web_Depository']==edgar_10K_depository, 'CIK'].iloc[0]
        company_name = edgar_10K_filing.loc[edgar_10K_filing['Web_Depository']==edgar_10K_depository, 'Company'].iloc[0]
        
        # Check whether there are more than one financial year-end dates #
        if len(table.loc[table.index.str.contains('Period End Date|Period Date')].dropna(axis=1).drop_duplicates()) == 1:
            # Create variable to store latest financial year-end date in Python readable format: fin_year_end_date #
            # Assuming all CIKS in R1 table have the same financial year-end date #
            try:
                fin_year_end_date = dt.datetime.strptime(
                    re.sub(r'\.', '', table.loc[table.index.str.contains('Period End Date|Period Date')].dropna(axis=1).drop_duplicates().iloc[0,0]), 
                    '%b %d, %Y')
            except Exception:
                fin_year_end_date = dt.datetime.strptime(re.sub(r'\.', '', table.columns[0]), '%b %d, %Y')
        elif len(table.loc[table.index.str.contains('Period End Date')].dropna(axis=1).drop_duplicates()) > 1:
            # Print url if R1 table has multiple financial year-end dates # 
            print('Multiple financial year-end dates are detected in (%s). Please check!' % url)

        # Create variable to store latest share available date in Python readable format: latest_share_date #
        latest_share_date = max([dt.datetime.strptime(re.sub(r'\.', '', date)[:12], '%b %d, %Y') for date in table.columns])

        # Send GET request for each url_ticker #
        try:
            response = requests.get(edgar_10K_depository + '/FilingSummary.xml')
            # Print error message if GET request is not successful #
        except Exception as e:
            print(e)
        else:
            # Parse the xml details and get primary ticker and corporate website #
            soup = BeautifulSoup(response.text, 'xml')
            partial_link_shares = soup.find('Report')['instance']
            primary_ticker = partial_link_shares.split('-')[0].upper()
            # Update partial_link_shares if it does not contain .xml extension #
            if not '.xml' in partial_link_shares:
                idx_table = pd.read_html(edgar_10K_depository)[0]
                partial_link_shares = idx_table['Name'].loc[idx_table['Name'].str.upper().str.contains('.XML') &
                        ~idx_table['Name'].str.upper().str.contains('_CAL|_DEF|_LAB|_PRE|FILINGSUMMARY')].iloc[0]  
                primary_ticker = idx_table['Name'].loc[idx_table['Name'].str.upper().str.contains('.XSD')].iloc[0].split('-')[0].upper()
            # Update primary_ticker as NaN if it contains CIK or NONE #
            if re.search(r'(C[I]?K\d+|NONE)', primary_ticker):
                primary_ticker = np.nan
            # Update primary_ticker if it contains digits #
            elif re.search(r'\d+', primary_ticker):
                try:
                    primary_ticker = table.loc[table.index.str.contains('Trading Symbol')].dropna(axis=1).drop_duplicates().iloc[0,0].upper()
                except Exception:
                    primary_ticker = np.nan
                
            # Send GET request for each share url #
            share_url = edgar_10K_depository + '/' + partial_link_shares
            try:
                tree = ET.ElementTree(file=urllib.request.urlopen(share_url))
            # Print error message if GET request is not successful #
            except Exception as e:
                print(e)
            else:        
                # Initialise variables #
                corp_website = np.nan
                namespace = {}
                
                # Parse the XML details to get corporate website and other useful tags #
                for event, (name, value) in ET.iterparse(urllib.request.urlopen(share_url), ['start-ns']):
                    # Get corporate website #
                    if name.upper() == primary_ticker:
                        corp_website = re.sub(r'/\d{8}', '', value)
                        # Update corp_website as NaN if it contains CIK or only primary ticker #
                        if re.search(r'HTTP://C[I]?K\d{10}', corp_website.upper()):
                            corp_website = np.nan
                        elif primary_ticker != np.nan and (corp_website.upper() == 'HTTP://' + primary_ticker):
                            corp_website = np.nan
                        
                    # Get xbrli, xbrldi, dei and gaap #
                    elif name.upper() == 'XBRLI' or '/instance' in value:
                        namespace.update({'xbrli':value})
                    
                    elif name.upper() == 'XBRLDI':
                        namespace.update({'xbrldi':value})
                    
                    elif name.upper() == 'DEI':
                        namespace.update({'dei':value})
                    
                    elif name.upper() == 'US-GAAP':
                        namespace.update({'gaap':value})
                    
                # Reformat namespace #
                namespace={k:'{' + v + '}' for k, v in namespace.items()}
                      
                # Parse the same XML details to get other share-related information #
                root = tree.getroot() 
                if latest_share_date != fin_year_end_date:
                    shares_r1 = edgar_xml_translator(reference_date = latest_share_date, 
                                                     root = root, 
                                                     namespace = namespace,
                                                     company_cik = company_cik,
                                                     company_name = company_name,
                                                     extract_main = True)
                    shares_others = edgar_xml_translator(reference_date = fin_year_end_date, 
                                                         root = root, 
                                                         namespace = namespace,
                                                         company_cik = company_cik,
                                                         company_name = company_name,
                                                         extract_main = False)
                else:
                    shares_others = edgar_xml_translator(reference_date = fin_year_end_date, 
                                                         root = root, 
                                                         namespace = namespace,
                                                         company_cik = company_cik,
                                                         company_name = company_name,
                                                         extract_main = False)
             
                # Append CIK, company name, corporate website, financial year-end, primary ticker: edgar_cik_tickers #
                edgar_cik_tickers.append([
                        company_cik,
                        company_name,
                        corp_website,
                        fin_year_end_date,
                        primary_ticker
                        ])
            
                # Append CIK, company name, context ID, class shares, number of outstanding shares and cutoff date: edgar_cik_shares #
                try:
                    shares_r1
                except Exception:
                    shares_r1 = []
                for share_r1 in shares_r1:
                    if len(share_r1) > 0:
                        edgar_cik_shares.append(list(share_r1[0]))
                    
                try:
                    shares_others
                except Exception:
                    shares_others = []
                for share_others in shares_others:
                    if len(share_others) > 0:
                        edgar_cik_shares.append(share_others)
    
    return edgar_cik_tickers, edgar_cik_shares


def edgar_xml_translator(reference_date, root, namespace, company_cik, company_name, extract_main):
    ''' 
    Extracts company's share-related details from its 10-K filings
    Parameter:
        reference_date       : Reference date, e.g. latest share available date, for extraction
        xml_tree             : Tree obtained from xml parsing
        namespace            : Dictionary containing root-level information
        company_cik          : Company's CIK that is queried. Obtained from edgar_10K filings
        company_name         : Company's name that is queried. Obtained from edgar_10K filings
        extract_main         : Boolean value -
                                True means the share-related data is extracted from R1 table.
                                False means the share-related data is extracted from all tables in filings.
    Return:
        shares_r1         : List containing tuples of share-related information from R1 table only
        shares_others     : List containing lists of share-related information from other tables
    '''    
    # Get context ID and element for the reference date #
    context_ids = [(child.attrib['id'], reference_date, child.find(namespace['xbrli']+'entity').getchildren())
                    for child in root.getchildren() \
                    if 'context' in child.tag \
                    for grand in child.getchildren() \
                    for great in grand.getchildren() \
                    if 'instant' in great.tag and dt.datetime.strptime(great.text.strip(), '%Y-%m-%d')==reference_date]
                
    # Loop through each context ID to obtain the outstanding shares #
    if extract_main:
        # Extract share-related data from R1 table only #
        shares_r1 = []
        for context_id, date, element in context_ids:
            if len(element) == 1 and 'IDENTIFIER' in element[-1].tag.upper():
                shares_r1.append([(company_cik,
                                   company_name,
                                   context_id, 
                                   re.sub(r'([A-Z])' , r' \1', re.sub(r'{.*}', '', child.tag)).strip(),
                                   re.sub(r'([A-Z])' , 
                                          r' \1', 
                                          re.sub(r'SharesOutstanding', '', re.sub(r'{.*}', '', child.tag)).strip()).strip(),
                                   float(child.text), 
                                   date) 
                                   for child in root.getchildren() \
                                   if ('OUTSTANDING' in child.tag.upper() and \
                                       'contextRef' in child.attrib and \
                                       child.attrib['contextRef'] == context_id and \
                                       child.text is not None and \
                                       isposfloat(child.text))])
                                   
            elif 'SEGMENT' in element[-1].tag.upper():
                shares_name = re.sub(r'([A-Z])', r' \1', element[-1].getchildren()[0].text.split(':')[-1]).strip()
                if 'Member' in shares_name.split()[-1]:
                    shares_name = ' '.join([share_name for share_name in shares_name.split('Member')]).strip()
                    shares_r1.append([(company_cik,
                                       company_name,
                                       context_id,
                                       re.sub(r'([A-Z])' , r' \1', re.sub(r'{.*}', '', child.tag)).strip(),
                                       shares_name, 
                                       float(child.text), 
                                       date)
                                       for child in root.getchildren() \
                                       if ('OUTSTANDING' in child.tag.upper() and \
                                           'contextRef' in child.attrib and \
                                           child.attrib['contextRef'] == context_id and \
                                           child.text is not None and \
                                           isposfloat(child.text))])
        return shares_r1
    
    else:
        # Extract share-related data from other tables #
        shares_others = []
        voting_rights = []
        for context_id, date, element in context_ids:
            if len(element) == 1 and 'IDENTIFIER' in element[-1].tag.upper():
                for child in root.getchildren():
                    if ('contextRef' in child.attrib and child.attrib['contextRef'] == context_id) and \
                    re.search(r'(COMMON|PREFER.*|CAPITAL)(SHARE[S]?|STOCK|UNIT[S]?)', child.tag.upper()) and \
                    re.search(r'(OUTSTANDING|PARORSTATEDVALUE|PARVALUE)', child.tag.upper()) and \
                    child.text is not None and \
                    isposfloat(child.text):
                        shares_others.append([company_cik,
                                              company_name,
                                              context_id, 
                                              re.sub(r'([A-Z])' , r' \1', re.sub(r'{.*}', '', child.tag)).strip(), 
                                              re.sub(r'([A-Z])' , 
                                                     r' \1', 
                                                     re.sub(r'SharesOutstanding', '', re.sub(r'{.*}', '', child.tag)).strip()).strip(), 
                                              float(child.text), 
                                              date]) 
                    
            elif 'SEGMENT' in element[-1].tag.upper():
                shares_name = re.sub(r'([A-Z])', r' \1', element[-1].getchildren()[0].text.split(':')[-1]).strip()
                try:
                    'Member' in shares_name.split()[-1]
                except Exception:
                    continue
                else:
                    shares_name = ' '.join([share_name for share_name in shares_name.split('Member')]).strip()
                    for child in root.getchildren():
                        if ('contextRef' in child.attrib and child.attrib['contextRef'] == context_id) and \
                        re.search(r'(COMMON|PREFER.*|CAPITAL)(SHARE[S]?|STOCK|UNIT[S]?)', child.tag.upper()) and \
                        re.search(r'(OUTSTANDING|PARORSTATEDVALUE|PARVALUE)', child.tag.upper()) and \
                        child.text is not None and \
                        isposfloat(child.text):
                            shares_others.append([company_cik,
                                                  company_name,
                                                  context_id, 
                                                  re.sub(r'([A-Z])' , r' \1', re.sub(r'{.*}', '', child.tag)).strip(), 
                                                  shares_name, 
                                                  float(child.text), 
                                                  date])
            
        # Get securities' voting rights if provided in other tables #
        context_ids_votes = [(child.tag, child.attrib['contextRef'])
                            for child in root.getchildren() \
                            if ('contextRef' in child.attrib and \
                                re.search(r'(VOTINGRIGHTS|NUMBEROFVOTES)', child.tag.upper()) and \
                                child.text is not None)]

        context_ids_voting = []
        for child in root.getchildren():
            for ele, context_id_votes in context_ids_votes:
                if 'context' in child.tag and child.attrib['id'] == context_id_votes:
                    element = child.find(namespace['xbrli']+'entity').getchildren()
                    period_end = child.find(namespace['xbrli']+'period').getchildren()[-1].text.strip()
                    
                    if len(element) == 1 and 'IDENTIFIER' in element[-1].tag.upper():
                        context_ids_voting.append((context_id_votes, 
                                                   re.sub(r'([A-Z])' , r' \1', re.sub(r'{.*}', '', ele)).strip(),
                                                   dt.datetime.strptime(period_end, '%Y-%m-%d')))
                    
                    elif 'SEGMENT' in element[-1].tag.upper():
                        grandchildren = element[-1].getchildren()
                        if len(grandchildren) > 1:
                            shares_names = [re.sub(r'([A-Z])', r' \1', grandchild.text.split(':')[-1].strip()) 
                                            for grandchild in grandchildren
                                            if re.search(r'([Ss]eries|[Cc]lass)', grandchild.text)]
                        elif len(grandchildren) == 1:
                            shares_names = [re.sub(r'([A-Z])', r' \1', grandchild.text.split(':')[-1].strip()) 
                                            for grandchild in grandchildren]
                        
                        if len(shares_names) > 0:
                            context_ids_voting.append((context_id_votes, 
                                                       shares_names[-1],
                                                       dt.datetime.strptime(period_end, '%Y-%m-%d')))

        context_ids_votes = [(context_id, ' '.join([name for name in names.split('Member')]).strip(), period) \
                             for context_id, names, period in context_ids_voting]

        voting_rights.append([(company_cik,
                              company_name,
                              context_id,
                              re.sub(r'([A-Z])' , r' \1', re.sub(r'{.*}', '', child.tag)).strip(),
                              shares_name,
                              child.text.strip(),
                              period)
                              for child in root.getchildren() \
                              for context_id, shares_name, period in context_ids_votes
                              if ('contextRef' in child.attrib and \
                                  child.attrib['contextRef'] == context_id and \
                                  re.search(r'(VOTINGRIGHTS|NUMBEROFVOTES)', child.tag.upper()) and \
                                  child.text is not None)])
        
        # Append securities' voting rights in shares_others #
        if len(voting_rights) > 0:
            for voting_right in voting_rights[0]:
                shares_others.append(list(voting_right))
        
        return shares_others


#########################################################################################################################################
        
# Extract company's voting rights data #
def edgar_extract_votes(url):
    ''' 
    Connect to EDGAR web-links and extract relevant voting rights_data
    Parameter:
        url            : Edgar filing url (i.e. edgar_10K, edgar_14A)
    Return:
        share_rights   : List containing tuples that have the relevant voting rights data
    '''   
    # GET request #
    try:
        response = requests.get(url)
    except Exception as e:
        print(e)
    # Parse the html details using BeautifulSoup #
    parsers = ['xml', 'html5lib', 'lxml']
    for parser in parsers:
        # Get all html tags that might contain useful information and compile into a list: bodies #
        try:
            soup = BeautifulSoup(response.text, parser)
        except Exception:
            bodies = []
            continue
        else:
            # Kill all script and style elements in soup #
            for script in soup(['script', 'style']):
                script.extract()
            # Extract body text #    
            bodies = soup.find_all('body')
            if len(bodies) != 0:
                break
            else:
                bodies = soup.find_all('BODY')
                if len(bodies) != 0:
                    break
                else:
                    bodies = []
                    continue
        
    # Perform pre-processing on all the body text and tokenize them for NLP #
    for body in bodies:
        # Perform pre-processing #
        text = edgar_preprocess_filing_text(body)
        mod_sentences = edgar_sent_enhancer(text, process_flag = True)

        # Apply spaCy's NLP model to tokenised sentences: corpora #
        corpora = [nlp(mod_sentence) for mod_sentence in mod_sentences]
        # Apply spaCy's noun chunks to each corpus: chunks #
        chunks = [[(chunk.text, chunk.root.text, chunk.root.dep_, chunk.root.head.text, chunk.root.head.pos_) \
                   for chunk in corpus.noun_chunks] \
                   for corpus in corpora]

        # Narrow down the choice of chunks and extract class shares and their associated voting power: vote_chunks #    
        vote_chunks = [edgar_spacy_chunk(corpora[i], chunks[i], text) for i in range(len(corpora))]
            
        # Merge the chunks into one list for each company #
        merged_chunks = [chunk for chunks in vote_chunks if len(chunks) != 0 for chunk in chunks] 
#        if len(merged_chunks) == 0:
#            vote_chunks = [edgar_regex_chunk(corpora[i]) for i in range(len(corpora))] 
#            merged_chunks = [chunk for chunks in vote_chunks if len(chunks) != 0 for chunk in chunks] 
  
        # Append information to share_rights #
        if len(merged_chunks) != 0:
            for chunk in merged_chunks:
                share_rights.append((edgar_filing.loc[edgar_filing['Web_Filing'] == url]['Company'].iloc[0], 
                                     edgar_filing.loc[edgar_filing['Web_Filing'] == url]['Ticker'].iloc[0],
                                     edgar_filing.loc[edgar_filing['Web_Filing'] == url]['CIK'].iloc[0],
                                     url, chunk[0].title(), chunk[1], chunk[-1]))     
    
    return share_rights


def edgar_preprocess_filing_text(unprocessed_text):
    ''' 
    Tokenise selected text or corpus into sentences
    Parameters:
        unprocessed_text : Raw text extracted from specific filing 
    Return:
        text             : Text after pre-processing 
    '''  
    # Perform pre-processing #
    text = unprocessed_text.text.strip()             
    text = re.sub('[\n\xc2\xa0]', ' ', text)
    text = re.sub('•', ' ', text)
    # Identify wrongly formatted class stocks and translate them into proper format #
    text = re.sub(r'\w*[Cc]lass(\w)', r'Class \1', text)
    text = re.sub(r'\w*[Ss]eries(\w)', r'Series \1', text)
    # Remove all redundant spaces #
    text = re.sub(r'[\s]{2,}', ' ', text) 
            
    # Capitalise the potential class and common stocks in text #
    for match in re.finditer('|'.join(stock_name), text):
        if match.group(0) is not None:
            text = re.sub(match.group(0), match.group(0).title(), text)
            
    # Reduce target words, e.g. voting, to its root form, i.e. vote # 
    text = re.sub(r'\bvotes\b', 'vote', text)
    # Create spaces for start of new sentences #
    text = re.sub(r'([^\w\s,-/])([A-Z0-9])', r'\1 \2', text)
    # Standardise the case for the phrase non-voting #
    text = re.sub(r'[Nn]on-[Vv]ot[a-z]+', 'non-voting', text)
    # Identify potential non-voting stocks and translate their voting power to 0 votes #
    text = re.sub(r'(no(?:\svoting)? right[s]?)|do not(?:\s\w+) any \bvot[a-z]+\b(?:\sright[s]?)?', 'has 0 vote', text)
    
    return text
    

def edgar_sent_enhancer(text, process_flag = True):
    ''' 
    Tokenise selected text or corpus into sentences
    Parameters:
        text            : Text before NLTK sentence tokenisation
        process_flag    : Flag to indicate whether further pre-processing is required 
                          Default value is True
    Return:
        mod_sentences   : Sentences after NLTK sentence tokenisation
    '''  
    # Tokenise sentence in text using NLTK: sents #
    sents = nltk.sent_tokenize(text)
    
    # Include sentences that only contain the word 'vote' #
    vote_sents = [sent for sent in sents \
                  if re.search(r'\bvote\b', sent) and len(sent.split()) < 1000]
    
    # Apply spaCy's POS tagging for each sentence: pos_sents #
    pos_sents = [nlp(vote_sent) for vote_sent in vote_sents]
    
    # Apply Dependency tagging to filter sentences further #
    full_sents = [(pos_sent.text, pos_sent) for pos_sent in pos_sents \
                  for token in pos_sent \
                  if re.search('nummod', token.dep_)]
    full_sents = list(dict.fromkeys(full_sents))
    
    # Break the list into two separate lists, i.e. one with class securities and the other without class securities #
    security_sents = [(full_sent, corpus) for full_sent, corpus in full_sents if re.search('|'.join(stock_name), full_sent)]
    no_security_sents = [(full_sent, corpus) for full_sent, corpus in full_sents if not re.search('|'.join(stock_name), full_sent)]
    
    # Run edgar_extend_corpus function for no_security_sents #
    no_security_sents = edgar_extend_corpus(no_security_sents, sents, sent_dispersion = 3)
    
    # Combine security_sents and no_security_sents into one consolidated list # 
    full_sents = security_sents + no_security_sents
    
    # Remove duplicates #
    sents = list(dict.fromkeys(full_sents))
    
    # Use process_flag to determine whether further processing is required #            
    # Further processing is required #    
    if process_flag:
        # Initialise sentences #
        sentences = []
        
        # Identify all bracketed numerical voting power (if any) #
        for sent, corpus in sents:
            # Bracketed numerical voting power #
            if len(re.findall(r'\((\d?\.\d+|\d+[/]\d+|\d+)\)(?:\s\w+)* vote', sent)) != 0:
                # Create a variable to store found results #
                found = re.findall(r'\((\d?\.\d+|\d+[/]\d+|\d+)\)(?:\s\w+)* vote', sent)
                # Apply spaCy NLP and get part-of-speech (POS) #
                tokens = [(token.text, token.pos_) for token in corpus]
                
                # Remove all intermediate words separating bracketed numerical voting power and 'vote' #
                for target in found:
                    mod_tokens = []
                    for seq, token in enumerate(tokens):
                        # Chain all the relevant tokens together #
                        if target == token[0] and tokens[seq-1][0] == '(':
                            for counter in range(seq, 0, -1):
                                if tokens[counter][1] == 'ADP' or tokens[counter][1] == 'VERB':           
                                    mod_tokens.append([token[0] for token in tokens[:counter+1]])
                                    mod_tokens.append([target])
                                    break
                            for counter in range(seq, len(tokens)):
                                if tokens[counter][0] == 'vote':
                                    mod_tokens.append([token[0] for token in tokens[counter:-1]])
                                    break
                            break
                    # Remake sentence #
                    mod_sent = ' '.join([' '.join(mod_token) for mod_token in mod_tokens]) + '.'
                    mod_sent = re.sub(r'(?:\(\s)(.)(?:\s\))', r'(\1)', mod_sent)
            
                # Append mod_sent to sentences #
                sentences.append(mod_sent)
            
            # No bracketed numerical voting power #
            else:
                sentences.append(sent)                
        
        # Ensure name completeness for potential stocks #
        mod_sentences = []
        for sentence in sentences:
            if len(list(set(re.findall(r'((?:[Pp]refer[a-z]*|[Cc]ommon|[Cc]apital|[Oo]rdinary|[Ee]xchangeable)' + \
                                      '(?:[Ss]hare[s]?|[Ss]tock))', sentence)))) == 1:
                if re.search(r'[Ss]eries', sentence):
                    sentence = re.sub('\s{2,}', ' ', re.sub(r'([Ss]eries) ([A-Z]?[-]?[0-9]?)(,| and )', 
                                  r' Series \2 %s\3' 
                                  % re.findall(r'((?:[Pp]refer[a-z]*) (?:[Ss]hare[s]?|[Ss]tock))', sentence)[0], 
                                  sentence))
                elif re.search(r'[Cc]lass', sentence):
                    sentence = re.sub('\s{2,}', ' ', re.sub(r'([Cc]lass) ([A-Z]?[-]?[0-9]?)(,| and )', 
                                  r' Class \2 %s\3' 
                                  % re.findall(r'((?:[Pp]refer[a-z]*|[Cc]ommon|[Cc]apital|[Oo]rdinary|[Ee]xchangeable)' + \
                                                  '(?:[Ss]hare[s]?|[Ss]tock|))', sentence)[0], 
                                  sentence))
                mod_sentences.append(sentence)
            else:
                mod_sentences.append(sentence)
                
    # No further processing is required #    
    else:
        mod_sentences = sents

    return mod_sentences           


def edgar_extend_corpus(no_security_sents, sents, sent_dispersion = 3):
    ''' 
    Identify the potential class securities and associated voting rights from extended corpus 
    Parameters:
        no_security_sents   : List of sentences that contain no class securities
        sents               : Tokenised sentences after NLTK sentence tokenisation 
        sent_dispersion     : Number of preceding and subsequent sentences to consider for the extended corpus.
                              Default value is 3
    Return:
        modified_corpus : Enhanced corpus after considering the additional sentences
    '''                     
    # Initialise parameters #
    idxs = []
    modified_corpus = []
    
    # Find out index of corpus with voting power but no associated class security #
    for no_security_sent, corpus in no_security_sents:
        try:
            sents.index(no_security_sent)
        except Exception:
            continue
        else:        
            idxs.append(sents.index(no_security_sent)) 
    
    # Scan preceding and subsequent sentences for class securities and combine with no_security_sent #
    if idxs:
        for idx in idxs:
            modified_corpus_partial = []
            
            for dispersion in range(1, sent_dispersion + 1):
                for index, sent in enumerate(sents[max(0, idx - dispersion):min(idx + dispersion + 1, len(sents))]):
                    if re.search('|'.join(stock_name), sent) and \
                       not re.search(r'\bno\b.*\boutstanding\b', sent) and \
                       index < dispersion:
                        modified_corpus_partial.append((' '.join(sents[max(0, idx - dispersion):(idx + 1)]), corpus))
                        # Break inner loop if class security is found #
                        break
            
                    elif re.search('|'.join(stock_name), sent) and \
                         not re.search(r'\bno\b.*\boutstanding\b', sent) and \
                        index > dispersion:
                        modified_corpus_partial.append((' '.join(sents[idx: min(idx + dispersion + 1, len(sents))]), corpus))
                        # Break inner loop if class security is found #
                        break
            
                # Check whether modified_corpus_partial is filled #
                if modified_corpus_partial:
                    # Break inner loop if modified_corpus_partial is filled #
                    break
                else:
                    # Continue with loop #
                    continue
                
            # Append results in modified_corpus #
            if modified_corpus_partial:
                for ele in modified_corpus_partial:
                    modified_corpus.append(ele)
        
    return modified_corpus


def edgar_spacy_chunk(corpus, chunks, text):
    ''' 
    Narrow down sentences that contain information about stocks and associated voting rights 
    Parameters:
        corpus         : Processed corpus by spaCy
        chunks         : Tokenised chunks using spaCy
        text           : Original text after the initial pre-processing
    Return:
        vote_chunks    : List containing summarized class shares and voting rights information
    '''  
    # Run edgar_security_vote function to classify information into class security and voting rights lists #
    stock_indicator, vote_indicator = edgar_security_vote(corpus, chunks)

    # Check if stock_indicator == vote_indicator, i.e. one-to-one relationship # 
    # One-to-one relationship # 
    if (len(stock_indicator) == len(vote_indicator) and (len(stock_indicator) != 0) and (len(vote_indicator) != 0)):
        # Apply First-In-First-Out (FIFO) for stock-vote assignment #
        vote_chunks = [(stock_indicator[i], vote_indicator[i], corpus.text) for i in range(len(stock_indicator))]
    
    # Many-to-one relationships # 
    elif len(stock_indicator) > len(vote_indicator) and \
            len(stock_indicator) != 0 and \
            len(vote_indicator) == 1:
        # Apply filtering to exclude potential cumulative votes #
        if re.search(r'\btotal\b', corpus.text) and len(nltk.sent_tokenize(corpus.text)) == 1:
            vote_chunks = []
        else:
            vote_chunks = [(stock_indicator[i], vote_indicator[0], corpus.text) for i in range(len(stock_indicator))]  
            
    # One-to-Many relationships, e.g. class security is not in the same line as voting power # 
    elif len(stock_indicator) == 1 and len(vote_indicator) != 0:
        # Apply filtering to exclude potential cumulative votes #
        if re.search(r'\btotal\b', corpus.text) and len(nltk.sent_tokenize(corpus.text)) == 1:
            vote_chunks = []       
        else:
            vote_chunks = [(stock_indicator[0], vote_indicator[i], corpus.text) for i in range(len(vote_indicator))]      
        
    # No stock_indicator and vote_indicator details #
    else:
        vote_chunks = []
     
    return vote_chunks


def edgar_security_vote(corpus, chunks):
    ''' 
    Separate chunks into class security and voting rights 
    Parameter:
        corpus          : Processed corpus by spaCy
        chunks          : Tokenised chunks using spaCy
    Return:
        stock_indicator : List containing class securities
        vote_indicator  : List containing voting rights information
    '''  
    # Initialise placeholders #
    stock_indicator = []
    vote_indicator = []
    
    # Fill stock_indicator with class share information in chunks #
    for chunk in chunks:
        # Chunk contains potential stocks #
        if re.search('|'.join(stock_name), chunk[0]):
            stock_indicator.append(chunk[0])
#        # Chunk contains abbreviated stock units, e.g. PSUs, RSUs etc. #
#        elif re.search(r'[A-Z]SU[s]?', chunk[0]):
#            stock_indicator.append(chunk[0])
        
        # Chunk contains other variants of potential stocks #
        elif (lemmatizer(chunk[1].lower(), 'VERB')[0] == 'stock' and \
              len(chunk[0].split()) > 1 and \
#              not re.search(r'\b[Ee]ach\b', chunk[0].lower()) and \
              not re.search(r'\d', chunk[0]) and \
              re.search('|'.join(stock_name), chunk[0])) or \
             (lemmatizer(chunk[1].lower(), 'VERB')[0] == 'share' and \
              len(chunk[0].split()) > 1 and \
#              not re.search(r'\beach\b', chunk[0].lower()) and \
#              not re.search(r'\ba\b', chunk[0].lower()) and \
              not re.search(r'\d', chunk[0]) and \
              re.search('|'.join(stock_name), chunk[0])):
            if re.search(r'\b(and|or)\b', chunk[0]) and 'stock' in chunk[0].lower():
                for elem in re.split(r'\b(and|or)\b', chunk[0].lower()):
                    stock_indicator.append(elem.strip().title())
            elif re.search(r'\b(and|or)\b', chunk[0]) and 'share' in chunk[0].lower():     
                for elem in re.split(r'\b(and|or)\b', chunk[0].lower()):
                    stock_indicator.append(elem.strip().title())
            else:
                stock_indicator.append(chunk[0])
                
        # Identify potential class security using regular expression #
        else:  
            for match in re.finditer('|'.join(stock_name), corpus.text):
                if match.group(0) is not None:
                    stock_indicator.append(match.group(0).title())
    
    # Check whether stock_indicator is filled with class shares #    
    if len(stock_indicator) != 0:
        stock_indicator = list(dict.fromkeys(stock_indicator))
        temp = []
        # Remove potential duplicates in stock_indicator #
        for stock in stock_indicator:
            temp.append(' '.join([item for item in stock.split() if item.lower() not in stopWords]))
        if len(temp) != 0:
            temp = [re.sub(r'[Ss]hares', 'Share', unit) for unit in temp]
            stock_indicator = list(dict.fromkeys(temp))
        # Fill vote_indicator with voting information if stock_indicator is not empty #
        for seq, chunk in enumerate(chunks):
            if chunk[1] == 'vote' and len(chunk[0].split()) == 2 and isposfloat(word2num(chunk[0].split()[0])):
                # Determine if the voting right chunk has omitted other information, e.g. fractions #
                if chunk[-2].lower() == 'of' and isposfloat(word2num(chunks[max(0, seq - 1)][0])):
                    votes_per_share = word2num(chunks[max(0, seq - 1)][0]) * word2num(chunk[0].split()[0])
                    vote_indicator.append(votes_per_share)
                else:
                    votes_per_share = word2num(chunk[0].split()[0])
                    if votes_per_share < 1000000:
                        vote_indicator.append(votes_per_share)
                        
            elif chunk[1] == 'vote' and len(chunk[0].split()) == 3 and \
            isposfloat(word2num(chunk[0].split()[0])) and isposfloat(word2num(chunk[0].split()[1])) and \
            (word2num(chunk[0].split()[0]) == word2num(chunk[0].split()[1])):
                votes_per_share = word2num(chunk[0].split()[0])
                if votes_per_share < 1000000:
                    vote_indicator.append(votes_per_share)
                    
            elif lemmatizer(chunk[1], 'VERB')[0] == 'right' and len(chunk[0].split()) == 3 and \
                 re.search(r'\bvote\b', chunk[0].split()[1]) and isposfloat(word2num(chunk[0].split()[0])):
                votes_per_share = word2num(chunk[0].split()[0])
                if votes_per_share < 1000000:
                    vote_indicator.append(votes_per_share)                        
            
            elif lemmatizer(chunk[1], 'VERB')[0] == 'power' and len(chunk[0].split()) == 3 and \
                 re.search(r'\bvote\b', chunk[0].split()[1]) and isposfloat(word2num(chunk[0].split()[0])):
                votes_per_share = word2num(chunk[0].split()[0])
                if votes_per_share < 1000000:
                    vote_indicator.append(votes_per_share) 
    
    return stock_indicator, vote_indicator


#########################################################################################################################################
    
# Map EDGAR and SBSU datasets by TICKER #
def edgar_sbsu_mapping(edgar_tickers, filename, download_data = True):
    ''' 
    Maps EDGAR data to SBSU dataset
    Parameter:
        edgar_tickers     : Edgar ticker dataset extracted from company's filings
        filename          : Excel file containing the latest quarterly universe
        download_data     : Download data after mapping. Default value is True.
    Return:
        edgar_votes       : Pandas DataFrame containing the relevant voting rights data
    '''     
    # Initialise an empty array with shape (0,1): cik_ticker_arr #
    cik_ticker_arr = np.empty((0,2), str)
    # Connect to EDGAR archives and get ticker.txt #
    data = urllib.request.urlopen('https://www.sec.gov/include/ticker.txt')
    # Preprocess the data in ticker.txt #
    lines = data.readlines()
    lines = [re.split(r'[\t]', line.decode('utf-8').strip()) for line in lines]
    lines = [line for line in lines if len(line) == 2]
    # Vectorize column data into array #
    cik_ticker_arr = np.vstack([cik_ticker_arr, np.array([line for line in lines])])
    # Convert into Pandas DataFrame # 
    cik_tickers = pd.DataFrame(cik_ticker_arr, columns=['TICKER', 'CIK'])
    # Change all the tickers to upper case #
    cik_tickers['TICKER'] = cik_tickers['TICKER'].str.upper()
   
    # Replace dirty CIKs #
    for ticker, clean_cik in clean_ciks:
        cik_tickers.loc[cik_tickers['TICKER'] == ticker, 'CIK'] = clean_cik
    
    # Combine the cik_tickers dataset with the tickers extracted from companies' filings #
    tickers_new = edgar_tickers[['PRIMARY_TICKER','CIK']]
    tickers_new.columns = ['TICKER', 'CIK']
    cik_tickers = pd.concat([cik_tickers, tickers_new])
    cik_tickers.drop_duplicates(subset = ['TICKER'], inplace = True)
    
    # Load the SBSU dataset for latest quarterly rebalancing # 
    sbsu_full = pd.read_excel(filename)
    sbsu_US = sbsu_full.loc[(sbsu_full['exchCode'] == 'US') & (sbsu_full['sciBeta_status'] == 'Active'), 
                            ['sciBetaId', 'sciBeta_company_name', 'RIC']]
    
    # Join the SBSU dataset with cik_tickers using 'TICKER' column #
    sbsu_US['RIC'] = sbsu_US['RIC'].str.upper()
    sbsu_US_merged = pd.merge(sbsu_US, cik_tickers, how = 'left', left_on = 'RIC', right_on = 'TICKER')
    
    # Join the merged SBSU dataset with edgar_tickers #
    sbsu_US_merged = pd.merge(sbsu_US_merged, tickers_new, how = 'left', on = 'CIK')
    
    # Download the merged dataset #
    if True:
        sbsu_US_merged.to_excel('results.xlsx', index = False)
        sbsu_US_merged.to_excel(r'C:\Users\C_YEE\Dropbox\ESG Research\results.xlsx', index = False)


# Convert textual voting rights into numerical form #                          
def word2num(textnum, numwords = {}):
    ''' 
    Convert words to numbers
    Parameters:
        textnum              : Numbers that are spelt out in text
        numwords             : Dictionary containing the scales and increments
    Return:
        result+current       : Numbers in numerical format after translation
    ''' 
    if not numwords:
        # Initialise whole numbers #
        units = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', \
                 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
        tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']
        scales = ['hundred', 'thousand', 'million', 'billion', 'trillion']
        
        # Initialise other common vocabulary #
        numwords['no vote'] = numwords['no voting power'] = numwords['no voting rights'] = numwords['non-voting'] = (1, 0)
#        numwords['any'] = (1, 0)
        
        # Create scale and increment and store in dictionary: numwords #
        for idx, word in enumerate(units):  
            numwords[word] = (1, idx)
        for idx, word in enumerate(tens):       
            numwords[word] = (1, idx * 10)
        for idx, word in enumerate(scales): 
            numwords[word] = (10 ** (idx * 3 or 2), 0)
    
    # Initialise fraction dictionary: ordinal_words #
    ordinal_words = {'half': float(1/2), 'third': float(1/3), 'fourth': float(1/4), 'quarter': float(1/4), 'fifth': float(1/5), 
                     'sixth': float(1/6), 'seventh': float(1/7), 'eighth': float(1/8), 'ninth': float(1/9), 
                     'tenth': float(1/10), 'eleventh': float(1/11), 'twelfth': float(1/12), 'thirteenth': float(1/13), 
                     'fourteenth': float(1/14), 'fifteenth': float(1/15), 'sixteenth': float(1/16), 'seventeenth': float(1/17), 
                     'eighteenth': float(1/18), 'nineteenth': float(1/19), 'twentieth': float(1/20), 'hundredth': float(1/100),
                     'thousandth': float(1/1000)}    
    
    # Replace hyphen in text with space and change the text to lower case #
    textnum = textnum.replace('-', ' ').lower()
    # Replace comma in text with space #
    textnum = textnum.replace(',', '')
    # Remove all stopWords #
#    textnum = ' '.join([item for item in textnum.split() if item.lower() not in stopWords])
    
    # Compute the results #
    current = result = counter = 0
    if len(textnum.split()) == 0:
        scale, increment = (0, -np.inf)
    else:
        counter += 1
        for seq, word in enumerate(textnum.split()):
            if not re.search(r'\bof\b', word):
                if re.sub(r'(\w+)s\b', r'\1' , word) in ordinal_words:
                    # Assign scale and increment for fractions #
                    scale, increment = (ordinal_words[re.sub(r'(\w+)s\b', r'\1' , word)], 0)
                elif word in numwords:
                    # Assign scale and increment for decimals and whole numbers # 
                    if re.search(r'\bof\b', textnum) and seq == len(textnum.split()) - 1:
                        scale, increment = (numwords[word][1], 0)
                    else:
                        scale, increment = numwords[word]
                # Assign scale and increment for numerical real numbers # 
                elif isposfloat(word):
                    scale, increment = (1, float(word))
                elif re.search(r'\d+[/]\d+\w*', word):
                    temp = re.search('\d+[/]\d+', word)[0]
                    scale, increment = (0, float(temp.split('/')[0]) / float(temp.split('/')[1]))
                else:
                    # Assign scale and increment for invalid word #
                    scale, increment = (0, -np.inf)
                    #raise Exception('Illegal word: ' + word)
            
                current = current * scale + increment
                if scale > 100:
                    result += current
                    current = 0
            
    if re.search(r'\bof\b', textnum) and float(result + current) == 0:      
        return float(-np.inf)
    elif counter == 0 and float(result + current) == 0:
        return float(-np.inf)
    else:
        return float(result + current)


def isposfloat(value):
    ''' 
    Test whether a value is a positive float
    Parameter:
        value        : Input value
    Return:
        True/False   : Boolean indicating whether the input value is a positive float
    ''' 
    try:
        if float(value) >= 0:
            return True
        else:
            return False 
    except ValueError:
        return False

def convert2df(lsts, num_of_cols=4, col_names=[], output_excel=0, filename='default'):
    ''' 
    Vectorise lists into a DataFrame
    Parameter:
        lsts           : Input list of lists
        num_of_cols    : Number of columns for DataFrame. Default value is 4
        col_names      : List containing column names
        output_excel   : Option to download DataFrame in Excel. Default value is 0, i.e. Do not output
        filename       : Name of output Excel file. Default value is default
    Return:
        df             : Pandas DataFrame containing the relevant dataset
    ''' 
    # Create empty array #
    arr = np.empty((0, num_of_cols), str)
    
    # Vectorise data #
    arr = np.vstack([arr, np.array([lst for lst in lsts])])
    
    # Convert vector in DataFrame # 
    df = pd.DataFrame(arr, columns=col_names)
    
    # Drop duplicates in DataFrame #
    df.drop_duplicates(subset=col_names, inplace=True)

    # Output DataFrame into Excel if output_excel is not equal to 0 #
    if output_excel != 0:
        df.to_excel(r'%s.xlsx' % filename, index=False)
        df.to_excel(r'C:\Users\C_YEE\Dropbox\ESG Research\%s.xlsx' % filename, index=False)
    return df


def main():    
    try:
        edgar_filings, edgar_10K, edgar_14A, edgar_others = edgar_download(start_year=2018, end_year=2019, end_quarter=3)    
    except Exception as e:
        print(e)
        input('Press Enter to continue!')


if __name__ == '__main__':
    main()

try:
    edgar_filings, edgar_10K, edgar_14A, edgar_others = edgar_download(start_year=2018, end_year=2019, end_quarter=3)
except Exception as e:
    print(e)
    input('Press Enter to continue!')
    
# Commence download of EDGAR data #
edgar_cik_tickers=[] 
edgar_cik_shares=[]
edgar_10K_depositories = list(edgar_10K['Web_Depository'])
func = partial(edgar_extract_tickers_shares, edgar_10K_filing = edgar_10K)
with ThreadPoolExecutor(max_workers=30) as executor:
    executor.map(func, edgar_10K_depositories)
tickers = convert2df(edgar_cik_tickers, num_of_cols=5, 
                    col_names=['CIK', 'EDGAR_COMPANY_NAME', 'CORP_WEBSITE', 'YEAR_END', 'EDGAR_TICKER'], 
                    output_excel=1, filename='TICKERS')
outstanding_shares = convert2df(edgar_cik_shares, num_of_cols=7,
                                col_names=['CIK', 'EDGAR_COMPANY_NAME', 'DETAILS', 'REF_NAME_LONG', 'REF_NAME_SHORT',
                                           'RAW_VALUE', 'CUTOFF_DATE'], 
                                output_excel=1, filename='OUTSTANDING_SHARES')
edgar_sbsu_mapping(edgar_tickers=tickers, filename='SBSU_BBG_201908.xlsx', download_data = True)

# Commence download of shares data from filings: url_list #
share_rights = []
SBSU_US = pd.read_excel('SHARES_SBSU.xlsx', sheet_name='Remaining')
SBSU_US['CIK'] = SBSU_US['CIK'].astype(str)
SBSU_US = pd.merge(SBSU_US, edgar_14A[['CIK', 'Company', 'Web_Filing']], how='inner', on='CIK')
edgar_filing = SBSU_US
urls = list(edgar_filing['Web_Filing'])
with ThreadPoolExecutor(max_workers=30) as pool:
    pool.map(edgar_extract_votes, urls)

# Print share_rights data into Pandas DataFrame # 
edgar_votes = pd.DataFrame(share_rights, 
                           columns=['Company', 'Ticker', 'CIK', 'URL', 'Class_Shares', 'Votes_Per_Share', 'Corpus'])
# Remove special characters and stopwords from Class_Shares column #
edgar_votes['Class_Shares'] = edgar_votes['Class_Shares'].str.replace(r"[,(`']", "").str.replace(r'\d{3,}', '')
edgar_votes['Class_Shares'] = edgar_votes['Class_Shares'].str.replace(r'[^\x00-\x7F]+', '')
edgar_votes['Class_Shares'] = edgar_votes['Class_Shares'].apply(lambda x: \
               ' '.join([item for item in x.split() if item.lower() not in stopWords]))
edgar_votes['Class_Shares'] = edgar_votes['Class_Shares'].str.replace(r'([A-Z])[Ss][Uu][s]?', r'\1SU')
        
# Remove duplicates in Class_Shares entries and sort them in alphabetical order #
edgar_votes.drop_duplicates(subset=['Company', 'Class_Shares', 'Votes_Per_Share'], keep='first', inplace = True)
edgar_votes.sort_values(by=['Company', 'Class_Shares'], inplace = True)

edgar_votes.to_excel(r'C:\Users\C_YEE\Dropbox\ESG Research\EDGAR_14A_VOTES.xlsx', sheet_name = 'EDGAR_14A', index = False)
edgar_votes.to_excel(r'C:\Users\C_YEE\Dropbox\ESG Research\EDGAR_10K_VOTES.xlsx', sheet_name = 'EDGAR_10K', index = False)